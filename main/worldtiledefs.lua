local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local _PlayFootstep = PlayFootstep
function PlayFootstep(inst, volume, ispredicted, ...)
    if inst == nil then
        return _PlayFootstep(inst, volume, ispredicted, ...)
    end
    
    local _snowlevel = rawget(TheWorld.state, "snowlevel")
    local _wetness = rawget(TheWorld.state, "wetness")

    local climate = GetClimate(inst)
    if IsIAClimate(climate) then
        TheWorld.state.snowlevel = 0
        TheWorld.state.wetness = TheWorld.state.islandwetness
    end

    _PlayFootstep(inst, volume, ispredicted, ...)

    TheWorld.state.snowlevel = _snowlevel
    TheWorld.state.wetness = _wetness

    -- NOTE: in sw this only ran for DoRunSounds in the player stategraph and there were no size 
    -- differences based on tag or rider support
    if IsIAClimate(climate) and TheWorld.components.flooding ~= nil then
        local pos = inst:GetPosition()
        if TheWorld.components.flooding:IsPointOnFlood(pos.x, 0, pos.z) then
            local size_inst = inst
            if inst:HasTag("player") then
                local rider = inst.components.rider or inst.replica.rider
                if rider ~= nil and rider:IsRiding() then
                    size_inst = rider:GetMount() or inst
                end
            end
            local rot = inst.Transform:GetRotation()
            local splash = SpawnPrefab("splash_footstep"..
                (   
                    (size_inst:HasTag("smallcreature") and "_small") or
                    (size_inst:HasTag("largecreature") and "_large" or "")
                ))
            local CameraRight = TheCamera:GetRightVec()
            local CameraDown = TheCamera:GetDownVec()
            local displacement = CameraRight:Cross(CameraDown) * .15
            local pos = pos - displacement
            splash.Transform:SetPosition(pos.x,pos.y, pos.z)
            splash.Transform:SetRotation(rot)
        end
    end
end
