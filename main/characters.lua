--[WARNING]: This file is imported into modclientmain.lua, be careful!

local characterassets = {
	--character
    Asset("ATLAS", "images/crafting_menu_avatars/avatar_walani.xml"),
    Asset("IMAGE", "images/crafting_menu_avatars/avatar_walani.tex"),

    Asset("IMAGE", "images/saveslot_portraits/walani.tex"),
    Asset("ATLAS", "images/saveslot_portraits/walani.xml"),

    Asset("IMAGE", "bigportraits/walani.tex"),
    Asset("ATLAS", "bigportraits/walani.xml"),

    Asset("IMAGE", "bigportraits/walani_none.tex"),
    Asset("ATLAS", "bigportraits/walani_none.xml"),

    Asset("IMAGE", "images/avatars/avatar_walani.tex"),
    Asset("ATLAS", "images/avatars/avatar_walani.xml"),

    Asset("IMAGE", "images/avatars/avatar_ghost_walani.tex"),
    Asset("ATLAS", "images/avatars/avatar_ghost_walani.xml"),

    Asset("IMAGE", "images/avatars/self_inspect_walani.tex"),
    Asset("ATLAS", "images/avatars/self_inspect_walani.xml"),

    Asset("IMAGE", "images/names_walani.tex"),
    Asset("ATLAS", "images/names_walani.xml"),

    Asset("IMAGE", "images/names_gold_walani.tex"),
	Asset("ATLAS", "images/names_gold_walani.xml"),

    Asset("IMAGE", "images/names_gold_cn_walani.tex"),
	Asset("ATLAS", "images/names_gold_cn_walani.xml"),

    Asset("ATLAS", "images/crafting_menu_avatars/avatar_wilbur.xml"),
    Asset("IMAGE", "images/crafting_menu_avatars/avatar_wilbur.tex"),

    Asset("IMAGE", "images/saveslot_portraits/wilbur.tex"),
    Asset("ATLAS", "images/saveslot_portraits/wilbur.xml"),

    Asset("IMAGE", "bigportraits/wilbur.tex"),
    Asset("ATLAS", "bigportraits/wilbur.xml"),

    Asset("IMAGE", "bigportraits/wilbur_none.tex"),
    Asset("ATLAS", "bigportraits/wilbur_none.xml"),

    Asset("IMAGE", "images/avatars/avatar_wilbur.tex"),
    Asset("ATLAS", "images/avatars/avatar_wilbur.xml"),

    Asset("IMAGE", "images/avatars/avatar_ghost_wilbur.tex"),
    Asset("ATLAS", "images/avatars/avatar_ghost_wilbur.xml"),

    Asset("IMAGE", "images/avatars/self_inspect_wilbur.tex"),
    Asset("ATLAS", "images/avatars/self_inspect_wilbur.xml"),

    Asset("IMAGE", "images/names_wilbur.tex"),
    Asset("ATLAS", "images/names_wilbur.xml"),

    Asset("IMAGE", "images/names_gold_wilbur.tex"),
	Asset("ATLAS", "images/names_gold_wilbur.xml"),

    Asset("IMAGE", "images/names_gold_cn_wilbur.tex"),
	Asset("ATLAS", "images/names_gold_cn_wilbur.xml"),
}

for k, v in pairs(characterassets) do table.insert(Assets, v) end

local IAENV = env
local AddModCharacter = AddModCharacter
GLOBAL.setfenv(1, GLOBAL)

AddModCharacter("walani", "FEMALE")
AddModCharacter("wilbur", "MALE")

PREFAB_SKINS["walani"] = {  -- Fixed the display of large figure
	"walani_none",
}

PREFAB_SKINS_IDS["walani"] = {
    ["walani_none"] = 1
}

PREFAB_SKINS["wilbur"] = {
    "wilbur_none",
}

PREFAB_SKINS_IDS["wilbur"] = {
    ["wilbur_none"] = 1
}

if IAENV.is_mim_enabled then
	return
end

CUSTOM_CHARACTER_SAILFACES["walani"] = {
    walani_none = true
}

CUSTOM_CHARACTER_SAILFACES["wilbur"] = {
    wilbur_none = true,
}

