GLOBAL.setfenv(1, GLOBAL)

local worldsettings_overrides = require("worldsettings_overrides")
local applyoverrides_post = worldsettings_overrides.Post

-- local function OverrideTuningVariables(tuning)
    -- if tuning ~= nil then
        -- for k, v in pairs(tuning) do
            -- TUNING[k] = v
        -- end
    -- end
-- end

local SPAWN_MODE_FN =
{
    never = "SpawnModeNever",
    always = "SpawnModeHeavy",
    often = "SpawnModeMed",
    rare = "SpawnModeLight",
}

local function SetSpawnMode(spawner, difficulty)
    if spawner ~= nil then
		local fn_name = SPAWN_MODE_FN[difficulty]
		if fn_name then
        	spawner[fn_name](spawner)
		end
    end
end

local SEASON_FRIENDLY_LENGTHS =
{
	noseason = 0,
	veryshortseason = TUNING.SEASON_LENGTH_FRIENDLY_VERYSHORT,
	shortseason = TUNING.SEASON_LENGTH_FRIENDLY_SHORT,
	default = TUNING.SEASON_LENGTH_FRIENDLY_DEFAULT,
	longseason = TUNING.SEASON_LENGTH_FRIENDLY_LONG,
	verylongseason = TUNING.SEASON_LENGTH_FRIENDLY_VERYLONG,
}

local SEASON_HARSH_LENGTHS =
{
	noseason = 0,
	veryshortseason = TUNING.SEASON_LENGTH_HARSH_VERYSHORT,
	shortseason = TUNING.SEASON_LENGTH_HARSH_SHORT,
	default = TUNING.SEASON_LENGTH_HARSH_DEFAULT,
	longseason = TUNING.SEASON_LENGTH_HARSH_LONG,
	verylongseason = TUNING.SEASON_LENGTH_HARSH_VERYLONG,
}

local MULTIPLY = {
	["never"] = 0,
	["veryrare"] = 0.25,
	["rare"] = 0.5,
	["default"] = 1,
	["often"] = 1.5,
	["always"] = 2,
}
local MULTIPLY_COOLDOWNS = {
	["never"] = 0,
	["veryrare"] = 2,
	["rare"] = 1.5,
	["default"] = 1,
	["often"] = .5,
	["always"] = .25,
}
local MULTIPLY_WAVES = {
	["never"] = 0,
	["veryrare"] = 0.25,
	["rare"] = 0.5,
	["default"] = 1,
	["often"] = 1.25,
	["always"] = 1.5,
}

local WORLDTYPES = WORLDTYPES or {mainclimate = {}, volcanoclimate = {}, islandclimate = {}, defaultclimate = {}, worldgen = {}}

--Overrides are after Load.
--To allow island components to Load, this is usually handled PreLoad in postinit/prefabs/world.lua
--However, the first time the world starts, there is no load, so this is the next-best opportunity.
applyoverrides_post.primaryworldtype = function(difficulty)
	if WORLDTYPES.volcanoclimate[difficulty] and not TheWorld:HasTag("volcano") then
		TheWorld:AddTag("volcano")
	end

	if WORLDTYPES.islandclimate[difficulty] and not TheWorld:HasTag("island") then
		TheWorld:AddTag("island")
		local volcanoisland = TheWorld.topology.ia_worldgen_version and TheWorld.topology and TheWorld.topology.overrides and TheWorld.topology.overrides.volcanoisland or "never"
		if volcanoisland == "default" and not TheWorld:HasTag("volcano") then
			TheWorld:AddTag("volcano")
		end
	end

	if not WORLDTYPES.defaultclimate[difficulty] and TheWorld:HasTag("forest") then
		TheWorld:RemoveTag("forest")
	end
	if TheWorld.installIAcomponents then
		TheWorld:installIAcomponents()
	end
end

applyoverrides_post.volcano = function(difficulty)
    if difficulty == "never" then
        local vm = TheWorld.components.volcanomanager
        if vm then
            vm:SetIntensity(0)
        end
    end
end

applyoverrides_post.dragoonegg = function(difficulty)
    local vm = TheWorld.components.volcanomanager
    if vm then
		vm:SetFirerainIntensity(MULTIPLY[difficulty] or 1)
    end
end

applyoverrides_post.tides = function(difficulty)
    if difficulty == "never" then
        local flooding = TheWorld.components.flooding
        if flooding and flooding.SetMaxTideModifier then
            flooding:SetMaxTideModifier(0)
        end
    end
end

applyoverrides_post.mild = function(difficulty)
	if difficulty == "random" then
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "autumn", length = GetRandomItem(SEASON_FRIENDLY_LENGTHS), random = true})
	else
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "autumn", length = SEASON_FRIENDLY_LENGTHS[difficulty]})
	end
end

applyoverrides_post.hurricane = function(difficulty)
	if difficulty == "random" then
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "winter", length = GetRandomItem(SEASON_HARSH_LENGTHS), random = true})
	else
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "winter", length = SEASON_HARSH_LENGTHS[difficulty]})
	end
end

applyoverrides_post.monsoon = function(difficulty)
	if difficulty == "random" then
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "spring", length = GetRandomItem(SEASON_FRIENDLY_LENGTHS), random = true})
	else
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "spring", length = SEASON_FRIENDLY_LENGTHS[difficulty]})
	end
end

applyoverrides_post.dry = function(difficulty)
	if difficulty == "random" then
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "summer", length = GetRandomItem(SEASON_HARSH_LENGTHS), random = true})
	else
		TheWorld:PushEvent("ms_setseasonlength_tropical", {season = "summer", length = SEASON_HARSH_LENGTHS[difficulty]})
	end
end

applyoverrides_post.floods = function(difficulty)
    local flooding = TheWorld.components.flooding
    if flooding and flooding.SetFloodSettings then
        local lvl = TUNING.MAX_FLOOD_LEVEL --15,
        local freq = TUNING.FLOOD_FREQUENCY --0.005,
		flooding:SetFloodSettings(math.min(1, MULTIPLY[difficulty]) * lvl, (MULTIPLY[difficulty] or 1) * freq)
    end
end

applyoverrides_post.oceanwaves = function(difficulty)
	local wm = TheWorld.components.wavemanager_ia
	if wm then
		wm:SetWaveSettings(MULTIPLY[difficulty] or 1)
	end
	TUNING.WATERVISUALSHIMMER = MULTIPLY_WAVES[difficulty] or 1
	TUNING.WATERVISUALCAMERA = MULTIPLY_WAVES[difficulty] or 1
	-- OverrideTuningVariables({
		-- WATERVISUALSHIMMER = MULTIPLY_WAVES[difficulty] or 1,
		-- WATERVISUALCAMERA = MULTIPLY_WAVES[difficulty] or 1,
	-- })
end

applyoverrides_post.poison = function(difficulty)
	IA_CONFIG.poisonenabled = difficulty ~= "never"
end

applyoverrides_post.tigershark = function(difficulty)
	local tigersharker = TheWorld.components.tigersharker
	if tigersharker then
		tigersharker:SetChanceModifier(MULTIPLY[difficulty] or 1)
		tigersharker:SetCooldownModifier(MULTIPLY_COOLDOWNS[difficulty] or 1)
	end
end

applyoverrides_post.kraken = function(difficulty)
	local krakener = TheWorld.components.krakener
	if krakener then
		krakener:SetChanceModifier(MULTIPLY[difficulty] or 1)
		krakener:SetCooldownModifier(MULTIPLY_COOLDOWNS[difficulty] or 1)
	end
end

applyoverrides_post.twister = function(difficulty)
local basehassler = TheWorld.components.twisterspawner
	if basehassler then
		if difficulty == "never" then
			basehassler:OverrideAttacksPerSeason("TWISTER", 0)
        elseif difficulty == "rare" then
            basehassler:OverrideAttacksPerSeason("TWISTER", 2)
		elseif difficulty == "default" then
            --Defaults specified in twisterspawner.lua
        elseif difficulty == "often" then
			basehassler:OverrideAttacksPerSeason("TWISTER", 8)
		elseif difficulty == "always" then
			basehassler:OverrideAttacksPerSeason("TWISTER", 10)
		end
		if difficulty == "always" then
			basehassler:OverrideAttackDuringOffSeason("TWISTER", true)
		else
			basehassler:OverrideAttackDuringOffSeason("TWISTER", false)
		end
	end
end

applyoverrides_post.mosquitos = function(difficulty)
	if TheWorld.components.floodmosquitospawner then
		SetSpawnMode(TheWorld.components.floodmosquitospawner, difficulty)
	end
end

local chess_fn = applyoverrides_post.chess
applyoverrides_post.chess = function(difficulty, ...)
	if chess_fn then
		chess_fn(difficulty, ...)
	end

	local chessnavy = TheWorld.components.chessnavy
	if chessnavy then
		chessnavy:SetDifficultyMultiplier(nil)
		chessnavy:SetFrequencyMultiplier(nil)
		if difficulty == "never" then
			-- chessnavy:SetDifficultyMultiplier(0)
			-- chessnavy:SetFrequencyMultiplier(90)
			chessnavy:SetEnabled(false)
		else
			chessnavy:SetEnabled(true)
		end
		if difficulty == "rare" then
			-- chessnavy:SetDifficultyMultiplier(.5)
			chessnavy:SetFrequencyMultiplier(2)
		elseif difficulty == "often" then
			chessnavy:SetDifficultyMultiplier(1.3)
			chessnavy:SetFrequencyMultiplier(.8)
		elseif difficulty == "always" then
			chessnavy:SetDifficultyMultiplier(1.5)
			chessnavy:SetFrequencyMultiplier(.6)
		end
	end
end

--[[applyoverrides_post.clock_segs = function(difficulty)
	if type(difficulty) == "table" then
		TheWorld:PushEvent("ms_setseasonclocksegs", difficulty)
	end
end]]
