--[[
AddRecipe("name", {Ingredient("name", numrequired)}, GLOBAL.RECIPETABS.LIGHT, TECH.NONE, "placer", min_spacing, b_nounlock, numtogive, "builder_required_tag", nil, "image.tex", testfn)

AquaticRecipe("name", {distance=, shore_distance=, platform_distance=, shore_buffer_max=, shore_buffer_min=, platform_buffer_max=, platform_buffer_min=, aquatic_buffer_min=, noshore=})
]]

local AddRecipe2 = AddRecipe2
local AddRecipePostInit = AddRecipePostInit
local AddDeconstructRecipe = AddDeconstructRecipe
local AddCharacterRecipe = AddCharacterRecipe
GLOBAL.setfenv(1, GLOBAL)

local function SortRecipe(a, b, filter_name, offset)
    local filter = CRAFTING_FILTERS[filter_name]
    if filter and filter.recipes then
        for sortvalue, product in ipairs(filter.recipes) do
            if product == a then
                table.remove(filter.recipes, sortvalue)
                break
            end
        end

        local target_position = #filter.recipes + 1
        for sortvalue, product in ipairs(filter.recipes) do
            if product == b then
                target_position = sortvalue + offset
                break
            end
        end

        table.insert(filter.recipes, target_position, a)
    end
end

local function SortBefore(a, b, filter_name)
    SortRecipe(a, b, filter_name, 0)
end

local function SortAfter(a, b, filter_name)
    SortRecipe(a, b, filter_name, 1)
end

local function IsSWMarshLand(pt, rot)
    local ground_tile = TheWorld.Map:GetTileAtPoint(pt.x, pt.y, pt.z)
    return ground_tile and (ground_tile == WORLD_TILES.MARSH or ground_tile == WORLD_TILES.TIDALMARSH)
end

AddRecipe2("chiminea", {Ingredient("limestonenugget", 2), Ingredient("sand", 2), Ingredient("log", 2)}, TECH.NONE, {placer = "chiminea_placer"}, {"LIGHT","COOKING","WINTER","RAIN"})
SortAfter("chiminea", "firepit", "LIGHT")
SortAfter("chiminea", "firepit", "COOKING")
SortAfter("chiminea", "firepit", "WINTER")
SortAfter("chiminea", "eyebrellahat", "RAIN")
AddRecipe2("obsidianfirepit", {Ingredient("obsidian", 8), Ingredient("log", 3)}, TECH.SCIENCE_TWO, {placer = "obsidianfirepit_placer"}, {"LIGHT","COOKING","WINTER","RAIN"})
SortAfter("obsidianfirepit", "coldfirepit", "LIGHT")
SortAfter("obsidianfirepit", "chiminea", "COOKING")
SortAfter("obsidianfirepit", "chiminea", "WINTER")
SortAfter("obsidianfirepit", "chiminea", "RAIN")
AddRecipe2("bottlelantern", {Ingredient("ia_messagebottleempty", 1), Ingredient("bioluminescence", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"LIGHT"})
SortAfter("bottlelantern", "lantern", "LIGHT")
AddRecipe2("sea_chiminea", {Ingredient("limestonenugget", 6), Ingredient("sand", 4), Ingredient("tar", 6)}, IA_CONFIG.sealabtweak and TECH.SCIENCE_THREE or TECH.NONE, {placer="sea_chiminea_placer"}, {"LIGHT","COOKING","WINTER","RAIN"})
AquaticRecipe("sea_chiminea", {shore_distance = 3, platform_distance = 4})
SortAfter("sea_chiminea", "obsidianfirepit", "LIGHT")
SortAfter("sea_chiminea", "obsidianfirepit", "COOKING")
SortAfter("sea_chiminea", "obsidianfirepit", "WINTER")
SortAfter("sea_chiminea", "obsidianfirepit", "RAIN")


AddRecipePostInit("wintersfeastoven", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("marble")
    if ingredient then
        ingredient:AddDictionaryPrefab("limestonenugget")
    end
end)
AddRecipePostInit("table_winters_feast", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("beefalowool")
    if ingredient then
        ingredient:AddDictionaryPrefab("fabric")
    end
end)
AddRecipe2("waterchest", {Ingredient("boards", 4), Ingredient("tar", 1)}, IA_CONFIG.sealabtweak and TECH.SCIENCE_THREE or TECH.NONE, {placer="waterchest_placer", min_spacing=1}, {"STRUCTURES", "CONTAINERS"})
AquaticRecipe("waterchest", {shore_distance=3, platform_distance=4})
SortAfter("waterchest", "treasurechest", "STRUCTURES")
SortAfter("waterchest", "treasurechest", "CONTAINERS")
AddRecipe2("wall_limestone_item", {Ingredient("limestonenugget", 2)}, TECH.SCIENCE_TWO, {numtogive=6}, {"STRUCTURES"})
SortAfter("wall_limestone_item", "wall_stone_item", "STRUCTURES")
AddRecipe2("wall_enforcedlimestone_item", {Ingredient("limestonenugget", 2), Ingredient("seaweed", 4)}, TECH.SCIENCE_ONE, {numtogive=6}, {"STRUCTURES"})
SortAfter("wall_enforcedlimestone_item", "wall_limestone_item", "STRUCTURES")
AddRecipe2("wildborehouse", {Ingredient("bamboo", 8), Ingredient("palmleaf", 5), Ingredient("pigskin", 4)}, TECH.SCIENCE_TWO, {placer="wildborehouse_placer"}, {"STRUCTURES"})
SortAfter("wildborehouse", "pighouse", "STRUCTURES")
AddRecipe2("ballphinhouse", {Ingredient("limestonenugget", 4), Ingredient("seaweed", 4), Ingredient("dorsalfin", 2)}, TECH.SCIENCE_ONE, {placer="ballphinhouse_placer", min_spacing=1}, {"STRUCTURES"})
AquaticRecipe("ballphinhouse", {shore_distance=3, platform_distance=4, noshore=true})
SortAfter("ballphinhouse", "wildborehouse", "STRUCTURES")
AddRecipe2("primeapebarrel", {Ingredient("twigs", 10), Ingredient("cave_banana", 3), Ingredient("poop", 4)}, TECH.SCIENCE_TWO, {placer="primeapebarrel_placer"}, {"STRUCTURES"})
SortAfter("primeapebarrel", "ballphinhouse", "STRUCTURES")
AddRecipe2("dragoonden", {Ingredient("dragoonheart", 1), Ingredient("rocks", 5), Ingredient("obsidian", 4)}, TECH.SCIENCE_TWO, {placer="dragoonden_placer"}, {"STRUCTURES"})
SortAfter("dragoonden", "primeapebarrel", "STRUCTURES")
AddRecipe2("turf_snakeskin", {Ingredient("snakeskin", 2), Ingredient("fabric", 1)}, TECH.SCIENCE_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_snakeskin", "turf_carpetfloor", "DECOR")
AddRecipe2("sandbagsmall_item", {Ingredient("sand", 3), Ingredient("fabric", 2)}, TECH.SCIENCE_ONE, {numtogive=4}, {"STRUCTURES","RAIN"})
SortAfter("sandbagsmall_item", "wall_enforcedlimestone_item", "STRUCTURES")
SortAfter("sandbagsmall_item", "lightning_rod", "RAIN")
AddRecipe2("sandcastle", {Ingredient("sand", 4), Ingredient("palmleaf", 2), Ingredient("seashell", 3)}, TECH.NONE, {placer="sandcastle_placer"}, {"STRUCTURES","DECOR"})
SortAfter("sandcastle", "sisturn", "STRUCTURES")
SortAfter("sandcastle", "endtable", "DECOR")
AddRecipePostInit("soil_amender", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("kelp")
    if ingredient then
        ingredient:AddDictionaryPrefab("seaweed")
    end
end)
AddRecipePostInit("soil_amender", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("messagebottleempty")
    if ingredient then
        ingredient:AddDictionaryPrefab("ia_messagebottleempty")
    end
end)
AddRecipe2("mussel_stick", {Ingredient("bamboo", 2), Ingredient("vine", 1), Ingredient("seaweed", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"GARDENING"})
SortAfter("mussel_stick", "premiumwateringcan", "GARDENING")
AddRecipe2("fish_farm", {Ingredient("coconut", 4), Ingredient("rope", 2), Ingredient("silk", 2)}, TECH.SCIENCE_ONE, {placer="fish_farm_placer"}, {"GARDENING"})
AllRecipes.fish_farm.testfn = function(pt, rot)
    local ents = TheSim:FindEntities(pt.x, pt.y, pt.z, 5, {"structure"})
    if #ents < 1 then
        return true
    end
    return false
end
AquaticRecipe("fish_farm", {shore_distance=3, platform_distance=4})
SortAfter("fish_farm", "seedpouch", "GARDENING")
AddRecipe2("mussel_bed", {Ingredient("mussel", 1), Ingredient("coral", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"GARDENING"})
SortAfter("mussel_bed", "compostwrap", "GARDENING")
if IA_CONFIG.oldwarly then
    AllRecipes["portablecookpot_item"].ingredients = {Ingredient("limestonenugget", 3), Ingredient("redgem", 1), Ingredient("log", 3)}
    AllRecipes["portableblender_item"].builder_tag = "invalid"
    AllRecipes["portablespicer_item"].builder_tag = "invalid"
end


AddRecipe2("monkeyball", {Ingredient("snakeskin", 4), Ingredient("cave_banana", 1), Ingredient("rope", 2)}, TECH.SCIENCE_ONE, {nounlock=false}, {"TOOLS"})
SortAfter("monkeyball", "megaflare", "TOOLS")
AddRecipePostInit("megaflare", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("glommerfuel")
    if ingredient then
        ingredient:AddDictionaryPrefab("blubber")
    end
end)
AddRecipe2("palmleaf_umbrella", {Ingredient("palmleaf", 3), Ingredient("twigs", 4), Ingredient("petals", 6)}, TECH.NONE, {nounlock=false}, {"RAIN","SUMMER","CLOTHING"})
SortAfter("palmleaf_umbrella", "grass_umbrella", "RAIN")
SortAfter("palmleaf_umbrella", "grass_umbrella", "SUMMER")
SortAfter("palmleaf_umbrella", "grass_umbrella", "CLOTHING")
AddRecipe2("antivenom", {Ingredient("venomgland", 1), Ingredient("coral", 2), Ingredient("seaweed", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"RESTORATION"})
SortAfter("antivenom", "healingsalve", "RESTORATION")
AddRecipePostInit("waterballoon", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("mosquitosack")
    if ingredient then
        ingredient:AddDictionaryPrefab("mosquitosack_yellow")
    end
end)
AddRecipe2("thatchpack", {Ingredient("palmleaf", 4)}, TECH.NONE, {nounlock=false}, {"CONTAINERS","CLOTHING"})
SortBefore("thatchpack", "backpack", "CONTAINERS")
SortBefore("thatchpack", "backpack", "CLOTHING")
if IA_CONFIG.oldwarly then
    AllRecipes["spicepack"].ingredients = {
        Ingredient("fabric", 1),
        Ingredient("rope", 1),
        Ingredient("bluegem", 1)
    }
end
AddRecipe2("seasack", {Ingredient("shark_gills", 1), Ingredient("vine", 2), Ingredient("seaweed", 5)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CONTAINERS","COOKING"})
SortAfter("seasack", "icepack", "CONTAINERS")
SortAfter("seasack", "icepack", "COOKING")
AddRecipe2("palmleaf_hut", {Ingredient("palmleaf", 4), Ingredient("bamboo", 4), Ingredient("rope", 4)}, TECH.SCIENCE_TWO, {placer="palmleaf_hut_placer"}, {"STRUCTURES","RAIN","SUMMER"})
SortAfter("palmleaf_hut", "siestahut", "STRUCTURES")
SortAfter("palmleaf_hut", "sandbagsmall_item", "RAIN")
SortAfter("palmleaf_hut", "siestahut", "SUMMER")
AddRecipe2("tropicalfan", {Ingredient("doydoyfeather", 5), Ingredient("cutreeds", 2), Ingredient("rope", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"SUMMER","CLOTHING"})
SortAfter("tropicalfan", "featherfan", "SUMMER")
SortAfter("tropicalfan", "featherfan", "CLOTHING")
AddRecipe2("doydoynest", {Ingredient("doydoyfeather", 2), Ingredient("twigs", 8), Ingredient("poop", 4)}, TECH.SCIENCE_TWO, {placer="doydoynest_placer"}, {"STRUCTURES"})
SortAfter("doydoynest", "rabbithouse", "STRUCTURES")

AddRecipe2("machete", {Ingredient("flint", 3), Ingredient("twigs", 1)}, TECH.NONE, {nounlock=false}, {"TOOLS"})
SortAfter("machete", "axe", "TOOLS")
AddRecipe2("goldenmachete", {Ingredient("goldnugget", 2), Ingredient("twigs", 4)}, TECH.SCIENCE_TWO, {nounlock=false}, {"TOOLS"})
SortAfter("goldenmachete", "goldenaxe", "TOOLS")

AddRecipe2("sea_lab", {Ingredient("limestonenugget", 2), Ingredient("sand", 2), Ingredient("transistor", 2)}, TECH.SCIENCE_ONE, {placer="sea_lab_placer"}, {"PROTOTYPERS","STRUCTURES"})
AquaticRecipe("sea_lab", {shore_distance=3, platform_distance=4})
SortAfter("sea_lab", "researchlab2", "PROTOTYPERS")
SortAfter("sea_lab", "researchlab2", "STRUCTURES")
AddRecipe2("icemaker", {Ingredient("heatrock", 1), Ingredient("bamboo", 5), Ingredient("transistor", 2)}, TECH.SCIENCE_TWO, {placer="icemaker_placer"}, {"SUMMER","STRUCTURES"})
SortAfter("icemaker", "firesuppressor", "SUMMER")
SortAfter("icemaker", "firesuppressor", "STRUCTURES")

AddRecipe2("piratihatitator", {Ingredient("parrot", 1), Ingredient("boards", 4), Ingredient("piratehat", 1)}, TECH.SCIENCE_ONE, {placer="piratihatitator_placer"}, {"PROTOTYPERS","MAGIC","STRUCTURES"})
SortAfter("piratihatitator", "researchlab4", "PROTOTYPERS")
SortAfter("piratihatitator", "researchlab4", "MAGIC")
SortAfter("piratihatitator", "researchlab4", "STRUCTURES")
AddRecipe2("ox_flute", {Ingredient("ox_horn", 1), Ingredient("nightmarefuel", 2), Ingredient("rope", 1)}, TECH.MAGIC_TWO, {nounlock=false}, {"MAGIC"})
SortAfter("ox_flute", "panflute", "MAGIC")
AddRecipe2("shipwrecked_entrance", {Ingredient("nightmarefuel", 4), Ingredient("livinglog", 4), Ingredient("sunken_boat_trinket_4", 1)}, TECH.MAGIC_TWO, {placer="shipwrecked_entrance_placer"}, {"MAGIC","STRUCTURES"})
AllRecipes.shipwrecked_entrance.testfn = function(pt, rot)
    if TheWorld:HasTag("island") or TheWorld:HasTag("volcano") then
        AllRecipes.shipwrecked_entrance.product = "shipwrecked_exit"
    else
        AllRecipes.shipwrecked_entrance.product = "shipwrecked_entrance"
    end
    return true, false
end
SortAfter("shipwrecked_entrance", "telebase", "MAGIC")
SortAfter("shipwrecked_entrance", "telebase", "STRUCTURES")

AddRecipe2("fabric", {Ingredient("bamboo", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"REFINE"})
SortAfter("fabric", "beeswax", "REFINE")
AddRecipe2("limestonenugget", {Ingredient("coral", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"REFINE"})
SortAfter("limestonenugget", "fabric", "REFINE")
AddRecipe2("nubbin", {Ingredient("corallarve", 1), Ingredient("limestonenugget", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"REFINE"})
SortAfter("nubbin", "limestonenugget", "REFINE")
AddRecipe2("goldnugget", {Ingredient("dubloon", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"REFINE"})
SortAfter("goldnugget", "nubbin", "REFINE")
AddRecipe2("ice", {Ingredient("hail_ice", 4)}, TECH.SCIENCE_TWO, {nounlock=false}, {"REFINE"})
SortAfter("ice", "goldnugget", "REFINE")
AddRecipe2("ia_messagebottleempty", {Ingredient("sand", 3)}, TECH.SCIENCE_TWO, {nounlock=false}, {"REFINE"})
SortAfter("ia_messagebottleempty", "ice", "REFINE")


AddRecipe2("spear_poison", {Ingredient("venomgland", 1), Ingredient("spear", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"WEAPONS"})
SortAfter("spear_poison", "spear", "WEAPONS")
AddRecipe2("armorseashell", {Ingredient("seashell", 10), Ingredient("seaweed", 2), Ingredient("rope", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"ARMOUR"})
SortAfter("armorseashell", "armorwood", "ARMOUR")
AddRecipe2("armorlimestone", {Ingredient("limestonenugget", 3), Ingredient("rope", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"ARMOUR"})
SortAfter("armorlimestone", "armormarble", "ARMOUR")
AddRecipe2("armorcactus", {Ingredient("needlespear", 3), Ingredient("armorwood", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"ARMOUR"})
SortAfter("armorcactus", "armorlimestone", "ARMOUR")
AddRecipe2("oxhat", {Ingredient("ox_horn", 1), Ingredient("seashell", 4), Ingredient("rope", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"ARMOUR"})
SortAfter("oxhat", "footballhat", "ARMOUR")
AddRecipe2("blowdart_poison", {Ingredient("cutreeds", 2), Ingredient("venomgland", 1), Ingredient("feather_crow", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"WEAPONS"})
SortAfter("blowdart_poison", "blowdart_fire", "WEAPONS")
AddRecipe2("coconade", {Ingredient("coconut", 2), Ingredient("rope", 1), Ingredient("gunpowder", 1)}, TECH.SCIENCE_ONE, {numtogive=2}, {"WEAPONS"})
SortAfter("coconade", "gunpowder", "WEAPONS")
AddRecipe2("spear_launcher", {Ingredient("jellyfish", 1), Ingredient("bamboo", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"WEAPONS"})
SortAfter("spear_launcher", "spear_wathgrithr", "WEAPONS")
AddRecipe2("cutlass", {Ingredient("swordfish_dead", 1), Ingredient("goldnugget", 2), Ingredient("twigs", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"WEAPONS"})
SortAfter("cutlass", "nightstick", "WEAPONS")
AddRecipe2("brainjellyhat", {Ingredient("coral_brain", 1), Ingredient("jellyfish", 1), Ingredient("rope", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"PROTOTYPERS","CLOTHING"})
SortAfter("brainjellyhat", "researchlab3", "PROTOTYPERS")
SortAfter("brainjellyhat", "catcoonhat", "CLOTHING")
AddRecipe2("shark_teethhat", {Ingredient("houndstooth", 5), Ingredient("goldnugget", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"CLOTHING"})
SortAfter("shark_teethhat", "brainjellyhat", "CLOTHING")
AddRecipePostInit("kelphat", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("kelp")
    if ingredient then
        ingredient:AddDictionaryPrefab("seaweed")
    end
end)
AddRecipe2("snakeskinhat", {Ingredient("snakeskin", 1), Ingredient("strawhat", 1), Ingredient("boneshard", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CLOTHING","RAIN"})
SortAfter("snakeskinhat", "rainhat", "CLOTHING")
SortAfter("snakeskinhat", "rainhat", "RAIN")
AddRecipe2("armor_snakeskin", {Ingredient("snakeskin", 2), Ingredient("vine", 2), Ingredient("boneshard", 2)}, TECH.SCIENCE_ONE, {nounlock=false}, {"CLOTHING","RAIN","WINTER"})
SortAfter("armor_snakeskin", "raincoat", "CLOTHING")
SortAfter("armor_snakeskin", "raincoat", "RAIN")
SortAfter("armor_snakeskin", "raincoat", "WINTER")
AddRecipe2("blubbersuit", {Ingredient("blubber", 4), Ingredient("fabric", 2), Ingredient("palmleaf", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CLOTHING","RAIN","WINTER"})
SortAfter("blubbersuit", "armor_snakeskin", "CLOTHING")
SortAfter("blubbersuit", "armor_snakeskin", "RAIN")
SortAfter("blubbersuit", "armor_snakeskin", "WINTER")
AddRecipe2("tarsuit", {Ingredient("tar", 4), Ingredient("fabric", 2), Ingredient("palmleaf", 2)}, TECH.SCIENCE_ONE, {nounlock=false}, {"CLOTHING","RAIN"})
SortAfter("tarsuit", "blubbersuit", "CLOTHING")
SortAfter("tarsuit", "blubbersuit", "RAIN")
AddRecipePostInit("hawaiianshirt", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("cactus_flower")
    if ingredient then
        ingredient:AddDictionaryPrefab("petals")
    end
end)
AddRecipe2("armor_windbreaker", {Ingredient("blubber", 2), Ingredient("fabric", 1), Ingredient("rope", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CLOTHING","RAIN"})
SortAfter("armor_windbreaker", "tarsuit", "CLOTHING")
SortAfter("armor_windbreaker", "tarsuit", "RAIN")
AddRecipe2("gashat", {Ingredient("ia_messagebottleempty", 2), Ingredient("coral", 3), Ingredient("jellyfish", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CLOTHING"})
SortAfter("gashat", "brainjellyhat", "CLOTHING")
AddRecipe2("aerodynamichat", {Ingredient("shark_fin", 1), Ingredient("vine", 2), Ingredient("coconut", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CLOTHING"})
SortAfter("aerodynamichat", "gashat", "CLOTHING")
AddRecipe2("double_umbrellahat", {Ingredient("shark_gills", 2), Ingredient("umbrella", 1), Ingredient("strawhat", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"CLOTHING","RAIN","SUMMER"})
SortAfter("double_umbrellahat", "eyebrellahat", "CLOTHING")
SortAfter("double_umbrellahat", "eyebrellahat", "RAIN")
SortAfter("double_umbrellahat", "eyebrellahat", "SUMMER")
AddRecipePostInit("polly_rogershat", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("monkey_mediumhat")
    if ingredient then
        ingredient:AddDictionaryPrefab("piratehat")
    end
end)

AddRecipe2("boat_lograft", {Ingredient("log", 6), Ingredient("cutgrass", 4)}, TECH.NONE, {placer="boat_lograft_placer"}, {"SEAFARING"})
AquaticRecipe("boat_lograft", {distance=4, platform_buffer_min=2})
AddRecipe2("boat_raft", {Ingredient("bamboo", 4), Ingredient("vine", 3)}, TECH.NONE, {placer="boat_raft_placer"}, {"SEAFARING"})
AquaticRecipe("boat_raft", {distance=4, platform_buffer_min=2})
AddRecipe2("boat_row", {Ingredient("boards", 3), Ingredient("vine", 4)}, TECH.SCIENCE_ONE, {placer="boat_row_placer"}, {"SEAFARING"})
AquaticRecipe("boat_row", {distance=4, platform_buffer_min=2})
AddRecipe2("boat_cargo", {Ingredient("boards", 6), Ingredient("rope", 3)}, TECH.SCIENCE_TWO, {placer="boat_cargo_placer"}, {"SEAFARING"})
AquaticRecipe("boat_cargo", {distance=4, platform_buffer_min=2})
AddRecipe2("boat_armoured", {Ingredient("boards", 6), Ingredient("rope", 3), Ingredient("seashell", 10)}, TECH.SCIENCE_TWO, {placer="boat_armoured_placer"}, {"SEAFARING"})
AquaticRecipe("boat_armoured", {distance=4, platform_buffer_min=2})
AddRecipe2("boat_encrusted", {Ingredient("boards", 6), Ingredient("rope", 3), Ingredient("limestonenugget", 4)}, TECH.SCIENCE_TWO, {placer="boat_encrusted_placer"}, {"SEAFARING"})
AquaticRecipe("boat_encrusted", {distance=4, platform_buffer_min=2})
AddRecipe2("boatrepairkit", {Ingredient("boards", 2), Ingredient("stinger", 2), Ingredient("rope", 2)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING"})
AddRecipe2("tarlamp", {Ingredient("tar", 1), Ingredient("seashell", 1)}, TECH.NONE, {nounlock=false}, {"LIGHT","SEAFARING"})
SortBefore("tarlamp", "lantern", "LIGHT")
AddRecipe2("boat_torch", {Ingredient("twigs", 2), Ingredient("torch", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"LIGHT","SEAFARING"})
SortAfter("boat_torch", "bottlelantern", "LIGHT")
AddRecipe2("boat_lantern", {Ingredient("ia_messagebottleempty", 1), Ingredient("twigs", 2), Ingredient("fireflies", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"LIGHT","SEAFARING"})
SortAfter("boat_lantern", "boat_torch", "LIGHT")
AddRecipe2("sail_palmleaf", {Ingredient("bamboo", 2), Ingredient("vine", 2), Ingredient("palmleaf", 4)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING"})
AddRecipe2("sail_cloth", {Ingredient("bamboo", 2), Ingredient("rope", 2), Ingredient("fabric", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"SEAFARING"})
AddRecipe2("sail_snakeskin", {Ingredient("log", 4), Ingredient("rope", 2), Ingredient("snakeskin", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"SEAFARING"})
AddRecipe2("sail_feather", {Ingredient("bamboo", 4), Ingredient("rope", 2), Ingredient("doydoyfeather", 4)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING"})
AddRecipe2("ironwind", {Ingredient("turbine_blades", 1), Ingredient("transistor", 1), Ingredient("goldnugget", 2)},  TECH.SCIENCE_TWO, {nounlock=false}, {"SEAFARING"})
AddRecipe2("boatcannon", {Ingredient("coconut", 6), Ingredient("log", 5), Ingredient("gunpowder", 4)},  TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING"})
AddRecipe2("seatrap", {Ingredient("palmleaf", 4), Ingredient("ia_messagebottleempty", 3), Ingredient("jellyfish", 1)},  TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING","TOOLS","GARDENING"})
SortAfter("seatrap", "birdtrap", "TOOLS")
SortAfter("seatrap", "birdtrap", "GARDENING")
AddRecipe2("trawlnet", {Ingredient("bamboo", 2), Ingredient("rope", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING"})
AddRecipe2("telescope", {Ingredient("ia_messagebottleempty", 1), Ingredient("pigskin", 1), Ingredient("goldnugget", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING","TOOLS"})
SortAfter("telescope", "compass", "TOOLS")
AddRecipe2("supertelescope", {Ingredient("telescope", 1), Ingredient("tigereye", 1), Ingredient("goldnugget", 1)}, TECH.SCIENCE_TWO, {nounlock=false}, {"SEAFARING","TOOLS"})
SortAfter("supertelescope", "telescope", "TOOLS")
AddRecipe2("captainhat", {Ingredient("seaweed", 1), Ingredient("boneshard", 1), Ingredient("strawhat", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING","CLOTHING"})
SortAfter("captainhat", "shark_teethhat", "CLOTHING")
AddRecipe2("piratehat", {Ingredient("boneshard", 2), Ingredient("silk", 2), Ingredient("rope", 1)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING","CLOTHING"})
SortAfter("piratehat", "captainhat", "CLOTHING")
AddRecipe2("armor_lifejacket", {Ingredient("fabric", 2), Ingredient("vine", 2), Ingredient("ia_messagebottleempty", 3)}, TECH.SCIENCE_ONE, {nounlock=false}, {"SEAFARING","CLOTHING"})
SortAfter("armor_lifejacket", "armor_windbreaker", "CLOTHING")
AddRecipe2("buoy", {Ingredient("ia_messagebottleempty", 1), Ingredient("bioluminescence", 2), Ingredient("bamboo", 4)}, TECH.SCIENCE_ONE, {placer="buoy_placer"}, {"LIGHT","SEAFARING","STRUCTURES"})
AquaticRecipe("buoy", {shore_distance=3, platform_distance=4})
SortBefore("buoy", "nightlight", "STRUCTURES")
SortAfter("buoy", "sea_chiminea", "LIGHT")
AddRecipe2("quackendrill", {Ingredient("quackenbeak", 1), Ingredient("gears", 1), Ingredient("transistor", 2)}, TECH.SCIENCE_TWO, {nounlock=false}, {"TOOLS","SEAFARING"})
SortAfter("quackendrill", "beef_bell", "TOOLS")
AddRecipe2("quackeringram", {Ingredient("quackenbeak", 1), Ingredient("rope", 4), Ingredient("bamboo", 4)}, TECH.SCIENCE_TWO, {nounlock=false}, {"SEAFARING"})
AddRecipe2("tar_extractor", {Ingredient("coconut", 2), Ingredient("limestonenugget", 4), Ingredient("bamboo", 4)}, IA_CONFIG.sealabtweak and TECH.SCIENCE_THREE or TECH.NONE, {placer="tar_extractor_placer"}, {"SEAFARING","STRUCTURES"})
AquaticRecipe("tar_extractor", {shore_distance=3, platform_distance=4})
SortAfter("tar_extractor", "icemaker", "STRUCTURES")
AllRecipes.tar_extractor.testfn = function(pt, rot)
    local range = .1
    local tarpits = TheSim:FindEntities(pt.x, pt.y, pt.z, range, {"tarpit"})

    if #tarpits > 0 then
        for k, v in pairs(tarpits) do
            if not v:HasTag("NOCLICK") then
                return true, false
            end
        end
    end

    --Fix an extremely inconvenient bug with left-clicking to build a recipe (does not apply to action buttons) -M
    range = 1
    tarpits = TheSim:FindEntities(pt.x, pt.y, pt.z, range, {"tarpit"})

    if #tarpits > 0 then
        for k, v in pairs(tarpits) do
            if not v:HasTag("NOCLICK") then
                local newpt = v:GetPosition()
                --realign (editing the actual pt via the table pointer)
                pt.x = newpt.x
                pt.y = newpt.y
                pt.z = newpt.z
                return true, false
            end
        end
    end

    return false, false
end
AddRecipe2("sea_yard", {Ingredient("tar", 6), Ingredient("limestonenugget", 6), Ingredient("log", 4)}, IA_CONFIG.sealabtweak and TECH.SCIENCE_THREE or TECH.NONE, {placer="sea_yard_placer", min_spacing=4}, {"SEAFARING","STRUCTURES"})
AquaticRecipe("sea_yard", {shore_distance=3, platform_distance=4})
SortAfter("sea_yard", "tar_extractor", "STRUCTURES")

-- TURFS --
AddRecipe2("turf_jungle", {Ingredient("jungletreeseed", 1), Ingredient("vine", 1)}, TECH.TURFCRAFTING_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_jungle", "turf_monkey_ground", "DECOR")
AddRecipe2("turf_meadow", {Ingredient("cutgrass", 1), Ingredient("petals", 1)}, TECH.TURFCRAFTING_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_meadow", "turf_jungle", "DECOR")
AddRecipe2("turf_tidalmarsh", {Ingredient("cutreeds", 1), Ingredient("spoiled_food", 2)}, TECH.MASHTURFCRAFTING_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_tidalmarsh", "turf_meadow", "DECOR")
AddRecipe2("turf_magmafield", {Ingredient("rocks", 1), Ingredient("nitre", 1)}, TECH.TURFCRAFTING_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_magmafield", "turf_tidalmarsh", "DECOR")
AddRecipe2("turf_ash", {Ingredient("ash", 1), Ingredient("charcoal", 1)}, TECH.TURFCRAFTING_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_ash", "turf_magmafield", "DECOR")
AddRecipe2("turf_volcano", {Ingredient("rocks", 1), Ingredient("charcoal", 1)}, TECH.TURFCRAFTING_TWO, {numtogive=4}, {"DECOR"})
SortAfter("turf_volcano", "turf_ash", "DECOR")
--

--UNCRAFTABLE:
--NOTE: These recipes are not supposed to be craftable! This is just so the deconstruction staff works as expected.

AddDeconstructRecipe("wildborehead", {Ingredient("pigskin", 2), Ingredient("bamboo", 2)})
AddDeconstructRecipe("ia_trident", {Ingredient("goldnugget", 12), Ingredient("needlespear", 3)})
AddDeconstructRecipe("snakeoil", {})
AddDeconstructRecipe("peg_leg", {Ingredient("log", 1)})
AddDeconstructRecipe("turbine_blades", {Ingredient("trinket_17", 3)}) -- trinket_17 - Bent Spork
AddDeconstructRecipe("magic_seal", {Ingredient("purplegem", 2), Ingredient("nightmarefuel", 4), Ingredient("meat", 1)})
AddDeconstructRecipe("harpoon", {Ingredient("rope", 1), Ingredient("twigs", 2), Ingredient("houndstooth", 3)})
AddDeconstructRecipe("barrel_gunpowder", {Ingredient("boards", 3), Ingredient("gunpowder", 2)})
AddDeconstructRecipe("barrel_gunpowder_land", {Ingredient("boards", 3), Ingredient("gunpowder", 2)})
AddDeconstructRecipe("krakenchest", {Ingredient("boards", 4), Ingredient("tentaclespots", 2), Ingredient("boneshard", 1)})
AddDeconstructRecipe("luggagechest", {Ingredient("boards", 2), Ingredient("fabric", 1)})
AddDeconstructRecipe("boat_surfboard", {Ingredient("boards", 1), Ingredient("seashell", 2)})

--WINTERSFEASTCOOKING

AddRecipePostInit("wintercooking_berrysauce", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("mosquitosack")
    if ingredient then
        ingredient:AddDictionaryPrefab("mosquitosack_yellow")
    end
end)
AddRecipePostInit("wintercooking_bibingka", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("foliage")
    if ingredient then
        ingredient:AddDictionaryPrefab("jungletreeseed")
    end
end)
AddRecipePostInit("wintercooking_lutefisk", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("driftwood_log")
    if ingredient then
        ingredient:AddDictionaryPrefab("palmleaf")
    end
end)
AddRecipePostInit("wintercooking_pavlova", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("moon_tree_blossom")
    if ingredient then
        ingredient:AddDictionaryPrefab("hail_ice")
    end
end)
AddRecipePostInit("wintercooking_pumpkinpie", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("phlegm")
    if ingredient then
        ingredient:AddDictionaryPrefab("venomgland")
    end
end)
AddRecipePostInit("wintercooking_tourtiere", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("acorn")
    if ingredient then
        ingredient:AddDictionaryPrefab("coconut")
    end
    local ingredient = recipe:FindAndConvertIngredient("pinecone")
    if ingredient then
        ingredient:AddDictionaryPrefab("jungletreeseed")
    end
end)


--OBSIDIAN
AddRecipe2("obsidianaxe", {Ingredient("axe", 1), Ingredient("obsidian", 2), Ingredient("dragoonheart", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("obsidianaxe", "eyeturret_item", "CRAFTING_STATION")
AddRecipe2("obsidianmachete", {Ingredient("machete", 1), Ingredient("obsidian", 3), Ingredient("dragoonheart", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("obsidianmachete", "obsidianaxe", "CRAFTING_STATION")
AddRecipe2("spear_obsidian", {Ingredient("spear", 1), Ingredient("obsidian", 3), Ingredient("dragoonheart", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("spear_obsidian", "obsidianmachete", "CRAFTING_STATION")
AddRecipe2("volcanostaff", {Ingredient("firestaff", 1), Ingredient("obsidian", 4), Ingredient("dragoonheart", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("volcanostaff", "spear_obsidian", "CRAFTING_STATION")
AddRecipe2("armorobsidian", {Ingredient("armorwood", 1), Ingredient("obsidian", 5), Ingredient("dragoonheart", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("armorobsidian", "volcanostaff", "CRAFTING_STATION")
AddRecipe2("obsidiancoconade", {Ingredient("coconade", 3), Ingredient("obsidian", 3), Ingredient("dragoonheart", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true, numtogive=3}, {"CRAFTING_STATION"})
SortAfter("obsidiancoconade", "armorobsidian", "CRAFTING_STATION")
AddRecipe2("wind_conch", {Ingredient("obsidian", 4), Ingredient("purplegem", 1), Ingredient("magic_seal", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("wind_conch", "obsidiancoconade", "CRAFTING_STATION")
AddRecipe2("windstaff", {Ingredient("obsidian", 2), Ingredient("nightmarefuel", 3), Ingredient("magic_seal", 1)}, TECH.OBSIDIAN_TWO, {nounlock=true}, {"CRAFTING_STATION"})
SortAfter("windstaff", "wind_conch", "CRAFTING_STATION")

-- WALANI
AddCharacterRecipe("surfboard_item", {Ingredient("boards", 1), Ingredient("seashell", 2)}, TECH.NONE,  {builder_tag = "surfer"})
SortAfter("surfboard_item", "mermhat", "CHARACTER")
--WICKERBOTTOM

AddCharacterRecipe("book_meteor", {Ingredient("papyrus", 2), Ingredient("obsidian", 2)}, TECH.SCIENCE_THREE, {builder_tag="bookbuilder"})
SortAfter("book_meteor", "book_sleep", "CHARACTER")
AddRecipePostInit("book_light", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("lightbulb")
    if ingredient then
        ingredient:AddDictionaryPrefab("rainbowjellyfish_dead")
    end
end)
AddRecipePostInit("book_fish", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("oceanfishingbobber_ball")
    if ingredient then
        ingredient:AddDictionaryPrefab("roe")
    end
end)
AddRecipePostInit("book_rain", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("goose_feather")
    if ingredient then
        ingredient:AddDictionaryPrefab("doydoyfeather")
    end
end)
AddRecipePostInit("book_moon", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("opalpreciousgem")
    if ingredient then
        ingredient:AddDictionaryPrefab("magic_seal")
    end
end)

--MADSCIENCE

AddRecipePostInit("halloween_experiment_bravery", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("froglegs")
    if ingredient then
        ingredient:AddDictionaryPrefab("snakeskin")
    end
end)
AddRecipePostInit("halloween_experiment_health", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("mosquito")
    if ingredient then
        ingredient:AddDictionaryPrefab("mosquito_yellow")
    end
end)
AddRecipePostInit("halloween_experiment_sanity", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("crow")
    if ingredient then
        ingredient:AddDictionaryPrefab("toucan")
    end
end)
AddRecipePostInit("halloween_experiment_root", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("batwing")
    if ingredient then
        ingredient:AddDictionaryPrefab("needlespear")
    end
end)

--WILLOW

AddRecipePostInit("bernie_inactive", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("beefalowool")
    if ingredient then
        ingredient:AddDictionaryPrefab("fabric")
    end
end)

--WOLFGANG
AddRecipePostInit("dumbbell_marble", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("marble")
    if ingredient then
        ingredient:AddDictionaryPrefab("limestonenugget")
    end
end)
AddRecipePostInit("dumbbell_gem", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("thulecite")
    if ingredient then
        ingredient:AddDictionaryPrefab("obsidian")
    end
end)
--WURT

local fish = IA_CONFIG.pondfishable and "pondfish_tropical" or "fish_tropical"
AddCharacterRecipe("mermhouse_fisher_crafted", {Ingredient(fish, 2), Ingredient("cutreeds", 3), Ingredient("fishingrod", 2), Ingredient("boards", 4)}, TECH.SCIENCE_ONE, {placer = "mermhouse_fisher_crafted_placer", product = "mermhouse_fisher_crafted", builder_tag="merm_builder"}, {"STRUCTURES"})
SortAfter("mermhouse_fisher_crafted", "mermwatchtower", "CHARACTER")
SortAfter("mermhouse_fisher_crafted", "mermwatchtower", "STRUCTURES")

AddCharacterRecipe("wurt_turf_tidalmarsh", {Ingredient("cutreeds", 1), Ingredient("spoiled_food", 2)},  TECH.NONE, {builder_tag="merm_builder", product="turf_tidalmarsh", numtogive = 4})
SortAfter("wurt_turf_tidalmarsh", "wurt_turf_marsh", "CHARACTER")

AddRecipePostInit("mermhouse_crafted", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("pondfish")
    if ingredient then
        ingredient:AddDictionaryPrefab(fish)
    end
end)
AddRecipePostInit("mermhat", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("pondfish")
    if ingredient then
        ingredient:AddDictionaryPrefab(fish)
    end
end)

AllRecipes["mermthrone"].ingredients = {
    Ingredient("cutreeds", 20),
    Ingredient("pigskin", 10),
    Ingredient("silk", 15)
}
CONSTRUCTION_PLANS["mermthrone_construction"] = {
    Ingredient("cutreeds", 20),
    Ingredient("pigskin", 10),
    Ingredient("silk", 15)
}
AllRecipes["mermhouse_crafted"].testfn = IsSWMarshLand
AllRecipes["mermthrone_construction"].testfn = IsSWMarshLand
AllRecipes["mermwatchtower"].testfn = IsSWMarshLand
AllRecipes["mermhouse_fisher_crafted"].testfn = IsSWMarshLand

--WORMWOOD

AddCharacterRecipe("poisonbalm", {Ingredient("livinglog", 1), Ingredient("venomgland", 1)}, TECH.NONE, {builder_tag="plantkin"}, {"RESTORATION"})
SortAfter("poisonbalm", "antivenom", "RESTORATION")
SortAfter("poisonbalm", "livinglog", "CHARACTER")
--WAXWELL

AddRecipe2("shadowhacker_builder",  {Ingredient("nightmarefuel", 2), Ingredient("machete", 1), Ingredient(CHARACTER_INGREDIENT.MAX_SANITY, TUNING.SHADOWWAXWELL_SANITY_PENALTY.SHADOWHACKER)}, TECH.SHADOW_TWO, {builder_tag="shadowmagic", nounlock=true}, {"CRAFTING_STATION"})
SortAfter("shadowhacker_builder", "shadowlumber_builder", "CRAFTING_STATION")
--WEBBER

AddCharacterRecipe("mutator_tropical_spider_warrior", { Ingredient("monstermeat", 2), Ingredient("silk", 1), Ingredient("venomgland", 1)}, TECH.SPIDERCRAFT_ONE, {builder_tag="spiderwhisperer"})
SortAfter("mutator_tropical_spider_warrior", "mutator_warrior", "CHARACTER")
--WIGFRID

AddRecipePostInit("battlesong_sanitygain", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("moonbutterflywings")
    if ingredient then
        ingredient:AddDictionaryPrefab("coral_brain")
    end
end)
AddRecipePostInit("battlesong_sanityaura", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("nightmare_timepiece")
    if ingredient then
        ingredient:AddDictionaryPrefab("doydoybaby")
    end
end)
AddRecipePostInit("battlesong_fireresistance", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("oceanfish_small_9_inv")
    if ingredient then
        ingredient:AddDictionaryPrefab("pondneon_quattro")
    end
end)

--WALTER

AddRecipePostInit("slingshotammo_marble", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("marble")
    if ingredient then
        ingredient:AddDictionaryPrefab("limestonenugget")
    end
end)
AddRecipePostInit("slingshotammo_thulecite", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("thulecite_pieces")
    if ingredient then
        ingredient:AddDictionaryPrefab("obsidian")
    end
end)
--Placeholder until we will add obsidian ammo
AddCharacterRecipe("slingshotammo_obsidian", {Ingredient("thulecite_pieces", 1), Ingredient("nightmarefuel", 1)}, TECH.OBSIDIAN_TWO, {builder_tag="pebblemaker", product="slingshotammo_thulecite", numtogive = 10, no_deconstruction=true, nounlock=true}, {"CRAFTING_STATION"})
SortAfter("slingshotammo_obsidian", "slingshotammo_thulecite", "CHARACTER")
AddRecipePostInit("slingshotammo_obsidian", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("thulecite_pieces")
    if ingredient then
        ingredient:AddDictionaryPrefab("obsidian")
    end
end)
AddRecipePostInit("slingshot", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("mosquitosack")
    if ingredient then
        ingredient:AddDictionaryPrefab("mosquitosack_yellow")
    end
end)

--WANDA

AddRecipePostInit("pocketwatch_parts", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("thulecite_pieces")
    if ingredient then
        ingredient:AddDictionaryPrefab("dubloon")
    end
end)
AddRecipePostInit("pocketwatch_heal", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("marble")
    if ingredient then
        ingredient:AddDictionaryPrefab("limestonenugget")
    end
end)
AddRecipePostInit("pocketwatch_recall", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("walrus_tusk")
    if ingredient then
        ingredient:AddDictionaryPrefab("ox_horn")
    end
end)
AddRecipePostInit("pocketwatch_weapon", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("marble")
    if ingredient then
        ingredient:AddDictionaryPrefab("limestonenugget")
    end
end)

--WX-78

AddRecipePostInit("wx78module_light", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("lightbulb")
    if ingredient then
        ingredient:AddDictionaryPrefab("rainbowjellyfish_dead")
    end
end)
AddRecipePostInit("wx78module_nightvision", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("mole")
    if ingredient then
        ingredient:AddDictionaryPrefab("blowdart_flup")
    end
    local ingredient = recipe:FindAndConvertIngredient("fireflies")
    if ingredient then
        ingredient:AddDictionaryPrefab("bioluminescence")
    end
end)
AddRecipePostInit("wx78module_movespeed", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("rabbit")
    if ingredient then
        ingredient:AddDictionaryPrefab("crab")
    end
end)
AddRecipePostInit("wx78module_maxhunger", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("slurper_pelt")
    if ingredient then
        ingredient:AddDictionaryPrefab("doydoyfeather")
    end
end)
AddRecipePostInit("wx78module_music", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("singingshell_octave3")
    if ingredient then
        ingredient:AddDictionaryPrefab("seashell")
    end
end)
AddRecipePostInit("wx78module_taser", function(recipe)
    local ingredient = recipe:FindAndConvertIngredient("goatmilk")
    if ingredient then
        ingredient:AddDictionaryPrefab("jellyfish_dead")
    end
end)

--[[
Recipe("woodlegshat", {Ingredient("fabric", 3), Ingredient("boneshard", 4), Ingredient("dubloon", 10)}, RECIPETABS.NAUTICAL, TECH.NONE, RECIPE_GAME_TYPE.SHIPWRECKED)
Recipe("woodlegsboat", {Ingredient("boatcannon", 1), Ingredient("boards", 4), Ingredient("dubloon", 4)}, RECIPETABS.NAUTICAL, TECH.NONE, RECIPE_GAME_TYPE.SHIPWRECKED, "woodlegsboat_placer", nil, nil, nil, true, 4)
]]
