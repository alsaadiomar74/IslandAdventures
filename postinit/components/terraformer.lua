local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local Terraformer = require("components/terraformer")

local function GetBaseTile(tile, x, z)
	local climate = GetClimate(Vector3(x, 0, z))
	local basetile = WORLD_TILES.DIRT
	if IsClimate(climate, "volcano") then
		basetile = WORLD_TILES.VOLCANO_ROCK
	elseif IsClimate(climate, "island") then
		basetile = WORLD_TILES.BEACH
	--elseif tile == WORLD_TILES.PIGRUINS then
	--	basetile = WORLD_TILES.DEEPRAINFOREST
	end
	return basetile
end

local _Terraform = Terraformer.Terraform
function Terraformer:Terraform(pt, doer, ...)
    local world = TheWorld
    local map = world.Map

	local _x, _y, _z = pt:Get()
    local x, y = map:GetTileCoordsAtPoint(_x, _y, _z)

	local below_soil_turf
	if map:GetTileAtPoint(_x, _y, _z) == WORLD_TILES.FARMING_SOIL and TheWorld.components.farming_manager then
		below_soil_turf = TheWorld.components.farming_manager:GetTileBelowSoil(x, y)
	end

	local tile = self.turf or below_soil_turf or WORLD_TILES.DIRT
	local basetile = tile == WORLD_TILES.DIRT and GetBaseTile(tile, x, y) or nil
	if basetile ~= nil then
		tile = self.turf
		self.turf = basetile
	end
	local ret = {_Terraform(self, pt, doer, ...)}
	if basetile ~= nil then
		self.turf = tile
	end
	return unpack(ret)
end
