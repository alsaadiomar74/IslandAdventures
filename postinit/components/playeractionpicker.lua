local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local PlayerActionPicker = require("components/playeractionpicker")

--replacing the function twice, so logic doesnt need to get copy pasted -Z
local _GetLeftClickActions = PlayerActionPicker.GetLeftClickActions
local function __GetLeftClickActions(self, position, target, ...)
    if self.leftclickoverride ~= nil then
        local actions, usedefault = self.leftclickoverride(self.inst, target, position)
        if not usedefault or (actions ~= nil and #actions > 0) then
            return _GetLeftClickActions(self, position, target, ...)
        end
    end

    local actions = nil
    local useitem = self.inst.replica.inventory:GetActiveItem()
    local equipitem = self.inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    local boatitem = self.inst.replica.sailor and self.inst.replica.sailor:GetBoat() and self.inst.replica.sailor:GetBoat().replica.container and self.inst.replica.sailor:GetBoat().replica.container:GetItemInBoatSlot(BOATEQUIPSLOTS.BOAT_LAMP)
    local ispassable = self.map:IsPassableAtPoint(position:Get())

	self.disable_right_click = false

	local steering_actions = self:GetSteeringActions(self.inst, position)
	if steering_actions ~= nil then
		-- self.disable_right_click = true
		return steering_actions
	end

    --if we're specifically using an item, see if we can use it on the target entity
    if useitem ~= nil then
        return _GetLeftClickActions(self, position, target, ...)
    elseif target ~= nil and target ~= self.inst then
        --if we're clicking on a scene entity, see if we can use our equipped object on it, or just use it
        if self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_INSPECT) and
            target:HasTag("inspectable") and
            (self.inst.CanExamine == nil or self.inst:CanExamine()) and
            (self.inst.sg == nil or self.inst.sg:HasStateTag("moving") or self.inst.sg:HasStateTag("idle") or self.inst.sg:HasStateTag("channeling")) and
            (self.inst:HasTag("moving") or self.inst:HasTag("idle") or self.inst:HasTag("channeling")) then
            return _GetLeftClickActions(self, position, target, ...)
        elseif self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) and target.replica.combat ~= nil and self.inst.replica.combat:CanTarget(target) then
            return _GetLeftClickActions(self, position, target, ...)
        elseif equipitem ~= nil and equipitem:IsValid() and not boatitem then
            return _GetLeftClickActions(self, position, target, ...)
        elseif boatitem ~= nil and boatitem:IsValid() and not equipitem then
            actions = self:GetEquippedItemActions(target, boatitem)
        elseif equipitem ~= nil and equipitem:IsValid() and boatitem ~= nil and boatitem:IsValid() then
            local equip_act = self:GetEquippedItemActions(target, equipitem)

            if self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) or GetTableSize(equip_act) == 0 then
                actions = self:GetEquippedItemActions(target, boatitem)
            end

            if not actions or (not self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) and GetTableSize(equip_act) > 0) then
                return _GetLeftClickActions(self, position, target, ...)
            end
        end

        if actions == nil or #actions == 0 then
            return _GetLeftClickActions(self, position, target, ...)
        end
    end

    if actions == nil and target == nil and ispassable then
        if equipitem ~= nil and equipitem:IsValid() and not boatitem then
            return _GetLeftClickActions(self, position, target, ...)
        elseif boatitem ~= nil and boatitem:IsValid() and not equipitem then
            actions = self:GetPointActions(position, boatitem)
        elseif equipitem ~= nil and equipitem:IsValid() and boatitem ~= nil and boatitem:IsValid() then
            local equip_act = self:GetPointActions(position, equipitem)

            if self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) or GetTableSize(equip_act) == 0 then
                actions = self:GetPointActions(position, boatitem)
            end

            if not actions or (not self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) and GetTableSize(equip_act) > 0) then
                return _GetLeftClickActions(self, position, target, ...)
            end
        end
        --this is to make it so you don't auto-drop equipped items when you left click the ground. kinda ugly.
        if actions ~= nil then
            for i, v in ipairs(actions) do
                if v.action == ACTIONS.DROP then
                    table.remove(actions, i)
                    break
                end
            end
        end
        if actions == nil or #actions <= 0 then
            return _GetLeftClickActions(self, position, target, ...)
        end
    end

    return actions or {}
end

local _GetRightClickActions = PlayerActionPicker.GetRightClickActions
local function __GetRightClickActions(self, position, target, ...)
	if self.disable_right_click then
		return _GetRightClickActions(self, position, target, ...)
	end
    if self.rightclickoverride ~= nil then
        local actions, usedefault = self.rightclickoverride(self.inst, target, position)
        if not usedefault or (actions ~= nil and #actions > 0) then
            return _GetRightClickActions(self, position, target, ...)
        end
    end

    local steering_actions = self:GetSteeringActions(self.inst, position, true)
    if steering_actions ~= nil then
        --self.disable_right_click = true
        return steering_actions
    end

    local actions = nil
    local useitem = self.inst.replica.inventory:GetActiveItem()
    local equipitem = self.inst.replica.inventory:GetEquippedItem(EQUIPSLOTS.HANDS)
    local boatitem = self.inst.replica.sailor and self.inst.replica.sailor:GetBoat() and self.inst.replica.sailor:GetBoat().replica.container and self.inst.replica.sailor:GetBoat().replica.container:GetItemInBoatSlot(BOATEQUIPSLOTS.BOAT_LAMP)
    local ispassable = self.map:IsPassableAtPoint(position:Get())

    if target ~= nil and self.containers[target] then
        return _GetRightClickActions(self, position, target, ...)
    elseif useitem ~= nil then
        return _GetRightClickActions(self, position, target, ...)
    elseif target ~= nil and not target:HasTag("walkableplatform") then

        if equipitem ~= nil and equipitem:IsValid() and not boatitem then
            return _GetRightClickActions(self, position, target, ...)
        elseif boatitem ~= nil and boatitem:IsValid() and not equipitem then
            actions = self:GetEquippedItemActions(target, boatitem, true)

            --strip out all other actions for weapons with right click special attacks
            if boatitem.components.aoetargeting ~= nil then
                return (#actions <= 0 or actions[1].action == ACTIONS.CASTAOE) and actions or {}
            end
        elseif equipitem ~= nil and equipitem:IsValid() and boatitem ~= nil and boatitem:IsValid() then
            local equip_act = self:GetEquippedItemActions(target, equipitem, true)

            if self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) or GetTableSize(equip_act) == 0 then
                actions = self:GetEquippedItemActions(target, boatitem, true)
                --strip out all other actions for weapons with right click special attacks
                if boatitem.components.aoetargeting ~= nil then
                    return (#actions <= 0 or actions[1].action == ACTIONS.CASTAOE) and actions or {}
                end
            end

            if not actions or (not self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) and GetTableSize(equip_act) > 0) then
                return _GetRightClickActions(self, position, target, ...)
            end
        end

        if actions == nil or #actions == 0 then
            return _GetRightClickActions(self, position, target, ...)
        end
    elseif ((equipitem ~= nil and equipitem:IsValid()) or (boatitem ~= nil and boatitem:IsValid())) and (ispassable or (equipitem and equipitem:HasTag("allow_action_on_impassable")) or
        ((equipitem ~= nil and equipitem.components.aoetargeting ~= nil and equipitem.components.aoetargeting.alwaysvalid and equipitem.components.aoetargeting:IsEnabled()) or
        (boatitem ~= nil and boatitem.components.aoetargeting ~= nil and boatitem.components.aoetargeting.alwaysvalid and boatitem.components.aoetargeting:IsEnabled()))) then
        --can we use our equipped item at the point?

        if (equipitem and equipitem:IsValid()) and not boatitem then
            return _GetRightClickActions(self, position, target, ...)
        elseif (boatitem and boatitem:IsValid()) and not equipitem then
            actions = self:GetPointActions(position, boatitem, true, target)
        elseif (equipitem and equipitem:IsValid()) and (boatitem and boatitem:IsValid()) then
            local equip_act = self:GetPointActions(position, equipitem, true, target)

            if self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) or GetTableSize(equip_act) == 0 then
                actions = self:GetPointActions(position, boatitem, true, target)
            end

            if not actions or (not self.inst.components.playercontroller:IsControlPressed(CONTROL_FORCE_ATTACK) and GetTableSize(equip_act) > 0) then
                return _GetRightClickActions(self, position, target, ...)
            end
        end
    end
    if (actions == nil or #actions <= 0) and (target == nil or target:HasTag("walkableplatform")) and ispassable then
        actions = self:GetPointSpecialActions(position, useitem, true)
    end

    if (actions == nil or #actions <= 0) and target == nil and ispassable then
        return _GetRightClickActions(self, position, target, ...)
    end

    return actions or {}
end

local function IsLandRoT(tile)
	return IsLand(tile) and not IsOceanTile(tile)
end

local function ShouldRemoveActionsOverBoundaries(actions)
    if not actions or #actions == 0 then
        return true
    end
    local first_action = actions[1].action
    return not (first_action.instant or first_action.crosseswaterboundaries)
end

function PlayerActionPicker:GetLeftClickActions(position, target, ...)
    local actions = __GetLeftClickActions(self, position, target, ...)

    if TheInput:ControllerAttached() then
        return actions
    end

    local x, _, z = self.inst.Transform:GetWorldPosition()
    -- Lenient player check for water/land, also force the player to be "on land"(if they are technically on water) or "on water"(if they are technically on land) if they have the sailor component.
    local IsWaterBased = IsWaterAny(GetVisualTileType(x, 0, z, 1.25 / 4)) and (not self.inst:HasTag("_sailor") or self.inst:HasTag("sailing"))
    local IsLandBased = IsLandRoT(GetVisualTileType(x, 0, z, 0.25 / 4)) and (not self.inst:HasTag("_sailor") or not self.inst:HasTag("sailing"))
    -- Tight cursor check for water/land
    local IsCursorWet = IsWaterAny(GetVisualTileType(position.x, 0, position.z, 0.001 / 4))
    local IsCursorDry = IsLandRoT(GetVisualTileType(position.x, 0, position.z, 1.5 / 4))
    local IsDstBoat = TheWorld.Map:GetPlatformAtPoint(position.x, 0, position.z) -- DST boat

    if IsWaterBased and (not IsCursorWet or IsDstBoat) and (target == nil or not target:CanOnWater()) then
        if ShouldRemoveActionsOverBoundaries(actions) then
            if self.inst:HasTag("_sailor") and self.inst:HasTag("sailing") and TheWorld.Map:IsValidTileAtPoint(position.x, 0, position.z) then
                -- Find the landing position, where water meets the land
                local pos = self.inst:GetPosition()
                local dir = (position - pos):GetNormalized()
                local dist = (position - pos):Length()
                local step = 0.25
                local num_steps = dist / step

                local landing_pos
                local is_dst_boat

                for i = 0, num_steps, 1 do
                    local test_pos = pos + dir * step * i
                    local test_tile = TheWorld.Map:GetTileAtPoint(test_pos.x , test_pos.y, test_pos.z)
                    is_dst_boat = TheWorld.Map:GetPlatformAtPoint(test_pos.x , test_pos.y, test_pos.z) -- DST boat
                    if not IsWaterAny(test_tile) or is_dst_boat then
                        landing_pos = test_pos
                        break
                    end
                end
                if landing_pos then
                    if not is_dst_boat then
                        landing_pos.x, landing_pos.y, landing_pos.z = TheWorld.Map:GetTileCenterPoint(landing_pos.x, 0, landing_pos.z)
                    end
                    --OKAY! if the disembark action still causes a rare crash due to (*** failed to evaluate *** ) then we are doomed and this mod is cursed -Half
                    if type(landing_pos) == "table" and landing_pos.x ~= nil and landing_pos.y ~= nil and landing_pos.z ~= nil then
                        actions = { BufferedAction(self.inst, nil, ACTIONS.DISEMBARK, nil, landing_pos) }
                    end
                end
            else
                actions = nil
            end
        end
    elseif IsLandBased and not IsCursorDry and (target == nil or not target:CanOnLand()) then
        if ShouldRemoveActionsOverBoundaries(actions) then
            actions = nil
        end
    end

    return actions or {}
end

function PlayerActionPicker:GetRightClickActions(position, target, ...)
    local actions = __GetRightClickActions(self, position, target, ...)

    if TheInput:ControllerAttached() then
        return actions
    end

    local x, _, z = self.inst.Transform:GetWorldPosition()
    --lenient player check for water/land, also force the player to be "on land"(if they are technically on water) or "on water"(if they are technically on land) if they have the sailor component.
    local IsWaterBased = IsWater(GetVisualTileType(x, 0, z, 1.25 / 4)) and (not self.inst:HasTag("_sailor") or self.inst:HasTag("sailing"))
    local IsLandBased = IsLand(GetVisualTileType(x, 0, z, 0.25 / 4)) and (not self.inst:HasTag("_sailor") or not self.inst:HasTag("sailing"))
    --tight cursor check for water/land
    local IsCursorWet = IsWater(GetVisualTileType(position.x, 0, position.z, 0.001 / 4))
    local IsCursorDry = IsLand(GetVisualTileType(position.x, 0, position.z, 1.5 / 4))

    if IsWaterBased and not IsCursorWet and (target == nil or not target:CanOnWater()) then
        if ShouldRemoveActionsOverBoundaries(actions) then
            actions = nil
        end
    elseif IsLandBased and not IsCursorDry and (target == nil or not target:CanOnLand()) then
        if ShouldRemoveActionsOverBoundaries(actions) then
            actions = nil
        end
    end

    return actions or {}
end

local _SortActionList = PlayerActionPicker.SortActionList
function PlayerActionPicker:SortActionList(actions, target, ...)
    local ret = _SortActionList(self, actions, target, ...)

    for i, action in ipairs(ret) do
        if action.action == ACTIONS.DEPLOY and action.invobject.replica.inventoryitem then
            local deploydistance = action.invobject.replica.inventoryitem:GetDeployDist()
            if deploydistance ~= 0 then
                action.distance = deploydistance
            end
        end
    end

    return ret
end
