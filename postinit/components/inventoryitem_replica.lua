local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local InventoryItem = require("components/inventoryitem_replica")

local _CanDeploy = InventoryItem.CanDeploy
function InventoryItem:CanDeploy(pt, ...)
	local result = _CanDeploy(self, pt, ...)

	if result and self.inst.components.deployable == nil then
		-- The item is deployable, but we still need to check for ocean
		-- This is client-side by the way
		local tile = TheWorld.Map:GetTileAtPoint(pt:Get())
		local tileinfo = GetTileInfo(tile)

		return (
            not tileinfo
            or (self.classified.candeployonland:value() and tileinfo.land ~= false)
            or (self.classified.candeployonshallowocean:value() and tile == WORLD_TILES.OCEAN_SHALLOW)
            or (
                tileinfo.water and (
                    (self.classified.candeployonbuildableocean:value() and tileinfo.buildable) or
                    (self.classified.candeployonunbuildableocean:value() and not tileinfo.buildable)
                )
            )
        ) and result or false
	end

	return result
end

function InventoryItem:SetLongPickup(longpickup)
    self.classified.longpickup:set(longpickup)
end

function InventoryItem:CanLongPickup()
    return self.classified ~= nil and self.classified.longpickup:value()
end

function InventoryItem:GetDeployDist()
    if self.inst.components.deployable then
        return self.inst.components.deployable:GetDeployDist()
    end
    return self.classified ~= nil and self.classified.deploydistance:value() or 0
end

function InventoryItem:DeployAtRange()
    if self.inst.components.deployable then
        self.inst.components.deployable:DeployAtRange()
    end
    return self.classified ~= nil and self.classified.deployatrange:value() or false
end

local _SerializeUsage = InventoryItem.SerializeUsage
function InventoryItem:SerializeUsage(...)
    _SerializeUsage(self, ...)
    if self.inst.components.obsidiantool then
        local charge, maxcharge = self.inst.components.obsidiantool:GetCharge()
        self.classified:SerializeObsidianCharge(charge / maxcharge)
    else
        self.classified:SerializeObsidianCharge(nil)
    end

    if self.inst.components.inventory then
        self.classified:SerializeInvSpace(self.inst.components.inventory:NumItems() / self.inst.components.inventory.maxslots)
    else
        self.classified:SerializeInvSpace(nil)
    end

    if self.inst.components.fuse then
        self.classified:SerializeFuse(self.inst.components.fuse.consuming and self.inst.components.fuse.fusetime or 0)
    else
        self.classified:SerializeFuse(nil)
    end
end

local _DeserializeUsage = InventoryItem.DeserializeUsage
function InventoryItem:DeserializeUsage(...)
    _DeserializeUsage(self, ...)
    if self.classified ~= nil then
        self.classified:DeserializeObsidianCharge()
        self.classified:DeserializeInvSpace()
        self.classified:DeserializeFuse()
    end
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

IAENV.AddClassPostConstruct("components/inventoryitem_replica", function(cmp)

    local _SetOwner = cmp.SetOwner
    function cmp:SetOwner(owner, ...)
        local boat_owner = owner ~= nil and owner:HasTag("boatcontainer") and owner.components.container ~= nil and owner.components.container.opener
        if boat_owner then
            if self.inst.Network ~= nil then
                self.inst.Network:SetClassifiedTarget(boat_owner)
            end
            if self.classified ~= nil then
                self.classified.Network:SetClassifiedTarget(boat_owner or self.inst)
            end
            return
        end
        return _SetOwner(self, owner, ...)
    end

    if TheWorld.ismastersim then
        cmp.inst:ListenForEvent("obsidianchargechange", function(inst, data)
    		cmp.classified:SerializeObsidianCharge(data.percent)
    	end)
        cmp.inst:ListenForEvent("invspacechange", function(inst, data)
            cmp.classified:SerializeInvSpace(data.percent)
        end)
        cmp.inst:ListenForEvent("fusechange", function(inst, data)
            cmp.classified:SerializeFuse(data.time)
        end)

    	local deployable = cmp.inst.components.deployable
    	if deployable ~= nil then
    		cmp.classified.deployatrange:set(deployable.deployatrange)
    		cmp.classified.candeployonland:set(deployable.candeployonland)
    		cmp.classified.candeployonshallowocean:set(deployable.candeployonshallowocean)
    		cmp.classified.candeployonbuildableocean:set(deployable.candeployonbuildableocean)
    		cmp.classified.candeployonunbuildableocean:set(deployable.candeployonunbuildableocean)
            cmp.classified.deploydistance:set(deployable.deploydistance)
    	end
    end

end)
