local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local _OnLoad = nil

IAENV.AddComponentPostInit("moonstormmanager", function(cmp, inst)
    if not TheWorld.ismastersim then
        return
    end

    local function SendStartTheMoonstormsToShard()
        local  _alterguardian_defeated_count = UpvalueHacker.GetUpvalue(cmp.OnSave, "_alterguardian_defeated_count")
        for id in pairs(Shard_GetConnectedShards()) do
            SendModRPCToShard(SHARD_MOD_RPC["Island Adventure"]["TheMoonstorms"], id, true, _alterguardian_defeated_count)
        end
    end

    inst:ListenForEvent("ms_startthemoonstorms", SendStartTheMoonstormsToShard)

    local function SendStopTheMoonstormsToShard()
        for id in pairs(Shard_GetConnectedShards()) do
            SendModRPCToShard(SHARD_MOD_RPC["Island Adventure"]["TheMoonstorms"], id, false)
        end
    end
    inst:ListenForEvent("ms_stopthemoonstorms", SendStopTheMoonstormsToShard)

    if not _OnLoad then
        _OnLoad = cmp.OnLoad or function() end
    end

    cmp.OnLoad = function(self, data, ...)
        if data ~= nil then
            if data._alterguardian_defeated_count then
                local _alterguardian_defeated_count = data._alterguardian_defeated_count
                if _alterguardian_defeated_count > 0 then
                    self.inst:DoTaskInTime(0, function()
                        for id in pairs(Shard_GetConnectedShards()) do
                            SendModRPCToShard(SHARD_MOD_RPC["Island Adventure"]["TheMoonstorms"], id, true, _alterguardian_defeated_count, true, data.moonstyle_altar)
                        end
                    end)
                end
            end
        end

        _OnLoad(self, data, ...)
    end
end)
