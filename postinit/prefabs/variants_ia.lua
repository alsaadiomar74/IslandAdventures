local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------

local function IsOnIATurf(inst)
	local tile = TheWorld.Map:GetTileAtPoint(inst:GetPosition():Get())
	--Hardcode beach because it is "climate neutral" for compatibility with old worlds (beach surrounds mainland) -M
	return CLIMATE_TURFS and tile and CLIMATE_TURFS.ISLAND[tile] or tile == WORLD_TILES.BEACH
end

local function ClimateFallback(inst, source)
	if source ~= nil then
		return IsInIAClimate(source)
	elseif inst ~= nil then
		return IsInIAClimate(inst)
	end
end
----------------------------------------------------------------------------------------

local POSSIBLE_VARIANTS = {
	grassgekko = {
		default = {build="grassgecko"},
		tropical = {build="grassgecko_green_build",testfn=IsOnIATurf},
	},
	grasspartfx = {
		default = {build="grass1"},
		tropical = {build="grassgreen_build",testfn=IsOnIATurf},
	}, --TODO make grasspartfx and grassgekko based on grass visualvariant not turf
	grass = {
		default = {build="grass1",minimap="grass.png"},
		tropical = {build="grassgreen_build",minimap="grass_tropical.tex",testfn=IsOnIATurf},
	},
	dug_grass = {
		default = {build="grass1",invatlas="default"},
		tropical = {build="grassgreen_build",invatlas="images/ia_inventoryimages.xml", sourceprefabs={
			"grass_water",
		},fallbackfn=ClimateFallback},
	},
	krampus = {
		default = {build="krampus_build"},
		tropical = {build="krampus_hawaiian_build",testfn=IsInIAClimate},
	},
	butterfly = {
		default = {build="butterfly_basic",invatlas="default"},
		tropical = {build="butterfly_tropical_basic",invatlas="images/ia_inventoryimages.xml",testfn=IsInIAClimate},
	},
	cutgrass = {
		default = {build="cutgrass",invatlas="default"},
		tropical = {build="cutgrassgreen",invatlas="images/ia_inventoryimages.xml", sourceprefabs={
			"grass_water",
		},fallbackfn=ClimateFallback},
	},
	butterflywings = {
		default = {build="butterfly_wings",bank="butterfly_wings",invatlas="default"},
		tropical = {build="butterfly_tropical_wings",bank="butterfly_tropical_wings",invatlas="images/ia_inventoryimages.xml",fallbackfn=ClimateFallback},
	},
	log = {
		default = {build="log",invatlas="default"},
		tropical = {build="log_tropical",invatlas="images/ia_inventoryimages.xml",sourceprefabs={
			"palmtree",
			"jungletree",
			"mangrovetree",
			"livingjungletree",
			"leif_palm",
			"leif_jungle",
			"wallyintro_debris_1",
			"wallyintro_debris_2",
			"wallyintro_debris_3",
		},fallbackfn=ClimateFallback},
	},
	cave_banana = {
		default = {name="default",build="cave_banana",invatlas="default"},
		tropical = {name="BANANA",build="bananas",invatlas="images/ia_inventoryimages.xml",sourceprefabs={
			"primeape",
			"primeapebarrel",
			"jungletree",
			"bananabush", --neat
			"leif_jungle",
			"treeguard_banana",
		},fallbackfn=ClimateFallback},
	},
	cave_banana_cooked = {
		default = {name="default",build="cave_banana",invatlas="default"},
		tropical = {name="BANANA_COOKED",build="bananas",invatlas="images/ia_inventoryimages.xml",fallbackfn=ClimateFallback},
	},
}

for name,variants in pairs(POSSIBLE_VARIANTS) do
	for variant,data in pairs(variants) do
		if data.invatlas then
			local atlas = data.invatlas == "default" and "images/inventoryimages.xml" or data.invatlas
			local fn = data.testfn or data.fallbackfn
			VARIANT_INVATLAS[name..".tex"] = VARIANT_INVATLAS[name..".tex"] or {}
			VARIANT_INVATLAS[name..".tex"][variant] = {invatlas = atlas, testfn = fn}
		end
	end
end

----------------------------------------------------------------------------------------

local function fn(inst)
	if TheWorld.ismastersim then
		if not inst.components.visualvariant then
			inst:AddComponent("visualvariant")
		end
		for k,v in pairs(POSSIBLE_VARIANTS[inst.prefab]) do
			--allow others to override us
			if not next(inst.components.visualvariant.possible_variants) then
				inst.components.visualvariant.possible_variants = POSSIBLE_VARIANTS[inst.prefab]
			elseif not inst.components.visualvariant.possible_variants[k] then
				inst.components.visualvariant.possible_variants[k] = v
			end
		end
	end
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

for k,v in pairs(POSSIBLE_VARIANTS) do
	IAENV.AddPrefabPostInit(k, fn)
end
