local IAENV = env
GLOBAL.setfenv(1, GLOBAL)
--------------------------------------------------------------

local function GetLeader(inst) -- Needed for FindEntityToWorkAction
    return inst.components.follower.leader
end

local SEE_WORK_DIST = 10 -- Needed for FindEntityToWorkAction
local KEEP_DANCING_DIST = 2 -- Needed for FindEntityToWorkAction
local TOWORK_CANT_TAGS = { "fire", "smolder", "event_trigger", "INLIMBO", "NOCLICK" } -- Needed for FindEntityToWorkAction
local function FindEntityToWorkAction(inst, action, addtltags) -- This was just taken directly from the shadow's brain file
    local leader = GetLeader(inst)
    if leader ~= nil then
        local target = inst.sg.statemem.target
        if target ~= nil and
            target:IsValid() and
            not (target:IsInLimbo() or
                target:HasTag("NOCLICK") or
                target:HasTag("event_trigger")) and
            target:IsOnValidGround() and
            target.components.workable ~= nil and
            target.components.workable:CanBeWorked() and
            target.components.workable:GetWorkAction() == action and
            not (target.components.burnable ~= nil
                and (target.components.burnable:IsBurning() or
                    target.components.burnable:IsSmoldering())) and
            target.entity:IsVisible() and
            target:IsNear(leader, KEEP_DANCING_DIST) then

            if addtltags ~= nil then
                for i, v in ipairs(addtltags) do
                    if target:HasTag(v) then
                        return BufferedAction(inst, target, action)
                    end
                end
            else
                return BufferedAction(inst, target, action)
            end
        end

        target = FindEntity(leader, SEE_WORK_DIST, nil, { action.id.."_workable" }, TOWORK_CANT_TAGS, addtltags)
        return target ~= nil and BufferedAction(inst, target, action) or nil
    end
end
------------------------------------------------------------------------------------------------------------
local DIG_TAGS = { "shadow_dig" } -- Add tags here
local HACK_TAGS = {"hack_workable"}
IAENV.AddBrainPostInit("shadowwaxwellbrain", function(brain) -- Here is the fun stuff, we will use this to add our new hacking to the brian.
    
    local LeaderInRangeGroup = nil -- Prep a variable to store the node that holds the digging/chopping/mining tasks.
    for i,node in ipairs(brain.bt.root.children) do
        if node.name == "Parallel" and node.children[1].name == "Leader In Range" then -- Find previously mentioned node
            LeaderInRangeGroup = node.children[2] -- Store it
        end
    end

    if LeaderInRangeGroup then -- If we had successfully found and stored the node in this variable, the following will happen
        local hackingnode = IfNode(function() return brain.inst.prefab == "shadowhacker" end, "Keep Hacking", -- Create a variable to hold a new If node.
            DoAction(brain.inst, function() return FindEntityToWorkAction(brain.inst, ACTIONS.HACK, HACK_TAGS) end) -- Same as digging and such, but for hacking.
        )
        table.insert(LeaderInRangeGroup.children, hackingnode) -- Smoothly slide this into the nodes next to the digging and such.
        local diggingnode = IfNode(function() return brain.inst.prefab == "shadowdigger" end, "Keep Digging", -- Create a variable to hold a new If node.
            DoAction(brain.inst, function() return FindEntityToWorkAction(brain.inst, ACTIONS.DIG, DIG_TAGS) end) --add a new node for digging that takes our new tag
        )
        table.insert(LeaderInRangeGroup.children, diggingnode)
    end
end)
