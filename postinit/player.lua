local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

----------------------------------------------------------------------------------------
local function removebuffs(inst, data)
	if inst.components.locomotor then
		inst.components.locomotor:RemoveExternalSpeedAdder(inst, "CAFFEINE")
		inst.components.locomotor:RemoveExternalSpeedAdder(inst, "SURF")
		inst.components.locomotor:RemoveExternalSpeedAdder(inst, "AUTODRY")
	end
end

local function blockPoison(inst, data)
	if inst.components.poisonable then
		inst.components.poisonable:SetBlockAll(true)
	end
end
local function unblockPoison(inst, data)
	if inst.components.poisonable and not inst:HasTag("beaver") then
		inst.components.poisonable:SetBlockAll(true)
	end
end

----------------------------------------------------------------------------------------

local function beachresurrect(inst)
	local oldpos = inst:GetPosition()
	--Step 1: find position
	local testwaterfn = function(offset)
		local test_point = oldpos + offset
		local tile = TheWorld.Map:GetTileAtPoint(test_point:Get())
		if IsWater(tile) or tile == WORLD_TILES.INVALID or tile == WORLD_TILES.IMPASSABLE then
			return false
		end

		local waterPos = FindValidPositionByFan(0, 12, 6, function(offset)
			return IsOnWater(test_point + offset)
		end)
		if waterPos == nil then
			return false
		end

		-- print("should be good... just checking for dangers now.")

		--TODO this is not Webber-friendly
		if #TheSim:FindEntities(oldpos.x, oldpos.y, oldpos.z, 12, nil, nil, {"fire", "hostile"}) > 0 then
			return false
		end

		return true
	end


	local pt
	for radius = 8, 508, 10 do
		local result_offset = FindValidPositionByFan(0, radius, 4 + 14 * math.floor(radius/508), testwaterfn)
		if result_offset then
			--we got a winner, stop the loop
			-- print("WE GOT A POINT AT RADIUS..."..radius)
			pt = oldpos + result_offset
			break
		end
	end
	if not pt then
		--try again, but farther
		for radius = 520, 3020, 25 do
			local result_offset = FindValidPositionByFan(0, radius, 16 + 16 * math.floor(radius/3020), testwaterfn)
			if result_offset then
				--we got a winner, stop the loop
				-- print("WE GOT A POINT AT RADIUS..."..radius)
				pt = oldpos + result_offset
				break
			end
		end
	end
	if not pt then
		--find a multiplayer_portal of any kind, those are safe
		for guid, ent in pairs(Ents) do
			if ent:IsValid() and ent.prefab and string.gsub(ent.prefab, 0, 18) == "multiplayer_portal" then
				pt = ent:GetPosition()
				break
			end
		end
	end
	if not pt then
		--failsafe to killing the player
		print("FATAL: Not able to beachresurrect "..tostring(inst)..". Kill them with \"drowning\" instead.")
		inst.components.health:DoDelta(-inst.components.health.currenthealth, false, "drowning", false, nil, true)
		return
	end

	--Step 2: put player there
	local crafting = GetValidRecipe("boat_lograft")
	if crafting and crafting.ingredients then
		for _, v in pairs(crafting.ingredients) do
			for i = 1, v.amount do
				local offset = FindValidPositionByFan(math.random()*2*PI, math.random()*2+2, 8, function(pos)
					return not IsOnWater(pos + pt)
				end)
				local item = SpawnPrefab(v.type)
				if offset then
					item.Transform:SetPosition((pt+offset):Get())
				else
					item.Transform:SetPosition(pt:Get())
				end
				item:Hide()
				item:DoTaskInTime(math.random()*3, function()
					item:Show()
					SpawnAt("sand_puff", item)
				end)
			end
		end
	end


	-- inst.components.hunger:Pause()
	inst:ScreenFade(false, .4, false)

	inst:DoTaskInTime(3, function()
		inst.Transform:SetPosition(pt:Get())
		SpawnAt("collapse_small", inst)
	end)

	inst:DoTaskInTime(3.4, function()

		inst:ScreenFade(true, .4, false)

		inst.sg:GoToState("wakeup")

		-- inst.components.hunger:Resume()

		if inst.components.health then
			inst.components.health:SetInvincible(false)
		end

		if inst.components.moisture then
			inst.components.moisture:SetPercent(1)
		end

		if not TheWorld.state.isday then
			SpawnAt("spawnlight_multiplayer", inst)
		end
	end)

end

----------------------------------------------------------------------------------------

local function newstate(inst, data)
	--data.statename
	if inst:HasTag("idle") then
		if inst._embarkingboat and inst._embarkingboat:IsValid() then
			inst.components.sailor:Embark(inst._embarkingboat)
			inst._embarkingboat = nil
		end
		inst:RemoveEventCallback("newstate", newstate)
	end
end

local function stop(inst)
	if inst.Physics then
		inst.Physics:Stop()
	end
end

local function GiveRaft(inst)
	if IA_CONFIG.newplayerboats and inst.components.builder then
		inst.components.builder:UnlockRecipe("boat_lograft")
		inst.components.builder.buffered_builds["boat_lograft"] = 0
		inst.replica.builder:SetIsBuildBuffered("boat_lograft", true)
	end
end


local function ms_respawnedfromghost(inst, data)
	if IsOnWater(inst) and inst.components.sailor and not inst:HasTag("wereplayer") then
		local boat = FindEntity(inst, 5, nil, {"sailable"}, {"INLIMBO", "fire", "NOCLICK"})
		if boat then
			boat.components.sailable.isembarking = true
			inst._embarkingboat = boat
			--Move there!
			inst:ForceFacePoint(boat:GetPosition():Get())
			local dist = inst:GetPosition():Dist(boat:GetPosition())
			inst.Physics:SetMotorVelOverride(dist / .8, 0, 0)
			inst:DoTaskInTime(.8, stop)
			--Drowning immunity appears to be not needed. -M
			inst:ListenForEvent("newstate", newstate)
		end
	end
	if IsInIAClimate(inst) then
		GiveRaft(inst)
	end
end

----------------------------------------------------------------------------------------

local _OnGotNewItem
local function OnGotNewItem(inst, data, ...)
	if (data.slot ~= nil or data.eslot ~= nil)
	and TheFocalPoint --just a small failsafe
	and IsOnWater(inst) then
		TheFocalPoint.SoundEmitter:PlaySound("ia/common/water_collect_resource")
	elseif _OnGotNewItem ~= nil then
		return _OnGotNewItem(inst, data, ...)
	end
end


----------------------------------------------------------------------------------------

local function WaterFailsafe(inst)
	local my_x, my_y, my_z = inst.Transform:GetWorldPosition()
	if not TheWorld.Map:IsPassableAtPoint(my_x, my_y, my_z)
	and not inst:HasTag("aquatic") then
	for k,v in pairs(Ents) do
			if v:IsValid() and v:HasTag("multiplayer_portal") then
				inst.Transform:SetPosition(v.Transform:GetWorldPosition())
				inst:SnapCamera()
			end
		end
	end
end

local OnSave_old
local function OnSave(inst, data)
	data.WarpOtherShardToWater = inst.WarpOtherShardToWater
	OnSave_old(inst, data)
end

local OnLoad_old
local function OnLoad(inst, data, ...)
	local pendingtasks = {}
	for per, _ in pairs(inst.pendingtasks) do
		pendingtasks[per] = true
	end
	OnLoad_old(inst, data, ...)
	local newtasks = {}
	for per, _ in pairs(inst.pendingtasks) do
		if not pendingtasks[per] and per.period == 0 and per.limit == 1 then
			table.insert(newtasks, per)
		end
	end
	if #newtasks == 1 then
		newtasks[1]:Cancel()
		--recreate with appropriate fn
		inst:DoTaskInTime(0, WaterFailsafe)
	end
	if data.WarpOtherShardToWater then -- Niko: If teleporting from another shard to water via Wanda's watches, prevent instant death.
		inst:DoTaskInTime(0, function()
			local x, y, z = inst.Transform:GetWorldPosition()
			if IsOnWater(x, y, z) then
				local boat = TheSim:FindEntities(x, y, z, TUNING.POCKETWATCH_BOATRANGE, {"sailable"})[1] or SpawnPrefab("boat_lograft")
				if boat ~= nil then
					inst.components.sailor:Embark(boat)
				end
			end
		end)
	end
end

local OnNewSpawn
local function OnNewSpawn_ia(inst, ...)
	local Starting_Items
	if IsInIAClimate(inst) then
		GiveRaft(inst)
		Starting_Items = deepcopy(TUNING.SEASONAL_STARTING_ITEMS)
		TUNING.SEASONAL_STARTING_ITEMS = TUNING.ISLAND_SEASONAL_STARTING_ITEMS
	end
	OnNewSpawn(inst, ...)
	if Starting_Items ~= nil then
		TUNING.SEASONAL_STARTING_ITEMS = Starting_Items
	end
end

local inventoryItemAtlasLookup = UpvalueHacker.GetUpvalue(GetInventoryItemAtlas, "inventoryItemAtlasLookup")
local function OnClimateChange(inst)
    if inst ~= ThePlayer then
        return
    end
    for imagename,possible_variants in pairs(VARIANT_INVATLAS) do
        local valid_variants = {}
        local desired_variants = {}
        for k, v in pairs(possible_variants) do
            if v.invatlas then
                if v.testfn and v.testfn(inst) then
                    table.insert(desired_variants, k)
                else
                    table.insert(valid_variants, k)
                end
            end
        end

        if #desired_variants > 0 then
            --if default is desired, pick that
            if table.contains(desired_variants, "default") then
                inventoryItemAtlasLookup[imagename] = resolvefilepath(possible_variants.default.invatlas)
            else
                inventoryItemAtlasLookup[imagename] = resolvefilepath(possible_variants[desired_variants[1]].invatlas)
            end
        else
            if table.contains(valid_variants, "default") then
                inventoryItemAtlasLookup[imagename] = resolvefilepath(possible_variants.default.invatlas)
            elseif valid_variants[1] then
                inventoryItemAtlasLookup[imagename] = resolvefilepath(possible_variants[valid_variants[1]].invatlas)
            else
                inventoryItemAtlasLookup[imagename] = resolvefilepath("images/inventoryimages.xml")
            end
        end
    end

    STRINGS.NAMES.FISHINGROD = TheWorld:HasTag("island") and STRINGS.NAMES.IA_FISHINGROD or STRINGS.NAMES.DST_FISHINGROD
    STRINGS.NAMES.OCEANFISHINGROD = TheWorld:HasTag("island") and STRINGS.NAMES.IA_OCEANFISHINGROD or STRINGS.NAMES.DST_OCEANFISHINGROD

    inst:PushEvent("refreshcrafting")
end

----------------------------------------------------------------------------------------
--Try to initialise all functions locally outside of the post-init so they exist in RAM only once
----------------------------------------------------------------------------------------

IAENV.AddPlayerPostInit(function(inst)
	inst.Physics:CollidesWith(COLLISION.WAVES)

	if TheWorld.ismastersim then
		if not inst.components.climatetracker then
			inst:AddComponent("climatetracker")
		end
		inst.components.climatetracker.period = 2
		inst:AddComponent("sailor")
		inst:AddComponent("keeponland")
		inst:AddComponent("ballphinfriend")

		if TheWorld:HasTag("island") then
			inst:AddComponent("mapwrapper")
        else
            inst:RemoveComponent("mapwrapper")
	    end

		inst:ListenForEvent("death", removebuffs)
		inst:ListenForEvent("death", blockPoison)
		inst:ListenForEvent("respawnfromghost", unblockPoison)
		inst:ListenForEvent("beachresurrect", beachresurrect)
		inst:ListenForEvent("ms_respawnedfromghost", ms_respawnedfromghost)

		if inst.OnSave then
			OnSave_old = inst.OnSave
			inst.OnSave = OnSave
		end
		if inst.OnLoad then
			OnLoad_old = inst.OnLoad
			inst.OnLoad = OnLoad
		end
		if inst.OnNewSpawn then
			OnNewSpawn = inst.OnNewSpawn
			inst.OnNewSpawn = OnNewSpawn_ia
		end
	end

	if not TheNet:IsDedicated() then
		-- player_common prefers to only set callbacks like this in "SetOwner", as the character owner can theoretically change
		if not _OnGotNewItem then
			for i, v in ipairs(inst.event_listening["setowner"][inst]) do
				if UpvalueHacker.GetUpvalue(v, "RegisterActivePlayerEventListeners") then
					_OnGotNewItem = UpvalueHacker.GetUpvalue(v, "RegisterActivePlayerEventListeners", "OnGotNewItem")
					UpvalueHacker.SetUpvalue(v, OnGotNewItem, "RegisterActivePlayerEventListeners", "OnGotNewItem")
					break
				end
			end
		end
		inst:DoTaskInTime(0, function()
			if inst == TheLocalPlayer then --only do this for the local player character
				inst:AddComponent("windvisuals")
				-- inst.components.windvisuals:SetRate(1.0)
				inst:AddComponent("watervisuals")
				inst.components.watervisuals:SetWaveSettings(0.8)
			end
		end)

		inst:ListenForEvent("climatechange", OnClimateChange)
        --init
        inst:DoTaskInTime(0.1, OnClimateChange)
	end
end)
