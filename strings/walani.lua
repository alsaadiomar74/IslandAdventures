return {
	ACTIONFAIL =
	{
        APPRAISE =
        {
            NOTNOW = "Guy's taking a break.",
        },
	    REPAIR =
        {
            WRONGPIECE = "Why won't it fit?",
        },
	    BUILD =
        {
            MOUNTED = "Naw. It's too high.",
			HASPET = "Bummer, wish I could handle more than one.",
			TICOON = "I've already got my eyes full following this guy!",
        },
		SHAVE =
		{
			AWAKEBEEFALO = "Woah, slow down. She's awake.",
			GENERIC = "Nah.",
			NOBITS = "My work here is done.",
			SOMEONEELSESBEEFALO = "Nah. That's not my job.",
		},
		STORE =
		{
			GENERIC = "It's holding all it can.",
			NOTALLOWED = "I could jam it in there I guess. Might get smushed.",
		    INUSE = "I could chill instead for a bit. They're doing all the work.",
			NOTMASTERCHEF = "I'm really not feeling up to the task.",
		},
		CONSTRUCT =
        {
            INUSE = "Someone's hard at work. Glad it's not me!",
            NOTALLOWED = "Huh. Smushing it in isn't working.",
            EMPTY = "There's nothing here, no need to work.",
            MISMATCH = "You sure these are wrong? Looks fine to me.",
        },
		RUMMAGE =
		{	
			GENERIC = "Haha, nah.",	
			INUSE = "Chillin' sounds like a better option than shuffling.",
			NOTMASTERCHEF = "I'm really not feeling up to the task.",
		},
		UNLOCK =
        {
        	WRONGKEY = "Huh. Guess that wasn't right.",
        },
		USEKLAUSSACKKEY =
        {
        	WRONGKEY = "Whoops, wrong thing.",
        	KLAUS = "Pretty bad timing, unless you wanted to die.",
        },
		ACTIVATE = 
		{
			LOCKED_GATE = "Yeah, I'm pretty sure it's locked.",
			HOSTBUSY = "I'm cool with waiting! Gives me an extra nap.",
            CARNIVAL_HOST_HERE = "Maybe the dude's finally taking a rest.",
            NOCARNIVAL = "Guess I missed the after-party.",
			EMPTY_CATCOONDEN = "Maybe I shouldn't snoop, never gets me any goods anyway.",
			KITCOON_HIDEANDSEEK_NOT_ENOUGH_HIDERS = "I could use some more of these little dudes for that.",
			KITCOON_HIDEANDSEEK_NOT_ENOUGH_HIDING_SPOTS = "Hm. Kinda barren around here for that.",
			KITCOON_HIDEANDSEEK_ONE_GAME_PER_DAY = "Time to get some naps in before I sleep!",
		},
		OPEN_CRAFTING =
		{
            PROFESSIONALCHEF = "I'm really not feeling up to that task.",
			SHADOWMAGIC = "I really don't wanna mess with that aka malihini stuff.",
		},
        COOK =
        {
            GENERIC = "Ughh, no.",
            INUSE = "Aloha, are you using that? Mind cooking me food while you're here?",
            TOOFAR = "I just need to walk over there to use it... Ughhh, I'm going to starve!",
        },
		START_CARRAT_RACE =
        {
            NO_RACERS = "I wouldn't wanna enter a race either.",
        },
		DISMANTLE =
		{
			COOKING = "Man, I almost totally messed up somebody's grinds!",
			INUSE = "I don't mean to go shoulder surfing. All yours, bud!",
			NOTEMPTY = "Sweet! Leftovers!",
        },
		FISH_OCEAN =
		{
			TOODEEP = "This rod's really not getting it. Too deep.",
		},
		OCEAN_FISHING_POND =
		{
			WRONGGEAR = "This rod gets me, it'd rather chill with the ocean.",
		},
        GIVE =
        {
		    GENERIC = "That's not happening any time soon, bud.",
            DEAD = "Poor guy's dead.",
            SLEEPING = "Aw. Let it sleep.",
            BUSY = "It's doing something else right now.",
            ABIGAILHEART = "At least I tried, right? That's one thing worth the effort.",
            GHOSTHEART = "Bummer, that guy's staying heartless.",
			NOTGEM = "Maybe if I try smushing it in with more force...",
            WRONGGEM = "Ugh, it doesn't want this gem.",
            NOTSTAFF = "That's not right, for whatever reason.",
			MUSHROOMFARM_NEEDSSHROOM = "Ugh, can someone give it a mushroom?",
            MUSHROOMFARM_NEEDSLOG = "Ugh, can someone give it a living log?",
			MUSHROOMFARM_NOMOONALLOWED = "This lil' guy isn't diggin' these logs.",
            SLOTFULL = "There's no room left in that thing.",
            DUPLICATE = "I could've sworn we had one just like this...",
            NOTSCULPTABLE = "Not a chance, that's just weird.",
			NOTATRIUMKEY = "Well, that didn't work. That's more pau hana time for me!",
            CANTSHADOWREVIVE = "That's not happening any time soon, bud.",
			WRONGSHADOWFORM = "Looks fine to me, we all see things a little differently.",
			NOMOON = "I think it misses the moon.",
			PIGKINGGAME_MESSY = "Ugh, cleaning. Who wants to do it?",
			PIGKINGGAME_DANGER = "I don't feel like risking it right now.",
			PIGKINGGAME_TOOLATE = "Nah, it's getting a bit too dark out. I'd rather sleep.",
			CARNIVALGAME_INVALID_ITEM = "Heh. Worth a shot.",
			CARNIVALGAME_ALREADY_PLAYING = "I don't have to stand in line, do I?",
			SPIDERNOHAT = "I think the little guy's packed enough already.",
			TERRARIUM_REFUSE = "It's not taking to this.",
            TERRARIUM_COOLDOWN = "It needs time to relax. Me too, buddy.",
            NOTAMONKEY = "Ooh, Ooh, Aaah...? Yeah, I've got some more practicing to do.",
            QUEENBUSY = "Guess I've gotta wait in line for this major kahuna.",
        },
        GIVETOPLAYER = 
        {
        	FULL = "Maybe I'll just set my stuff on the ground for you.",
            DEAD = "Guess you didn't want this that badly.",
            SLEEPING = "I got something for you. But I like the way you're thinking.",
            BUSY = "Hey, chill for a second. I got stuff here for you.",
    	},
    	GIVEALLTOPLAYER = 
        {
        	FULL = "This a bunch of stuff, make way.",
            DEAD = "Maybe I'll just hold onto these...",
            SLEEPING = "I've got a load of stuff for you. But I get it.",
            BUSY = "Hey, catch a break. This stuff's heavy.",
    	},
        WRITE =
        {
            GENERIC = "Nah. Don't wanna.",
            INUSE = "Eh, I didn't have much of an idea anyway.",
        },
		DRAW =
        {
            NOIMAGE = "Am I supposed to wing this drawing? Probably won't end well.",
        },
        CHANGEIN =
        {
            GENERIC = "What's wrong with what I'm wearing?",
            BURNING = "Yeah, I think I'll pass.",
            INUSE = "It's cool! Pick out somethin' stylish for me, shoots?",
			NOTENOUGHHAIR = "My bud needs a new coat first.",
            NOOCCUPANT = "Heh. I do like having to do nothing.",
        },
        ATTUNE =
        {
            NOHEALTH = "...I'm gonna need a few herbals first.",
        },
        MOUNT =
        {
            TARGETINCOMBAT = "Nah. He's busy right now.",
            INUSE = "I'm no shoulder hopper.",
			SLEEPING = "Hoʻoala! We'll nap after this, promise.",
        },
        SADDLE =
        {
            TARGETINCOMBAT = "Yeesh. He's too angry now.",
        },
        TEACH =
        {
            KNOWN = "I already know that, y'know?",
            CANTLEARN = "Looks boring.",
			WRONGWORLD = "I don't think this is anywhere around here.",
			--MapSpotRevealer/messagebottle
			MESSAGEBOTTLEMANAGER_NOT_FOUND = "I'm not really feeling this down here.",--Likely trying to read messagebottle treasure map in caves
            STASH_MAP_NOT_FOUND = "Whoever mapped this slacking on the job. Good for them!",-- Likely trying to read stash map  in world without stash
        },
		WRAPBUNDLE =
        {
            EMPTY = "There's nothing to wrap!",
        },
		PICKUP =
        {
			RESTRICTION = "I'd rather not. Too much work.",
			INUSE = "I don't mean to go shoulder surfing. All yours, bud!",
			NOTMINE_YOTC =
            {
                "Hey, you're not mine!",
                "Which one was mine again...?",
            },
			FULL_OF_CURSES = "Those aren't really my style anyway.",
        },
		SLAUGHTER =
        {
            TOOFAR = "I didn't want to have to do it anyway...",
        },
        REPLATE =
        {
            MISMATCH = "Not so sure about that.", 
            SAMEDISH = "Uh, we already serve that.", 
        },
		ROW_FAIL =
        {
            BAD_TIMING0 = "I'm getting serious noodle arms over here!",
            BAD_TIMING1 = "This is too much work. Can I use my hands?",
            BAD_TIMING2 = "Nothing wrong with a little splash.",
        },
        LOWER_SAIL_FAIL =
        {
            "Ugh, this thing's not makin' it easy.",
            "This is why I don't do the heavy lifting on a boat!",
            "Auwe! This thing's junk!",
        },
        BATHBOMB =
        {
            GLASSED = "Looks chill enough already.",
            ALREADY_BOMBED = "There's enough bubble in this bath.",
        },
		GIVE_TACKLESKETCH =
		{
			DUPLICATE = "I think we already have something like this.",
		},
		COMPARE_WEIGHABLE =
		{
			TOO_SMALL = "It's a little smaller but that's okay.",
			OVERSIZEDVEGGIES_TOO_SMALL = "It'll grow when it's ready.",
		},
		PLANTREGISTRY_RESEARCH_FAIL =
        {
            GENERIC = "My work here is done. Time for a break.",
            FERTILIZER = "I don't wanna think more about that, like, ever.",
        },
        FILL_OCEAN =
        {
            UNSUITABLE_FOR_PLANTS = "First rule of salt water is to not drink it, everyone knows that.",
        },
        POUR_WATER =
        {
            OUT_OF_WATER = "All dry.",
        },
        POUR_WATER_GROUNDTILE =
        {
            OUT_OF_WATER = "Can't surf if there's no waves!",
        },
        USEITEMON =
        {
            --GENERIC = "I can't use this on that!",

            --construction is PREFABNAME_REASON
            BEEF_BELL_INVALID_TARGET = "Yeah, no.",
            BEEF_BELL_ALREADY_USED = "I wouldn't wanna come in between those two.",
            BEEF_BELL_HAS_BEEF_ALREADY = "I've got enough work as is.",
        },
        HITCHUP =
        {
            NEEDBEEF = "Looks like I need a beef-buddy.",
            NEEDBEEF_CLOSER = "Hey, where'd my beef bud go?",
            BEEF_HITCHED = "Poor guy doesn't need more than that.",
            INMOOD = "My bud needs to chill out first.",
        },
        MARK = 
        {
            ALREADY_MARKED = "This dude gets me.",
            NOT_PARTICIPANT = "I'm not the competitive type anyway.",
        },
        YOTB_STARTCONTEST = 
        {
            DOESNTWORK = "Huh. Was I supposed to be manning this?",
            ALREADYACTIVE = "This has gotta be for wherever else.",
        },
        YOTB_UNLOCKSKIN = 
        {
            ALREADYKNOWN = "My noggin's full of this.",
        },
		CARNIVALGAME_FEED =
        {
            TOO_LATE = "Ugh, how do aunties make this work?",
        },
		HERD_FOLLOWERS =
        {
            WEBBERONLY = "Hey, I wouldn't listen to me either.",
        },
		HARVEST =
        {
            DOER_ISNT_MODULE_OWNER = "No words needed, we can just chill.",
        },
		-- SHIPWRECKED QUOTES
		REPAIRBOAT =
        {
            GENERIC = "It doesn't need any work.",
        },
		EMBARK = 
		{
			INUSE = "Wait dude, I'm still over here!",
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		OPEN_CRAFTING  = 
		{
			FLOODED = "These machines don't share my love for the sea unfortunately.",
		},
		FISH_FLOTSAM = {

			TOOFAR = "Ughhh, that's too far.",
		},
	},
	ANNOUNCE_CANNOT_BUILD =
	{
		NO_INGREDIENTS = "I've gotta find the right kine stuff for this.",
		NO_TECH = "No clue what to do for that one.",
		NO_STATION = "Ugh. I've gotta walk all the way to a workstation. Remember me...",
	},
	ACTIONFAIL_GENERIC = "Well that didn't work.",
	ANNOUNCE_BOAT_LEAK = "This thing is letting on water.",
	ANNOUNCE_BOAT_SINK = "Yikes! This thing is about to wipeout!",
	ANNOUNCE_DIG_DISEASE_WARNING = "This thing was dying on me.",
    ANNOUNCE_PICK_DISEASE_WARNING = "That poor plant, why can't nature just chill out.",
	ANNOUNCE_ACCOMPLISHMENT = "Busywork.",
	ANNOUNCE_ACCOMPLISHMENT_DONE = "Well, that's finished.",
	ANNOUNCE_ADVENTUREFAIL = "All that work for nothing.",
	ANNOUNCE_MOUNT_LOWHEALTH = "Poor guy needs some herbals.",
	ANNOUNCE_BEES = "Relax your thorax!",
	ANNOUNCE_BOOMERANG = "Ow! Stop hitting yourself, Walani!",
	ANNOUNCE_BURNT = "OUCH! That's burnin' my digits!",
	ANNOUNCE_CANFIX = "\nI should probably repair that. Maybe later.", 
	ANNOUNCE_CHARLIE = "Who's there?! Leave me alone!",
	ANNOUNCE_CHARLIE_ATTACK = "ACHK! I SAID GO AWAY!",
	ANNOUNCE_COLD = "Way too cold!",
	ANNOUNCE_CRAFTING_FAIL = "Haha. I really messed that up.",
	ANNOUNCE_DEERCLOPS = "Something big is headed this way!",
	ANNOUNCE_CAVEIN = "The roof's crashing down on us!",
	ANNOUNCE_ANTLION_SINKHOLE = 
	{
		"Yikes! This party's about to crash!",
		"The earth speaks! She wants us OUT!",
		"The earth's makin' waves! And not the good kind!",
	},
	ANNOUNCE_ANTLION_TRIBUTE =
	{
        "Got you a special present, friend.",
        "This one's all yours, bud.",
        "This gift's pretty special, buddy.",
	},
	ANNOUNCE_SACREDCHEST_YES = "Guess I'm good enough.",
	ANNOUNCE_SACREDCHEST_NO = "Not cool enough.",
	ANNOUNCE_DUSK = "Darkness will be here soon!",
	ANNOUNCE_EAT =
	{
		GENERIC = "Not too bad. Heh heh.",
		INVALID = "I'll take a pass on that one.",
		PAINFUL = "Ish my tongue shupposed t'shwell up like that?",
		SPOILED = "Gnarly! I think I'm gonna hurl...",
		STALE = "I mean, I guess that was food.",
		YUCKY = "Haha, wow, no.",
	},
    ANNOUNCE_ENCUMBERED =
    {
        "Is it... break time yet...? I've got... noodle arms.",
        "Blehhhh...",
        "Is this what... working feels like? ...Ugh!",
        "I wanna go... back to the... beach...",
        "If I were surfing this pain wave... I'd duck dive by now... Ugggghhhh....",
        "Huhhhh... bleh... I need my board... and some food...",
        "I'm ready for... pau hana...!",
        "Uuugghhhhhhhhh...",
        "Hnnnhg! Why do I... gotta do this...? Bummer...!",
    },
	ANNOUNCE_ATRIUM_DESTABILIZING = 
    {
		"It's blowing this spot up!",
		"Let's get outta here!",
		"Time to split this scene.",
	},
	ANNOUNCE_RUINS_RESET = "All that work... We don't have to do it again do we?",
	ANNOUNCE_SNARED = "Ow! Sharp! Not cool, dude.",
	ANNOUNCE_SNARED_IVY = "Yikes! Hands off!",
	ANNOUNCE_REPELLED = "It's blocking us. Rude.",
	ANNOUNCE_THURIBLE_OUT = "That creepy thing's all tired out.",
	ANNOUNCE_ENTER_DARK = "H-holy geez, it's dark! HELP! Help me!",
	ANNOUNCE_ENTER_LIGHT = "Sweet crispy light! I missed you!",
	ANNOUNCE_FREEDOM = "Hooray, I'm free! Time for a nap?",
	ANNOUNCE_HIGHRESEARCH = "Woah, I think that, like, expanded my mind!",
	ANNOUNCE_HOT = "This heat is unbearable.",
	ANNOUNCE_HOUNDS = "Junkyard dogs incomin'.",
	ANNOUNCE_WORMS = "Something smells wormy.",
	ANNOUNCE_HUNGRY = "I'm crashing like a sick wave! Gimme food!",
	ANNOUNCE_HUNT_BEAST_NEARBY = "I hear rustling nearby.",
	ANNOUNCE_HUNT_LOST_TRAIL = "Ugh, it got away.",
	ANNOUNCE_HUNT_LOST_TRAIL_SPRING = "The water washed the tracks away. Oh well.",
	ANNOUNCE_INSUFFICIENTFERTILIZER = "I think it wants something. Hey! What do you want?",
	ANNOUNCE_INV_FULL = "What am I, a pack animal?",
	ANNOUNCE_KNOCKEDOUT = "Woah. What happened?",
	ANNOUNCE_LIGHTNING_DAMAGE_AVOIDED = "Not today, nature!",
	ANNOUNCE_LOWRESEARCH = "Radical! Knowledge!",
	ANNOUNCE_MOSQUITOS = "Rude! Make your own blood!",
	ANNOUNCE_NODANGERSIESTA = "Sleeping right now seems kinda dangerous.",
	ANNOUNCE_NOWARDROBEONFIRE = "Man, that sucks.",
    ANNOUNCE_NODANGERGIFT = "Get these dudes outta here, I got a gift!",
	ANNOUNCE_NOMOUNTEDGIFT = "It's soooo far... Can't you open it with your hooves, beef-buddy?",
	ANNOUNCE_NODANGERSLEEP = "Sleeping right now seems kinda dangerous.",
	ANNOUNCE_NODAYSLEEP = "You know I'm always down for a nap but... not right now.",
	ANNOUNCE_NODAYSLEEP_CAVE = "Are you lolo? I'm not sleeping in a cave! Without a pillow!",
	ANNOUNCE_NOHUNGERSIESTA = "Foooooooooooooood...",
	ANNOUNCE_NOHUNGERSLEEP = "Fooooooooooooooood...",
	ANNOUNCE_NONIGHTSIESTA = "That would be improper napping form.",
	ANNOUNCE_NONIGHTSIESTA_CAVE = "It's a bit too cavey around here for a siesta.",
	ANNOUNCE_NOSLEEPONFIRE = "Uhh... That's on fire right now.",
	ANNOUNCE_NOSLEEPHASPERMANENTLIGHT = "Hard to catch some Zzz's with a lighthouse watchin' me, bud.",
	ANNOUNCE_NO_TRAP = "Worst trap ever.",
	ANNOUNCE_PECKED = "Hey! Stop that!",
	ANNOUNCE_QUAKE = "The ground is shaking.",
	ANNOUNCE_RESEARCH = "I feel smarter.",
	ANNOUNCE_SHELTER = "Ah. I'm in my element when I'm out of the elements.",
	ANNOUNCE_THORNS = "Ow! It bit me!",
	ANNOUNCE_TOOL_SLIP = "You're not making this any more fun!",
	ANNOUNCE_TORCH_OUT = "WHERE DID MY LIGHT GO?!",
	ANNOUNCE_FAN_OUT = "That sucks.",
    ANNOUNCE_COMPASS_OUT = "Now this compass is junk.",
	ANNOUNCE_TRAP_WENT_OFF = "My bad.",
	ANNOUNCE_TREASURE = "Woohoo! Treasure!",
	ANNOUNCE_UNIMPLEMENTED = "Looks kinda half-baked.",
	ANNOUNCE_WAVE_BOOST = "Surf's up!",
	ANNOUNCE_WORMHOLE = "...Where am I?",
	ANNOUNCE_TOWNPORTALTELEPORT = "That was easy, aloha!",
	ANNOUNCE_DAMP = "A little water never hurt no one.",
	ANNOUNCE_WET = "I'll need to towel off soon.",
	ANNOUNCE_WETTER = "My clothes are all soggy... Haha.",
	ANNOUNCE_SOAKED = "I am thoroughly soaked.",
	ANNOUNCE_WASHED_ASHORE = "Oof. What a gnarly wipeout.",
	ANNOUNCE_TOADESCAPING = "Big guy's getting bored of us.",
	ANNOUNCE_TOADESCAPED = "Too tired to fight anymore, I feel you toad.",

	ANNOUNCE_DESPAWN = "Guess I'm off to surf the beyond! Aloha, a hui hou...",
	ANNOUNCE_GHOSTDRAIN = "Ugh. There's a party in my head, and I'm on the second floor.",
	ANNOUNCE_PETRIFED_TREES = "Woah. Tress totally didn't do that back home.",
	ANNOUNCE_KLAUS_ENRAGE = "Yikes! Total party crasher!",
	ANNOUNCE_KLAUS_UNCHAINED = "Dude's been unchained! Gross!",
	ANNOUNCE_KLAUS_CALLFORHELP = "No fair!",
	--hallowed nights
    ANNOUNCE_SPOOKED = "Whoa! ...Did anybody else just see that?",
	ANNOUNCE_BRAVERY_POTION = "Now I can face some wicked trees without my boots shaking!",
	ANNOUNCE_MOONPOTION_FAILED = "Huh. Well, I wouldn't wanna do anything either.",
	--winter's feast
	ANNOUNCE_EATING_NOT_FEASTING = "C'mon, dudes! Join the lu'au! We got grinds for days!",
	ANNOUNCE_WINTERS_FEAST_BUFF = "Choka! I'm shining like the ocean!",
	ANNOUNCE_IS_FEASTING = "Foooooooooooooood!",
	ANNOUNCE_WINTERS_FEAST_BUFF_OVER = "The seasons are always changing.",
	--lavaarena event
    ANNOUNCE_REVIVING_CORPSE = "C'mon, bud! Wake up!",
    ANNOUNCE_REVIVED_OTHER_CORPSE = "Gnarly! Keep on chugging, dude.",
    ANNOUNCE_REVIVED_FROM_CORPSE = "Ugh, what a wipeout. Can I keep napping?",
	--quagmire event
    QUAGMIRE_ANNOUNCE_NOTRECIPE = "I've thought up worse things to eat.",
    QUAGMIRE_ANNOUNCE_MEALBURNT = "Maybe it was too spicy?",
    QUAGMIRE_ANNOUNCE_LOSE = "Yikes! I did all I could, and that includes the naps!",
    QUAGMIRE_ANNOUNCE_WIN = "Pau! Did we do it?",
	--RoT
	ANNOUNCE_MOONALTAR_MINE =
	{
		GLASS_MED = "I think there's something in here.",
		GLASS_LOW = "Ugh. Alright, alright. Almost there.",
		GLASS_REVEAL = "Hope that was worth the work.",
		IDOL_MED = "I think there's something in here.",
		IDOL_LOW = "Ugh. Alright, alright. Almost there.",
		IDOL_REVEAL = "Hope that was worth the work.",
		SEED_MED = "I think there's something in here.",
		SEED_LOW = "Ugh. Alright, alright. Almost there.",
		SEED_REVEAL = "That probably wasn't worth the effort.",
	},
	ANNOUNCE_FLARE_SEEN = "Anybody else see that flare?",
    ANNOUNCE_MEGA_FLARE_SEEN = "Someone's having a party seen all across the world. I think everyone's invited.",
    ANNOUNCE_OCEAN_SILHOUETTE_INCOMING = "That's never a good sign at sea...",
	--
	ANNOUNCE_ATTACH_BUFF_ELECTRICATTACK    = "I'm all amped up!",
    ANNOUNCE_ATTACH_BUFF_ATTACK            = "FEAR ME!!",
    ANNOUNCE_ATTACH_BUFF_PLAYERABSORPTION  = "I'm totally stoked, I could take on every bomb wave!",
    ANNOUNCE_ATTACH_BUFF_WORKEFFECTIVENESS = "Huh? What's this tingling...? Is this? No! Productivity! Must. Resist.",
    ANNOUNCE_ATTACH_BUFF_MOISTUREIMMUNITY  = "Surf's up!",
	ANNOUNCE_ATTACH_BUFF_SLEEPRESISTANCE   = "No more naps? Can I take one anyway?",

    ANNOUNCE_DETACH_BUFF_ELECTRICATTACK    = "Now I'm all fizzled out.",
    ANNOUNCE_DETACH_BUFF_ATTACK            = "Haha, nah. We're all good.",
    ANNOUNCE_DETACH_BUFF_PLAYERABSORPTION  = "Guess I'll stick with the ankle slappers for now.",
    ANNOUNCE_DETACH_BUFF_WORKEFFECTIVENESS = "I could use a nap.... for a lifetime. Good to be back.",
    ANNOUNCE_DETACH_BUFF_MOISTUREIMMUNITY  = "Back to my usual moisture. The tide was coming in anyway.",
	ANNOUNCE_DETACH_BUFF_SLEEPRESISTANCE   = "Finally, I can get some sleep!",
	
	ANNOUNCE_OCEANFISHING_LINESNAP = "Aw, snap!",
	ANNOUNCE_OCEANFISHING_LINETOOLOOSE = "I guess I could tighten the line, but that's work.",
	ANNOUNCE_OCEANFISHING_GOTAWAY = "Stay free, little guy.",
	ANNOUNCE_OCEANFISHING_BADCAST = "I guess I'm a little rusty.",
	ANNOUNCE_OCEANFISHING_IDLE_QUOTE = 
	{
		"Nothing better than this.",
		"This is the life for me.",
		"I'd take a nap too if it weren't for the fish...",
		"Fishing really lets you just chillax.",
	},
	ANNOUNCE_WEIGHT = "Weight: {weight}",
	ANNOUNCE_WEIGHT_HEAVY  = "Weight: {weight}\nWhew, this is heavy.",

	ANNOUNCE_WINCH_CLAW_MISS = "You'll get it eventually.",
	ANNOUNCE_WINCH_CLAW_NO_ITEM = "Welp! Nap break.",
	ANNOUNCE_WEAK_RAT = "Dude's crashing like a sick wave!",

    ANNOUNCE_CARRAT_START_RACE = "Go get 'em, champ.",

    ANNOUNCE_CARRAT_ERROR_WRONG_WAY = {
        "Eh, he goes where he wants.",
        "Who am I to tell him where to go?",
    },
    ANNOUNCE_CARRAT_ERROR_FELL_ASLEEP = "Let me join you with the nappin', bud!",    
    ANNOUNCE_CARRAT_ERROR_WALKING = "Hey, that's more walking than I like to do at least.",    
    ANNOUNCE_CARRAT_ERROR_STUNNED = "Rat got your tongue, bud?",
	ANNOUNCE_POCKETWATCH_PORTAL = "Harsh! I thought I got used to crashing onto the shore.",
	ANNOUNCE_ARCHIVE_NEW_KNOWLEDGE = "I'm the last person you should give these machine ideas to.",
    ANNOUNCE_ARCHIVE_OLD_KNOWLEDGE = "Huh, I guess I knew that.",
    ANNOUNCE_ARCHIVE_NO_POWER = "It's looking a little lifeless.",

	ANNOUNCE_PLANT_RESEARCHED =
    {
        "Learning about nature isn't half bad, y'know?",
    },

    ANNOUNCE_PLANT_RANDOMSEED = "I can wait however long it takes, I'm in no rush.",

    ANNOUNCE_FERTILIZER_RESEARCHED = "Man, you're never gonna believe what it was! Heh.",

	ANNOUNCE_FIRENETTLE_TOXIN = 
	{
		"Ugh, I'm missing the sun back home! This stings!",
		"Sheesh! I need to take a dip!",
	},
	ANNOUNCE_FIRENETTLE_TOXIN_DONE = "Ah... Pau. Wanna see me do it again?",

	ANNOUNCE_TALK_TO_PLANTS = 
	{
        "Hang ten, mini mea kanu.",
        "I love a bud that knows how to chill. Wait, are we supposed do that?",
		"Howzit hanging, keiki? Grow any new leaves today?",
        "Your leaves are lookin' totally bloomed today.",
        "Aloha! Welp. My work here is done.",
	},

	ANNOUNCE_KITCOON_HIDEANDSEEK_START = "ekolu, elua, ekahi... Man, counting's a lotta work. Here I come!",
	ANNOUNCE_KITCOON_HIDEANDSEEK_JOIN = "I'll hang ten with the lil' kitties too!",
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND =
	{
		"Got ya, bud!",
		"Your hiding days are over, lil' buddy.",
		"Aloha, lil' buddy!",
		"I coulda found you in my sleep!",
	},
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND_ONE_MORE = "Have we not found them all yet?",
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND_LAST_ONE = "Ha! Now we all get to rest a lil'.",
	ANNOUNCE_KITCOON_HIDANDSEEK_FOUND_LAST_ONE_TEAM = "{name} got dibs on the last one!",
	ANNOUNCE_KITCOON_HIDANDSEEK_TIME_ALMOST_UP = "Yeesh, we're being timed!",
	ANNOUNCE_KITCOON_HIDANDSEEK_LOSEGAME = "Hey, I was still playing!",
	ANNOUNCE_KITCOON_HIDANDSEEK_TOOFAR = "Doubt they'd be way out here.",
	ANNOUNCE_KITCOON_HIDANDSEEK_TOOFAR_RETURN = "I think I can hear their cute little mews!",
	ANNOUNCE_KITCOON_FOUND_IN_THE_WILD = "Aw, c'mere lil' guy!",

	ANNOUNCE_TICOON_START_TRACKING	= "What'cha got, guy?",
	ANNOUNCE_TICOON_NOTHING_TO_TRACK = "He's all outta tracking senses I think.",
	ANNOUNCE_TICOON_WAITING_FOR_LEADER = "Where we goin', bud?",
	ANNOUNCE_TICOON_GET_LEADER_ATTENTION = "Alright, alright, I'll get moving! Eventually.",
	ANNOUNCE_TICOON_NEAR_KITCOON = "Got a scent, lil' fella?",
	ANNOUNCE_TICOON_LOST_KITCOON = "I didn't know this was a contest. Sheesh!",
	ANNOUNCE_TICOON_ABANDONED = "I was hoping I didn't have to do the work...",
	ANNOUNCE_TICOON_DEAD = "How could I possibly move on after that? Oh, that way I guess.",

    -- YOTB
    ANNOUNCE_CALL_BEEF = "Hele, beef-buddy!",
    ANNOUNCE_CANTBUILDHERE_YOTB_POST = "Way too far out.",
    ANNOUNCE_YOTB_LEARN_NEW_PATTERN =  "Sweet! Maybe I should start a collection?",

    -- AE4AE
    ANNOUNCE_EYEOFTERROR_ARRIVE = "My eyes aren't deceiving me, are they?",
    ANNOUNCE_EYEOFTERROR_FLYBACK = "I guess there's no time for pau hana?",
    ANNOUNCE_EYEOFTERROR_FLYAWAY = "Seems like a good opportunity to get a nap in.",

    -- PIRATES
    ANNOUNCE_CANT_ESCAPE_CURSE = "Major bummer. These crystals are NOT the chakra cleansing kind.",
    ANNOUNCE_MONKEY_CURSE_1 = "Bleh. I'm getting seriously itchy.",
    ANNOUNCE_MONKEY_CURSE_CHANGE = "Oh? Being a monkey is fine, actually.",
    ANNOUNCE_MONKEY_CURSE_CHANGEBACK = "Hey, I'm me again! Oh right, I'm me again.",

    ANNOUNCE_PIRATES_ARRIVE = "Ugh. I could smell that stench of pirate pilikia from across the sea.",

    -- SHIPWRECKED QUOTES

    ANNOUNCE_MAGIC_FAIL = "It won't work. Me either! Haha.",

    ANNOUNCE_SHARX = "Something smells sharky.",

    ANNOUNCE_TREASURE = "Woohoo! Treasure!",
    ANNOUNCE_MORETREASURE = "My luck continues!",
    ANNOUNCE_OTHER_WORLD_TREASURE = "I don't think this is anywhere around here.",
    ANNOUNCE_OTHER_WORLD_PLANT = "Can't do that, the poor thing wouldn't grow here.",

    ANNOUNCE_IA_MESSAGEBOTTLE =
    {
        "Reading's boring anyway.",
    },
    ANNOUNCE_VOLCANO_ERUPT = "The volcano speaks!",
    ANNOUNCE_MAPWRAP_WARN = "Looks pretty foggy over there.",
    ANNOUNCE_MAPWRAP_LOSECONTROL = "Uhhhm. Where am I?",
    ANNOUNCE_MAPWRAP_RETURN = "The fog is clearing!",
    ANNOUNCE_CRAB_ESCAPE = "I thought it was right here...",
    ANNOUNCE_TRAWL_FULL = "My work has paid off!",
    ANNOUNCE_BOAT_DAMAGED = "My floater's been dinged!",
	ANNOUNCE_BOAT_SINKING = "This thing is letting on water.",
	ANNOUNCE_BOAT_SINKING_IMMINENT = "I won't be floating much longer...",
    ANNOUNCE_WAVE_BOOST = "Surf's up!",

    ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "Whale, whale, whale!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "It must've dove deeper.",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "The waters are way too rough.",

	BATTLECRY =
	{
		GENERIC = "I'M A FORCE TO BE RECKONED WITH!",
		PIG = "DESTRUCTION!",
		PREY = "YOU WILL NOT WIN!",
		SPIDER = "YOU WILL REGRET THIS DAY!",
		SPIDER_WARRIOR = "I WILL END YOU!",
		DEER = "PREPARE FOR DESTRUCTION!",
	},
	COMBAT_QUIT =
	{
		GENERIC = "Oh man, you shoulda seen the look on your face.",
		PIG = "Haha, gotcha.",
		PREY = "Nah, I was just messin' with you.",
		SPIDER = "Pssh, nah, we're good.",
		SPIDER_WARRIOR = "Haha, nah. We're cool.",
	},
	DESCRIBE =
	{
	    MULTIPLAYER_PORTAL = "This is giving me the major jeepers.",
		MULTIPLAYER_PORTAL_MOONROCK = "Looks wicked glittering in the sun.",
        CONSTRUCTION_PLANS = "It'd take a lot of effort to put this together.",
        MOONROCKIDOL = "Who do I gotta give this thing to again?",
        MOONROCKSEED = "Wonder what makes it float?",
		ANTLION = 
		{
			GENERIC = "What'cha want, big guy?",
			VERYHAPPY = "They're just a big lolo fellow!",
			UNHAPPY = "Can we talk about this?",
		},
		ANTLIONTRINKET = "I made so many sick sandcastles in my hanabata days.",
		SANDSPIKE = "This is worse than it getting all the way in your shorts.",
        SANDBLOCK = "Pretty good castle, bud! I could give you a few tips though.",
        GLASSSPIKE = "Lookin' sharp!",
        GLASSBLOCK = "It's smaller than before!",

		ABIGAIL_FLOWER = 
		{ 
			GENERIC = "That's a pretty wicked flower.",
			LEVEL1 = "I think we all could use a little alone time now and then.",
			LEVEL2 = "You comin' out of your floral shell? No pressure, bud.",
			LEVEL3 = "Out and about today, ectobuddy?",
			-- deprecated
			LONG = "Something's up with that flower.",
			MEDIUM = "I don't feel comfortable chillin' around it.",
			SOON = "Something is getting ready to bloom.",
			HAUNTED_POCKET = "I should probably put this down.",
			HAUNTED_GROUND = "That flower's still pretty cool.",
		},

		BALLOONS_EMPTY = "There's not any party in deflated balloons.",
		BALLOON = "I've found the party.",
		BALLOONPARTY = "This balloon really knows how to party!",
		BALLOONSPEED =
        {
            DEFLATED = "Bummer. Balloon's lost all its surf.",
            GENERIC = "Think I can board better with this?",
        },
		BALLOONVEST = "I could totally teach the groms surfing with 'em wearing these.",
		BALLOONHAT = "I'm not gonna float away with it on, am I?",

		BERNIE_INACTIVE =
		{
			BROKEN = "Fallen to pieces? I know the feeling, bear buddy.",
			GENERIC = "Aloha, little bear buddy! Think that stuffing can surf?",
		},

		BERNIE_ACTIVE = "Lil' dude's gotten up to start dancing. Rad!",
		BERNIE_BIG = "He's like a sick wave in a tidal storm.",

		BOOKSTATION = "Wicker keeps insisting I should try surfing a good book now and then.",
		BOOK_BIRDS = "I'd rather sit back and watch the birds, first hand.",
		BOOK_TENTACLES = "This won't help me become a fan of reading.",
		BOOK_GARDENING = "Reading's enough work as is, but gardening too? No way.",
		BOOK_SILVICULTURE = "Too many words to get through to read the good parts.",
		BOOK_HORTICULTURE = "Reading's enough work as is, but gardening too? No way.",
		BOOK_SLEEP = "Finally, a book that gets me.",
		BOOK_BRIMSTONE = "Do you have a book about peace and love instead?",
		WAXWELLJOURNAL = "Read the dark? No thanks.",

        BOOK_FISH = "I can take a dive and get you a first hand account.",
        BOOK_FIRE = "What more could I even learn about fire?",
        BOOK_WEB = "A little self-help never hurt anyone.",
        BOOK_TEMPERATURE = "I'll pass. That's not natural enough.",
        BOOK_LIGHT = "Think you could use this to stop ships from running aground?",
        BOOK_RAIN = "I'm more of a fan of traditional rain dancing.",
        BOOK_MOON = "I always liked the sun a little more.",
        BOOK_BEES = "I think I can hear little dudes takin' a nap in here!",

        BOOK_HORTICULTURE_UPGRADED = "This takes up way too much brain power.",
        BOOK_RESEARCH_STATION = "I learn from experience! ...Is it too late to change my mind?",
        BOOK_LIGHT_UPGRADED = "I'd never stay awake past the first page.",

        FIREPEN = "Harsh! That'd make a killer baton.",

		LIGHTER = "Night will never see it coming.",
		LUCY = "This axe is pretty chill.",
		SPEAR_WATHGRITHR = "I don't get the point. I hope I don't get it either.",
	    WATHGRITHRHAT = "Mystical.",
		SPICEPACK = "Keeps snacks cool, like me.",

        PLAYER =
        {
            GENERIC = "Aloha, %s!",
            ATTACKER = "What's your deal, %s?",
            MURDERER = "Whoa, tone it down, dude.",
            REVIVER = "Me and %s are good pals.",
            GHOST = "Bummer about that wipeout, %s.",
			FIRESTARTER = "Burning things isn't cool, %s.",
        },
		WILSON = 
		{
			GENERIC = "Hang loose, %s!",
			ATTACKER = "Stay away, %s! Your hair's wicked sharp!",
			MURDERER = "Chillax your thorax, %s!",
			REVIVER = "Does this mean we're friends now, %s? That's too much work.",
			GHOST = "That was a serious wipeout. I guess you're wanting a heart or something?",
			FIRESTARTER = "Chill out, %s. Science doesn't always need fire.",
		},
		WOLFGANG = 
		{
			GENERIC = "How ya doin', %s?",
			ATTACKER = "Take it easy, big guy. I'm not gonna hurt ya.",
			MURDERER = "Stay away, %s! Chill those mitts!",
			REVIVER = "Me %s are good pals. We just like shoving food in our faces!",
			GHOST = "You took a serious pounding out there. You could probably use a heart and a big hug.",
			FIRESTARTER = "Burning things isn't cool, %s.",
		},
		WAXWELL = 
		{
			GENERIC = "You can hang loose too, I guess, %s.",
			ATTACKER = "What's your deal, %s? Haven't we had enough?",
			MURDERER = "You're still just a jerk with an ego, %s.",
			REVIVER = "This loser's totally my friend now. Shoots, %s?",
			GHOST = "Man, you totally fumbled that one. ...Why the fierce mug? You can always paddle back!",
			FIRESTARTER = "You know how to make a name for yourself, and not a good one.",
		},
		WX78 = 
		{
			GENERIC = "How ya doin', %s? Got rid of those fleshlings yet?",
			ATTACKER = "What's your deal, %s? Gears got your tongue?",
			MURDERER = "Stop all your hating and chill for once!",
			REVIVER = "Sometimes you just need to chillax like this, bot-buddy.",
			GHOST = "Gnarly wipeout, robo-buddy. Someone should get you a heart.",
			FIRESTARTER = "Why can't you just chillax for once, %s?",
		},
		WILLOW = 
		{
			GENERIC = "Hang ten, %s!",
			ATTACKER = "Can we talk this out, %s? The ocean isn't so bad!",
			MURDERER = "Take it easy, %s. No need for all that aggro!",
			REVIVER = "I know a couple of tricks that flow in your style, %s.",
			GHOST = "%s totally got extinguished out there.",
			FIRESTARTER = "Water moves a lot like flames, tita. Give it a chance!",
		},
		WENDY = 
		{
			GENERIC = "How's life treatin' ya, %s?",
			ATTACKER = "Alright, chill out, %s. We can talk this out.",
			MURDERER = "Really not cool... I should keep my distance.",
			REVIVER = "%s is good a chillin', but maybe too good at illin'.",
			GHOST = "I think you should stick to the ankle slappers for now, keiki.",
		    FIRESTARTER = "You're not lighting fires, are you? If you are it's cool, but don't do it again.",
		},
		WOODIE = 
		{
			GENERIC = "Aloha, %s! Been planting any trees?",
			ATTACKER = "Chill it, %s! Maybe talk it out with your axe?",
			MURDERER = "I should keep my distance for now, maybe he'll chill out.",
			REVIVER = "Me and %s are good pals. Dude's got a good heart.",
			GHOST = "Bummer, dude. You totally rag-dolled out there.",
			BEAVER = "That's rough, buddy.",
			BEAVERGHOST = "You deserve a heart, %s. And some time to relax.",
			MOOSE = "Gnarly. What else can you turn into?",
            MOOSEGHOST = "You could use some serious chill out time.",
            GOOSE = "Hey, little guy! Wait. ...%s?",
            GOOSEGHOST = "At least you can rest for a little while. Let those waves calm.",
			FIRESTARTER = "%s's been using a more natural method of forest clearing.",
		},
		WICKERBOTTOM = 
		{
			GENERIC = "How ya doin', auntie?",
			ATTACKER = "Put the fighting books down, %s. Why don't we bring out the chill and peaceful books?",
			MURDERER = "I didn't know picking up a book could be so disastrous. I'm never reading!",
			REVIVER = "If I can convince you to take a beach day, that's a win in my book.",
			GHOST = "You crashed out big time. I'll get you a heart though. Probably.",
		    FIRESTARTER = "Think about the books, %s.",
		},
		WES = 
		{
			GENERIC = "How's life treatin' ya, %s?",
			ATTACKER = "Don't stare at me like that, %s!",
			MURDERER = "Tone it down, will ya? You're giving me the creeps.",
			REVIVER = "%s probably enjoys chilling. But I don't wanna talk for em.",
			GHOST = "You totally spun out, mime dude.",
			FIRESTARTER = "Yep. Those were definitely real fires. Not cool.",
		},
		WEBBER = 
		{
			GENERIC = "How ya doin', little %s?",
			ATTACKER = "What's the matter, %s? Let me teach you the art of chilling.",
			MURDERER = "Yeesh, this spider's venomous!",
			REVIVER = "You've got the heart of a surfer, %s.",
			GHOST = "I'll get a heart for you, little buddy! ...Where do we put those, again?",
			FIRESTARTER = "I'm not mad, just disappointed, %s.",
		},
		WATHGRITHR = 
		{
			GENERIC = "Hoa aloha, %s!",
			ATTACKER = "Take it easy, %s! We can talk it out!",
			MURDERER = "Way too aggro, %s! You can't slay every good vibe!",
			REVIVER = "Me and %s are good pals. I think. You're not gonna arm wrestle me again are you?",
			GHOST = "Wicked wipeout, dude. I guess you'll be needing a heart or something?",
			FIRESTARTER = "You can't fight fire with fire, %s! Get it?",
		},
		WINONA =
        {
            GENERIC = "Howzit goin', %s?",
            ATTACKER = "%s will you ever just sit down and chill? Maybe nap?",
            MURDERER = "Yeesh. You really don't want to be on her bad side.",
            REVIVER = "It's pretty good to have someone doin' all the work for me around here!",
            GHOST = "You got seriously worked. I'll get you a heart though, might be after a nap.",
            FIRESTARTER = "Do you ever clock out, %s? Sheesh.",
        },
		WORTOX =
        {
            GENERIC = "How ya doin', %s? Pull any good pranks lately?",
            ATTACKER = "Pulling pranks on your good pal 'Lani? Not cool, dude.",
            MURDERER = "Stay away, dude! You're totally killing my vibes!",
            REVIVER = "You're just a real big softie, aren't you %s? Inside and out!",
            GHOST = "You're a real goofy footer, %s. I'll teach you to tame those waves.",
            FIRESTARTER = "That fire joke wasn't very funny, %s.",
        },
		WORMWOOD =
        {
            GENERIC = "How ya doin', plant buddy?",
            ATTACKER = "Aggression is the lamest way to get attention, %s.",
            MURDERER = "Chill down, dude! I'm not a fan of killer nature!",
            REVIVER = "%s has a heart of nature and a heart of... a gem.",
            GHOST = "You really got spat out of that one, %s.",
            FIRESTARTER = "Fire is really bad for the environment, %s. Don't you know?",
        },
		WURT =
        {
            GENERIC = "Hoa aloha, little fish bud!",
            ATTACKER = "Woah, kid's got a sharp tooth.",
            MURDERER = "Chill out, shark biscuit! Fish are friends!",
            REVIVER = "You can float, think you're ready to handle a party wave?",
            GHOST = "Everyone has a wipeout now and then. But true surfers always paddle back, %s.",
            FIRESTARTER = "Fire? Nah, let's not go down that path. Let me tell you more about the ocean.",
        },
		WALTER =
        {
            GENERIC = "Hang ten, %s!",
            ATTACKER = "Sheesh. Is that what they're teaching scouts over there?",
            MURDERER = "Murder's not a badge to wear with honor, bud.",
            REVIVER = "Now that's a badge worth earning, little dude!",
            GHOST = "You went way over the falls on that one. You pack any hearts on your trip?",
            FIRESTARTER = "You might wanna keep working on that fire keeping badge.",
        },
		WANDA =
        {
            GENERIC = "It's hang ten o' clock, %s!",
            ATTACKER = "All that stressing really makes you fighty, huh?",
            MURDERER = "Take things easy, %s! You can still reverse that killer 'tude!",
            REVIVER = "%s is solid proof people can change! And change. And change...",
            GHOST = "You really got caught inside that time impact zone. I'll see if I can reverse this one. After a nap.",
            FIRESTARTER = "You're burning a lot of time-bridges, %s.",
        },
        WONKEY =
        {
	        GENERIC = "Hang ten, iki keko!",
	        ATTACKER = "%s is up to some serious monkey business. And not the fun kind.",
	        MURDERER = "Let's tone it down, guy. We're in no jungle. I think?",
	        REVIVER = "%s's got the right idea. No shaving, no bathing. All sleeping, all eating.",
	        GHOST = "Sorry about your death, lil' keko. Do we have the right hearts?",
	        FIRESTARTER = "%s discovered fire! Pretty impressive, bud.",
        },
		WALANI = 
		{
			GENERIC = "Aloha, %s! Pehea 'oe? Catch any waves lately?",
	        ATTACKER = "Uoki, tita! A real surfer wouldn't do that!",
	        MURDERER = "You're being a real junkyard dog right now. That's not like us, not anymore!",
	        REVIVER = "%s is pretty chill, if you ask me.",
	        GHOST = "Sheesh, that must've been one harsh wipeout. You gotta watch out for those seriously choppy waves.",
	        FIRESTARTER = "What's cookin', good lookin'? Oh.",
		},
	    WARLY = 
		{
	        GENERIC = "Hoa aloha, %s! Anything good on the menu?",
	        ATTACKER = "You adding some sort of angry spice to your food, %s?",
	        MURDERER = "Take things easy, %s. I didn't eat that meal you saved, I swear!",
	        REVIVER = "%s knows just what to serve.",
	        GHOST = "Bummer! Who's gonna cook us up dinner now?",
	        FIRESTARTER = "Nobody likes burnt food, %s.",
		},
	    WILBUR = 
		{
	        GENERIC = "Hang ten, little %s.",
	        ATTACKER = "What's the matter, little buddy? Fella?",
	        MURDERER = "Whoa, tone it down, little guy.",
	        REVIVER = "Me and %s love to just chill, eatin' junk.",
	        GHOST = "Do our hearts work on monkeys? Or kings?",
	        FIRESTARTER = "I hope it wasn't my job to teach you not to burn stuff.",
		},
	    WOODLEGS = 
		{
			GENERIC = "Aloha, %s!",
	        ATTACKER = "%s is acting a bit too piratey for my taste.",
	        MURDERER = "Halt, %s! You're not part of a crew anymore!",
	        REVIVER = "Me and %s are good pals. Life at sea is where it's at.",
	        GHOST = "Sorry about your tumble, unko. Need a new pirate heart?",
	        FIRESTARTER = "You're fanning the flames on good vibes, %s. It's a bad look for you.",
		},
        MIGRATION_PORTAL = 
        {
            GENERIC = "Why would I want to go in there anyway?",
            OPEN = "Aloha, friends!",
            FULL = "Nobody wants me in there, I guess.",
        },
		ACCOMPLISHMENT_SHRINE = "Well, that looks like a lot of busywork.",
		ACORN = "Come on out, nature!",
        ACORN_SAPLING = "Grow strong little dude.",
		ACORN_COOKED = "Sorry, tree baby.",
		ADVENTURE_PORTAL = "Why would I want to go in there?",
		AMULET = "Looks great, feels great.", --Red
		ANCIENT_ALTAR = "This is giving me major jeepers.",
		ANCIENT_ALTAR_BROKEN = "Maybe it still works?",
		ANCIENT_STATUE = "How depressing.",
		ANIMAL_TRACK = "Friends are near!",
		ARMORDRAGONFLY = "This armor is so hot right now.",
		ARMORGRASS = "Doesn't seem all that sturdy.",
		ARMORMARBLE = "That's just overkill.",
		ARMORRUINS = "It's so fancy!",
		ARMORSKELETON = "Wearin' this make me feel a little grey.",
		SKELETONHAT = "Feels wrong to wear this on my head...",
		ARMORSLURPER = "So efficient!",
		ARMORSNURTLESHELL = "Protection from nature, by nature.",
		ARMORWOOD = "I hope the attacker doesn't have an axe.",
		ARMOR_SANITY = "Eyuk! It makes my skin crawl.",
		ASH =
		{
			GENERIC = "I must've missed the party.",
			REMAINS_EYE_BONE = "Huh. I guess it didn't like traveling?",
			REMAINS_GLOMMERFLOWER = "Huh. I guess it didn't like traveling?",
			REMAINS_THINGIE = "Huh. I guess it didn't like traveling?",
		},
		AXE = "I could cut trees with this... But I'd rather not.",
		BABYBEEFALO = 
		{
			GENERIC = "Growing up is a trap... don't do it!",
		    SLEEPING = "Sleeping away the terrors of the night.",
        },
        BUNDLE = "Huh. Somehow it's less heavy like this.",
        BUNDLEWRAP = "That's a wrap!",
		BACKPACK = "To haul my stuff around.",
		BACONEGGS = "Turn that frown upside down!",
		BANDAGE = "I can patch myself up with this!",
		BAT = "Go flap somewhere else!",
		BATBAT = "Maybe I could bat some bats with this.",
		BATWING = "A couple more of these and I'll be flyin'!",
		BATWING_COOKED = "Looks a bit leathery.",
		BATCAVE = "I'll let them keep catching their Z's.",
		BEARDHAIR = "Someone is probably missing these.",
		BEARGER = "Things don't have to get hairy, dude.",
		BEARGERVEST = "Made of real bearger chest!",
		BEARGER_FUR = "Now I can hibernate in comfort.",
		BEDROLL_FURRY = "For a more comfortable nap time!",
		BEDROLL_STRAW = "For nap time!",
		BEEQUEEN = "You're really gnarly looking, no offense your majesty",
		BEEQUEENHIVE = 
		{
			GENERIC = "That's looking pretty gooey.",
			GROWING = "Huh. That's new.",
		},
        BEEQUEENHIVEGROWN = "That's a whole lotta honey stuffs.",
        BEEGUARD = "I appreciate the dedication, dude, but can you not?",
		MINISIGN =
        {
            GENERIC = "Nice drawing.",
            UNDRAWN = "I could doodle on this.",
        },
        MINISIGN_ITEM = "I'm getting tired of carrying this around.",
		HIVEHAT = "For all the big and buzzy kahuna out there.",
		BEE =
		{
			GENERIC = "Well \"bzz bzz\" to you, too!",
			HELD = "You're probably safer here than out there, honestly.",
		},
		BEEBOX =
		{
			BURNT = "Maybe the bees did some redecorating?",
			FULLHONEY = "It's overflowing with sweet, sweet honey!",
			GENERIC = "That box has bees in and around it.",
			NOHONEY = "Would it be hypocritical to tell the bees to get to work?",
			SOMEHONEY = "Looks like the bees made me some honey! Thanks!",
			READY = "It's overflowing with sweet, sweet honey!",
		},
		MUSHROOM_FARM =
		{
			STUFFED = "Eh, I'll pick them eventually...",
			LOTS = "That's a lot of fungus. It's like a party.",
			SOME = "It's getting pretty good.",
			EMPTY = "Needs some stuff. Probably a mushroom.",
			ROTTEN = "It's gotten really nasty since the last time I saw it.",
			BURNT = "That's no mushroom house.",
			SNOWCOVERED = "Looks pretty frozen.",
		},
		BEEFALO =
		{
			FOLLOWER = "Aww, he loves me!",
			GENERIC = "I love these things!",
			NAKED = "Beautiful beach bod, bud.",
			SLEEPING = "Blissfully unaware of the terrors of night.",
			--Domesticated states:
            DOMESTICATED = "This one really gets me.",
            ORNERY = "Yeesh. Chill out!",
            RIDER = "Hey, wanna do all my walking for me?",
            PUDGY = "You like eating almost as much as I do.",
			MYPARTNER = "You and me, buddy. We got this.",
		},
		BEEFALOHAT = "It smells like beefalo.",
		BEEFALOWOOL = "It looks more like hair to me.",
		BEEHAT = "Stops the bee kisses.",
		BEESWAX = "A lotta work for some wax.",
		BEEHIVE = "The bees must sleep in there.",
		BEEMINE = "It's like a confetti bomb. But bees.",
		BELL = "Summons the big stomper.",
		BERRIES = "These are the edible kind, right?",
		BERRIES_COOKED = "They're all sticky now. Heh heh, gross.",
		BERRIES_JUICY = "Are my pockets not fresh enough?",
        BERRIES_JUICY_COOKED = "These dudes really hate being around, huh.",
		BERRYBUSH =
		{
			BARREN = "It needs a little love.",
			GENERIC = "Awesome! That bush has berries!",
			PICKED = "It's kinda just a regular bush now.",
			WITHERED = "That bush isn't looking very good.",
			DISEASED = "It's looking rough, but who am I to judge?",
			DISEASING = "That bush isn't looking too cool.",
			BURNING = "Why must it burn?",
		},
		BERRYBUSH_JUICY =
		{
			BARREN = "The life's been sucked out of it.",
			WITHERED = "I can nurse it back to health.",
			GENERIC = "How lush!",
			PICKED = "Mahalo for the snack, dude!",
			DISEASED = "It's looking bad, but who am I to judge?",
			DISEASING = "That bush isn't looking too cool.",
			BURNING = "Senseless.",
		},
		BIRDCAGE =
		{
			GENERIC = "I need to get a birdy buddy.",
			OCCUPIED = "I hope my birdy buddy is happy.",
			SLEEPING = "My birdy buddy's super sleepy.",
			HUNGRY = "My birdy buddy's belly's empty!",
			STARVING = "My birdy buddy's all bones!",
			DEAD = "My birdy buddy breathed his last...",
			SKELETON = "I have a lot of regrets in life.",
		},
		BIRDTRAP = "I can catch myself a birdy buddy with this.",
		BIRD_EGG = "There is a tiny birdy buddy in here!",
		BIRD_EGG_COOKED = "...I'm sorry, birdy buddy.",
		BISHOP = "Don't get any bright ideas.",
		BOARDS = "Not much surf in these boards.",
		BOOMERANG = "They say if you love something, you should let it go.",
		BURIEDTREASURE = "Hope this is worth the work.",
		BUSHHAT = "This makes me feel at one with nature.",
		BUTTER = "Haha. Slippery!",
		BUTTERFLY =
		{
			GENERIC = "What a pretty pattern!",
			HELD = "You're my pretty pocket pal.",
		},
		BUTTERFLYMUFFIN = "What a pretty muffin!",
		BUTTERFLYWINGS = "I feel pretty bad about that.",
		CAMPFIRE =
		{
			EMBERS = "I need fuel for my fire!",
			GENERIC = "My cozy campfire!",
			HIGH = "Uhh... I hope this doesn't spread.",
			LOW = "I think it needs some energy!",
			NORMAL = "My cozy campfire!",
			OUT = "Well, that's that.",
		},
		CANE = "I cane, I saw.",
		CARROT = "They must've named these after oranges.",
		CARROT_COOKED = "Smells good!",
		CARROT_PLANTED = "Ooo, that's a good find!",
		CARROT_SEEDS = "I could grow any kine with these.",
		CARTOGRAPHYDESK = 
		{	
			GENERIC = "Check out this map! Now we're getting somewhere!",
			BURNING = "Goodbye, knowledge. I barely knew ya.",
			BURNT = "Bummer. Not much use to anyone now.",
		},
		CAVE_BANANA = "Potassium!",
		CAVE_BANANA_COOKED = "Cooked potassium!",
		CHARCOAL = "The remains of something burnt.",
		CHESSPIECE_PAWN = 
        {
		GENERIC = "Nothing but a pawn.",
		},
        CHESSPIECE_ROOK = 
        {
			GENERIC = "I'm not familiar with whatever that is.",
			STRUGGLE = "Is it hatching?",
		},
        CHESSPIECE_KNIGHT = 
        {
			GENERIC = "Why the long face?",
			STRUGGLE = "Is it hatching?",
		},
        CHESSPIECE_BISHOP = 
        {
			GENERIC = "Looks pretty weird.",
			STRUGGLE = "Is it hatching?",
		},
		CHESSPIECE_MUSE = "Looks like something I'd never want to live up to.",
        CHESSPIECE_FORMAL = "Looks like a dork. A big one.",
        CHESSPIECE_HORNUCOPIA = "Reminds me of food. I could sure use some right about now...",
        CHESSPIECE_PIPE = "I wanna gag just thinking about it.",
        CHESSPIECE_DEERCLOPS = "Hang ten, stone buddy.",
        CHESSPIECE_BEARGER = "See? They're a lot better when they're just chilling.",
        CHESSPIECE_MOOSEGOOSE = "It's like I'm looking at the real thing!",
        CHESSPIECE_DRAGONFLY = "Totally wicked statue.",
		CHESSPIECE_MINOTAUR = "Hope he likes not being in a maze...",
		CHESSPIECE_BUTTERFLY = "This one's my favorite.",
        CHESSPIECE_ANCHOR = "My hands are calloused just thinking about it.",
        CHESSPIECE_MOON = "You look much better in the night sky.",
		CHESSPIECE_CARRAT = "Talk about a sweet victory.",
		CHESSPIECE_MALBATROSS = "Better like this than tugging at my board.",
        CHESSPIECE_CRABKING = "Maxwell's the real crab king around here.",
        CHESSPIECE_TOADSTOOL = "Sweet decoration!",
        CHESSPIECE_STALKER = "It's not made out of bones, is it?",
        CHESSPIECE_KLAUS = "That'll keep him away from the milk and cookies.",
        CHESSPIECE_BEEQUEEN = "Every queen's gotta have busts her of face all over the palace.",
        CHESSPIECE_ANTLION = "Sweet stone bug.",
		CHESSPIECE_BEEFALO = "Its fur isn't as soft as the real thing...",
		CHESSPIECE_KITCOON = "This is one of the cuter statues.",
		CHESSPIECE_CATCOON = "Now I'm missing their little meows.",
        CHESSPIECE_GUARDIANPHASE3 = "I'm a fan of keeping my past behind me.",
		CHESSPIECE_EYEOFTERROR = "You're much easier to look at this way.",
        CHESSPIECE_TWINSOFTERROR = "I don't think I need reminders of these.",

		CHESSJUNK1 = "Lots of potential here!",
		CHESSJUNK2 = "I hope someone makes something out of that.",
		CHESSJUNK3 = "Look at all those parts.",
		CHESTER = "The best kind of friend... one who works for free.",
		CHESTER_EYEBONE =
		{
			GENERIC = "I get the feeling I'm being watched.",
			WAITING = "Nap time!",
		},
		COLDFIRE =
		{
			EMBERS = "Don't leave me, light!",
			GENERIC = "I'm not really sure how this thing works.",
			HIGH = "Chillin'.",
			LOW = "That fire is slacking off.",
			NORMAL = "The fire is doing it's job.",
			OUT = "The fire is taking a break.",
		},
		COLDFIREPIT =
		{
			EMBERS = "Don't leave me, light!",
			GENERIC = "I'm not really sure how this thing works.",
			HIGH = "Chillin'.",
			LOW = "That fire is slacking off.",
			NORMAL = "The fire is doing it's job.",
			OUT = "The fire is taking a break.",
		},
		COMPASS =
		{
			GENERIC = "How do I use this thing again?",
			N = "North-ish?",
			S = "South-ish?",
			E = "East-ish?",
			W = "West-ish?",
			NE = "Northeast-ish?",
			SE = "Southeast-ish?",
			NW = "Northwest-ish?",
			SW = "Southwest-ish?",
		},
		COOKEDMEAT = "Surfing fuel!",
		COOKEDMONSTERMEAT = "This still looks awful.",
		COOKEDSMALLMEAT = "A tasty morsel.",
		COOKPOT =
		{
			BURNT = "The food might taste a bit burnt.",
			COOKING_LONG = "If I watch it it'll never boil.",
			COOKING_SHORT = "Food incoming!",
			DONE = "I cooked something. Yum!",
			EMPTY = "My kind of cooking... Just dump everything in and wait!",
		},
		CORN = "Aw, shucks.",
		CORN_COOKED = "I'll need a toothpick soon.",
		CORN_SEEDS = "I could grow some food of my own with these.",
		CUTGRASS = "It makes a swishing sound when I walk.",
		DEPLETED_GRASS =
		{
			GENERIC = "That grass looks pretty dead.",
		},
		GOGGLESHAT = "Choka! I'll look like I know what I'm doin' in these.",
        DESERTHAT = "Sweet, sweet sand protection.",
		DEVTOOL = "Useful!",
		DIRTPILE = "Well, would you look at that. A pile of dirt.",
		DIVININGROD =
		{
			COLD = "This rod isn't doing all that much.",
			GENERIC = "What a silly looking device.",
			HOT = "This rod is going crazy!",
			WARM = "I think the rod is trying to tell me something!",
			WARMER = "Woah, the rod is really going now.",
		},
		DRAGONFRUIT_SEEDS = "I could grow any kine fruit with these.",
		DRAGONPIE = "Delicious pie!",
		DUG_BERRYBUSH = "Planting this sounds like hard work.",
		DUG_BERRYBUSH_JUICY = "Planting this sounds like hard work.",
		DUG_GRASS = "Planting this sounds like hard work.",
		DUG_MARSH_BUSH = "Planting this sounds like hard work.",
		DUG_SAPLING = "Planting this sounds like hard work.",
		DURIAN = "Looks pretty difficult to eat.",
		DURIAN_COOKED = "Woah! That smells awful!",
		DURIAN_SEEDS = "I could grow, uh, da kine with these.",
		EARMUFFSHAT = "Keeps my ears fluffy and warm.",
		EGGPLANT_SEEDS = "I could grow any kine with these.",
		
		ENDTABLE = 
		{
			BURNT = "Eh, it's still fancy to me.",
			GENERIC = "That's one fancy table.",
			EMPTY = "Someone should put something nice there.",
			WILTED = "Looking rough.",
			FRESHLIGHT = "That's pretty nice.",
			OLDLIGHT = "Somebody should probably give it some more life.",
		},

		-----------------------------------------------------
		EVERGREEN =
		{
			BURNING = "That's not cool.",
			BURNT = "I know the feeling.",
			CHOPPED = "Sliced and diced.",
			GENERIC = "Reminds me of winter. Yuck.",
		},
		EVERGREEN_SPARSE =
		{
			BURNING = "That's not cool.",
			BURNT = "He's a burnout.",
			CHOPPED = "Cut down in its prime.",
			GENERIC = "Looks like the diet version.",
		},
		EYEBRELLAHAT = "My third eye will protect me.",
		TWIGGYTREE = 
		{
			BURNING = "That's not cool.",
			BURNT = "Someone overcooked their noodles.",
			CHOPPED = "Totally chopped.",
			GENERIC = "That's one noodly tree.",			
			DISEASED = "That's a bummer.",
		},
		TWIGGY_NUT_SAPLING = "That was a lot of work for a total noodle.",
        TWIGGY_OLD = "It's looking a little on the lighter side.",
		TWIGGY_NUT = "This one must be healthier for you.",
		EYEPLANT = "Made you look!",
		--INSPECTSELF = "Hang in there...",
		EYETURRET = "It's looking out for me.",
		EYETURRET_ITEM = "It's got my back.",
		FABRIC = "Where did my sticks go? I've been bamboozled!",
		FARMPLOT =
		{
			BURNT = "It's toast!",
			GENERIC = "Looks like manual labor to me.",
			GROWING = "I hope it's organic.", --Earth is having a ground-child
			NEEDSFERTILIZER = "That shrub could use some grub.",
		},
		FEATHERFAN = "An artificial ocean breeze.",
		MINIFAN = "You get what you make, I guess.",
		FEATHERHAT = "I'm at one with my feathered friends.",
		FEATHERSAIL = "For smooth and swift sailing!",
		FEATHER_CROW = "A real softy.",
		FEATHER_ROBIN = "I'm tickled by the color of this.",
		FEATHER_ROBIN_WINTER = "A portable reminder of the beauty of nature.",
		FEATHER_CANARY = "I would love a necklace made of these.",
		FEATHERPENCIL = "Lightweight. I like it.",
		COOKBOOK = "How am I supposed to pick what to eat?",
		FEM_PUPPET = "Bummer.",
		FERTILIZER = "Ground food.",
		FIREFLIES =
		{
			GENERIC = "My kind of insects.",
			HELD = "My glowy little pocket pals.",
		},
		FIREHOUND = "This one seems angrier somehow.",
		FIREPIT =
		{
			EMBERS = "This fire is tired.",
			GENERIC = "Oh, sweet enemy of darkness.",
			HIGH = "Heh, woah! Hot stuff!",
			LOW = "It's barely doing its job.",
			NORMAL = "Could be better... could be worse.",
			OUT = "It's taking a break.",
		},
		FIRESTAFF = "It's a hot commodity.",
		FIRESUPPRESSOR =
		{
			LOWFUEL = "It needs some juice.",
			OFF = "It's out cold.",
			ON = "Fling it on!",
		},
		FISHINGROD = "Have rod, will travel.",
		FISHSTICKS = "Looks like these fish are gonna be stickin' around.",
		FISHTACOS = "All times are taco times.",
		FISH_COOKED = "From my own personal recipe.",
		FISH_RAW = "Needs more fire.",
		FLINT = "This rock doesn't roll.",
		FLOWER = 
		{
            GENERIC = "I'm very picky when it comes to these. Heh heh heh.",
            ROSE = "This one's my favorite out of them all.",
        },
		FLOWER_WITHERED = "Looks like it was out surfing too long.",
		FLOWERHAT = "That's a happy looking headpiece!",
		FLOWERSALAD = "Now that's what I call health food.",
		FLOWER_EVIL = "Flowers shouldn't be that scary.",
		FOLIAGE = "It's all natural.",
		FOOTBALLHAT = "I feel like I could tackle anything.",
		FOSSIL_PIECE = "Poor dude.",
        FOSSIL_STALKER =
        {
			GENERIC = "It's missing bits. Aren't we all?",
			FUNNY = "Ha! You're looking pretty gnarly.",
			COMPLETE = "It's been fixed up, now what?",
        },
		STALKER = "He's really sad, in a sense.",
		STALKER_ATRIUM = "This bone head's got nothing on us!",
        STALKER_MINION = "I'm sorry, dude.",
		THURIBLE = "I'm hoping this thing's here for Kōkua.",
        ATRIUM_OVERGROWTH = "I can't read any of this stuff.",
		FRUITMEDLEY = "A sweet treat!",
		FURTUFT = "Furry.", 
		GEARS = "Looks like robot guts.",
		GOLDENAXE = "That's one treasury tool.",
		GOLDENPICKAXE = "I picked a good one.",
		GOLDENPITCHFORK = "It makes work feel shiny!",
		GOLDENSHOVEL = "I'm digging this fancy shovel.",
		GOLDNUGGET = "It's shiny and useful!",
		GRASS =
		{
			BARREN = "It needs help to grow.",
			BURNING = "That won't last.",
			GENERIC = "For all your grassy needs.",
			PICKED = "It will be back.",
			WITHERED = "It looks tired.",
			DISEASED = "I can relate...",
			DISEASING = "Something's hurting that plant.",
		},
	    GRASSGEKKO = 
		{
			GENERIC = "That gekko's made out of grass, cool.",	
			DISEASED = "Poor little dude.",
		},
		GRASS_UMBRELLA = "This should keep me almost dry.",
		GREEN_CAP = "Looks sort of tasty.",
		GREEN_CAP_COOKED = "Fried fungus.",
		GREEN_MUSHROOM =
		{
			GENERIC = "Funky fungi.",
			INGROUND = "It's taking a nap.",
			PICKED = "Maybe it's thirsty.",
		},
		GUACAMOLE = "A total snack attack.",
		GUNPOWDER = "It goes boom.",
		HAMBAT = "I can fight food with food.",
		HAMMER = "Smashing stuff *sounds* like work, but it's pretty fun.",
		HAWAIIANSHIRT = "This shirt is so me.",
		HEALINGSALVE = "It helps me live a little.",
		HEATROCK =
		{
			COLD = "It's icy!",
			FROZEN = "Refreshing.",
			GENERIC = "It's rock temperature.",
			HOT = "It's warm and glowing.",
			WARM = "It's a little toasty.",
		},
		HOMESIGN =
		{
			BURNT = "I know the feeling.",
			UNWRITTEN = "Marks nowhere.",
			GENERIC = "Hang ten!",
		},
		ARROWSIGN_POST =
		{
			GENERIC = "It's telling me where to go.",
            UNWRITTEN = "Nothing there.",
			BURNT = "Someone must not want me to go there.",
		},
		ARROWSIGN_PANEL =
		{
			GENERIC = "It's telling me where to go.",
            UNWRITTEN = "Nothing there.",
			BURNT = "Someone must not want me to go there.",
		},
		HONEY = "Sticky, sweet and good to eat!",
		HOUND = "This dog is a real drag!",
		HOUNDCORPSE =
		{
			 GENERIC = "Can someone take care of that. It's kinda killing the mood.",
			 BURNING = "I really hate to look at that. And smell it.",
			 REVIVING = "Nope!",
		},
		ICEPACK = "Keeps my snacks as cool as I am.",
		ICESTAFF = "Too cold to hold.",
		INSANITYROCK =
		{
			ACTIVE = "Ouch. My thoughts are hurting.",
			INACTIVE = "That's one dark tower.",
		},
		JAMMYPRESERVES = "Preserve those jams.",
		JUNGLETREE =
		{
			BURNING = "That's not cool.",
			BURNT = "I know the feeling.",
			CHOPPED = "Sliced and diced.",
			GENERIC = "It's like nature's canopy.",
		},
		KABOBS = "It's like a meatsicle!",
		KILLERBEE =
		{
			GENERIC = "It's badly BEEhaved.",
			HELD = "This bee needs to relax.",
		},
		KRAMPUS = "Stealing isn't cool!",
		KRAMPUS_SACK = "A bag of stolen goods.",
		LANTERN = "It really lightens the mood.",
		LAVAPOOL = "It's like a gooey campfire!",		
		HUTCH = "The best kind of friend - one who works for free.",
        HUTCH_FISHBOWL =
        {
            GENERIC = "You're pretty chill fish.",
            WAITING = "It's probably just sleeping.",
        },
		LAVASPIT =
		{
			COOL = "All dried up.",
			HOT = "Looks a little spicy.",
		},
		LAVA_POND = "It's like a gooey campfire!",
		LAVAE_COCOON = "Nice and chill.",
		LAVAE = "That's one spicy dude.",
		LAVAE_PET = 
		{
			STARVING = "It's about to start chomping at my boots.",
			HUNGRY = "That thing's hungry.",
			CONTENT = "It's happy with me.",
			GENERIC = "It's a pretty cute lava slug.",
		},
		LAVAE_EGG = 
		{
			GENERIC = "Something's warm in there.",
		},
		LAVAE_EGG_CRACKED =
		{
			COLD = "It's cold, but still mildly warm.",
			COMFY = "That egg's pretty chill.",
		},
		LAVAE_TOOTH = "It's a weird tooth.",

		LIGHTNING_ROD =
		{
			CHARGED = "It's fully energized!",
			GENERIC = "It's powered by electricity.",
		},
		LIVINGLOG = "It looks bummed.",
		LOG =
		{
			BURNING = "Waste of a good board.",
			GENERIC = "There's a board in there.",
		},
		LUREPLANT = "It's so attractive.",
		LUREPLANTBULB = "Get meat, the easy way.",
		MANDRAKE_ACTIVE = "You're messing up my chill vibes, dude.",
		MANDRAKE_PLANTED = "A mystical plant.",
		MANDRAKE = "I was rooting for it.",
		MANDRAKESOUP = "Mystical soup.",
		MANDRAKE_COOKED = "Not as mystical as I thought.",
		MANGROVETREE = "This tree really likes water.",
		MANGROVETREE_BURNT = "How?",
		MARSH_BUSH =
		{
		    BURNT = "They're the same spikes, just burnt now.",
			BURNING = "The spikes are burning. Good.",
			GENERIC = "I don't need spikes.",
			PICKED = "Yeoch!",
		},
		MARSH_PLANT = "Planty.",
		MARSH_TREE =
		{
			BURNING = "I've never seen such a spiky fire.",
			BURNT = "Still spiky.",
			CHOPPED = "Not so spiky now.",
			GENERIC = "Me and spikes don't get along.",
		},
		MEAT = "Five second rule!",
		MEATBALLS = "Saucy.",
		MEATRACK =
		{
			BURNT = "Your days of jerkying are over.",
			DONE = "That's some good looking jerky.",
			DRYING = "See you later, water.",
			DRYINGINRAIN = "Water doesn't make things dry.",
			GENERIC = "This rack could use some meat.",
			DONE_NOTMEAT = "That's some good looking food.",
            DRYING_NOTMEAT = "See you later, water.",
            DRYINGINRAIN_NOTMEAT = "Water doesn't make things dry.",
		},
		MEAT_DRIED = "Boy, this meat is jerky.",
		MERM = "Go dunk your head, guy.",
		MERMHEAD =
		{
			BURNT = "I like it better burnt.",
			GENERIC = "As if the pigs didn't smell bad enough.",
		},
		MERMHOUSE =
		{
			BURNT = "This house has char-acter.",
			GENERIC = "I could have sworn this was an outhouse.",
		},
		MINERHAT = "Another job I hope to never do.",
		MONSTERLASAGNA = "Still smells monstery...",
		MONSTERMEAT = "Am I really hungry enough?",
		MONSTERMEAT_DRIED = "Tough to chew AND crazy-making!",
		MULTITOOL_AXE_PICKAXE = "I'm picky when it comes to tools.",
        MUSHROOMHAT = "A few flowers on it would go a long way.",
        MUSHROOM_LIGHT2 =
        {
            ON = "A little light never hurt anyone.",
            OFF = "Nautical light.",
            BURNT = "Wasn't me this time.",
        },
        MUSHROOM_LIGHT =
        {
            ON = "That's a pretty mystical mushroom.",
            OFF = "Hey, I won't judge decoration choices.",
            BURNT = "You have to remember to turn it off too...",
        },
		SLEEPBOMB = "Forms a free nap circle! My new favorite kind of sleeping bag!",
        MUSHROOMBOMB = "I never learned which ones were safe to eat and which ones were explosive!",
        SHROOM_SKIN = "Grody!",
        TOADSTOOL_CAP =
        {
            EMPTY = "That's a walking hazard.",
            INGROUND = "Something's in there.",
            GENERIC = "Looks poisonous to me.",
        },
        TOADSTOOL =
        {
            GENERIC = "This dude's harboring some major stink bombs!",
            RAGE = "There's no chilling with this guy!",
        },
        MUSHROOMSPROUT =
        {
            GENERIC = "Gag! Reeks of an ōpala dump!",
            BURNT = "There's better options than that.",
        },
		MUSHTREE_TALL =
        {
            GENERIC = "That's a whole lot of mushtree!",
            BLOOM = "That smell is gnarly, dude.",
        },
        MUSHTREE_MEDIUM =
        {
            GENERIC = "Medium mushness.",
            BLOOM = "Do you mind blooming in another direction?",
        },
        MUSHTREE_SMALL =
        {
            GENERIC = "Grow tree! Mush! Mush!",
            BLOOM = "You're blooming a little too close for comfort.",
        },
        MUSHTREE_TALL_WEBBED = "Those spiders must be picky.",
        SPORE_TALL = "Looks like a pretty fun guy.",
        SPORE_MEDIUM = "It really makes you just wanna lie down and never get back up.",
        SPORE_SMALL = "It's so chill, these things are like family.",
        SPORE_TALL_INV = "If I can save one of these dudes from perishing, something can save me.",
        SPORE_MEDIUM_INV = "If I can save one of these dudes from perishing, something can save me.",
        SPORE_SMALL_INV = "There's still some hope out there...",
		NIGHTLIGHT = "Lights the night with 'mares.",
		NIGHTMAREFUEL = "The stuff of nightmares.",
		NIGHTSWORD = "Wield the dark.",
		NITRE = "Not just any old dumb rock.",
		OBSIDIAN = "Ouch, you're hot!",
		ONEMANBAND = "Let's get this jam sesh rollin'!",
		OASISLAKE = 
		{
			GENERIC = "Wonder how deep it goes...",
			EMPTY = "Maybe I could sand board in that.",
		},
		PANFLUTE = "Plays hypnotic tunes.",
		PAPYRUS = "Bet this burns good.",
        WAXPAPER = "No thanks, I'm not waxing.",
		PARROT = "Will you ever stop squawking?",
		PEACOCK = "A tropical turkey!",
		PEROGIES = "Comfort food!",
		PETALS = "Flowers totally get me.",
		PETALS_EVIL = "These smell like bad karma.",
		PICKAXE = "My back hurts just looking at it.",
		PIGSKIN = "Not by the hair of his rumpy rump rump.",
		PIRATEHAT = "High fashion for the high seas.",
		PITCHFORK = "Looks like some kind of trident.",
		PLANTMEAT = "It's got all two food groups.",
		PLANTMEAT_COOKED = "Hey, you're not a REAL steak!",
		PLANT_NORMAL =
		{
			GENERIC = "Farming is so hard!",
			GROWING = "It's still just a child.",
			READY = "Can I get to the eating part now?",
			WITHERED = "Something made it all... gross.",
		},
		POMEGRANATE = "Seedy.",
		POMEGRANATE_COOKED = "Hot'n'juicy.",
		POMEGRANATE_SEEDS = "Ugh, these should go in the ground I guess...",
		POOP = "Surf sewage!",
		POWCAKE = "I'll eat whatever.",
		PURPLEAMULET = "This thing drives me nuts!",
		PURPLEGEM = "Shiny!",

		RAINOMETER =
		{
			BURNT = "Guess you didn't see that coming.",
			GENERIC = "Tells me when to paddle in.",
		},
		RATATOUILLE = "I'd rather get curvy than scurvy.",
		RAZOR = "You're not coming near my legs!",
		REDGEM = "Booty-ful!",
		RED_CAP_COOKED = "I'm not THAT hungry.",
		REEDS =
		{
			BURNING = "Whoa. Heavy.",
			GENERIC = "More grass. Righteous.",
			PICKED = "Catch you on the flip flop.",
		},
		REFLECTIVEVEST = "Even sun worshippers need a break.",
		RESEARCHLAB =
		{
			BURNT = "Ugh, do I have to build you all over again now?",
			GENERIC = "I wish you were a do-all-the-work-for-me machine.",
		},
		RESEARCHLAB2 =
		{
			BURNT = "Ugh, do I have to build you all over again now?",
			GENERIC = "I still feel like I'm doing most of the work here.",
		},
		RESEARCHLAB3 =
		{
			BURNT = "Couldn't you have manipulated yourself into not getting burnt?",
			GENERIC = "Manipulating shadows sounds tough, glad I don't have to.",
		},
		RESURRECTIONSTATUE =
		{
			BURNT = "Up in smoke...",
			GENERIC = "Hope I never need you, weird wood Wilson...",
		},
		RESURRECTIONSTONE = "You better work.",
		ROCK = "I like you much better on land.",
		ROCKS = "Not flat. Won't skip.",
		ROCK_LIGHT =
		{
			GENERIC = "Gnarly!",
			LOW = "The lava is cooling.",
			NORMAL = "Gnarly!",
			OUT = "This fire is cooked.",
		},
		CAVEIN_BOULDER =
        {
            GENERIC = "Someone push this out of the way for me.",
            RAISED = "You'd get quite the scratch trying to get that.",
        },
		ROPE = "Good for a lot of boring stuff.",
		ROTTENEGG = "Smells like a ship cabin.",
		ROYAL_JELLY = "That's one sweet pile of jelly.",
        JELLYBEAN = "Pop 'em in my mouth, one after the other or all at once.",
		SADDLE_BASIC = "For riding things other than waves.",
        SADDLE_RACE = "It's like riding a wave only hairier.",
        SADDLE_WAR = "Is there a peace saddle?",
        SADDLEHORN = "Oh, that'll help getting the saddle off.",
	    SALTLICK = "Salt makes everything taste better. Even salt.",
        BRUSH = "Brushing beefalo hair is so relaxing.",
		RUINSHAT = "Fit for the queen boss!",
		RUINS_BAT = "Know a few scallywags I'd like to introduce this to.",
		SAPLING =
		{
			BURNING = "Not much to burn.",
			GENERIC = "Board material?",
			PICKED = "Looks noodled.",
			WITHERED = "Looks like I feel.",
		    DISEASED = "I feel you, plant.",
			DISEASING = "I'm probably next.",
		},
		SCARECROW = 
   		{
			GENERIC = "That's a weird straw dude.",
			BURNING = "Bummer.",
			BURNT = "If it didn't scare anyone before, it sure does now.",
   		},
		SCULPTINGTABLE=
   		{
			EMPTY = "I like making my art hands free.",
			BLOCK = "I'm not really good at any of this arts and crafts stuff.",
			SCULPTURE = "Pretty good work, dude.",
			BURNT = "That's a burnt table. Harsh.",
   		},
		SCULPTURE_KNIGHTHEAD = "Do I have to carry that? I'd really rather not.",
		SCULPTURE_KNIGHTBODY = 
		{
			COVERED = "I'm no art expert but that art seems a bit odd.",
			UNCOVERED = "Caught ya cracking, buddy.",
			FINISHED = "You're good now, dude.",
			READY = "Something wicked is happening now.",
		},
        SCULPTURE_BISHOPHEAD = "Did somebody lose a head?",
		SCULPTURE_BISHOPBODY = 
		{
			COVERED = "I try not to think about the work that had to go into these.",
			UNCOVERED = "Now it's turning into something...",
			FINISHED = "Lookin' good there, friend.",
			READY = "Something wicked is happening now.",
		},
        SCULPTURE_ROOKNOSE = "Ugh, who wants to carry this for me?",
		SCULPTURE_ROOKBODY = 
		{
			COVERED = "This thing's vibes are all off.",
			UNCOVERED = "Still in chunks, but a little less boring now.",
			FINISHED = "You're looking as good as new, buddy.",
			READY = "Something wicked is happening now.",
		},
        GARGOYLE_HOUND = "I probably shouldn't stand so close to that.",
        GARGOYLE_WEREPIG = "That thing's messing with my head.",
		SEEDS = "Ugh, do I have to like, plant these?",
		SEEDS_COOKED = "They get stuck in my teeth.",
		SEWING_KIT = "I'm not really the \"sewing\" kind.",
		SEWING_TAPE = "Just slap the tape on whatever, that'll fix it. No-sweat.",
		SHOVEL = "Ugh, digging...",
		SIESTAHUT =
		{
			BURNT = "Rest in peace.",
			GENERIC = "I could use a snooze.",
		},
		SILK = "Spider gunk...",
		SKELETON = "Surfing the beyond...",
		SKULLCHEST = "This won't Kramp my style.",
		SMALLBIRD =
		{
			GENERIC = "Eye see you.",
			HUNGRY = "Hope your stomach is as big as your eye.",
			STARVING = "Blink twice if you're hungry.",
			SLEEPING = "Sleep well, little guy.",
		},
		SMALLMEAT = "Mmmmmeat!",
		SMALLMEAT_DRIED = "A treat that travels well.",
		SPAT = "Hey there, little guy!",
		PHLEGM = "Blech.",
		SPEAR = "For stabbing problems.",
		SPIDER =
		{
			DEAD = "I wonder if it will rain now?",
			GENERIC = "Crawly creep.",
			SLEEPING = "Aww, it's so peaceful and disgusting.",
		},
		SPIDERDEN = "They don't like trespassers.",
		SPIDEREGGSACK = "Starts a spider farm.",
		SPIDERGLAND = "Why does health food taste so bad?",
		SPIDERHAT = "Ha! I'm your boss now, spiders!",
		SPIDERQUEEN = "I don't bow to royalty.",
		SPIDER_WARRIOR =
		{
			DEAD = "Now I can rest in peace.",
			GENERIC = "Back off, you're germy!",
			SLEEPING = "Hope you're having a nightmare.",
		},
		SPOILED_FOOD = "What a waste.",

		STINGER = "Glad I don't have to pick this out of my skin.",
		STRAWHAT = "Keeps my face from getting freckly.",
		STUFFEDEGGPLANT = "Health grub.",
	--	SURFBOARD = "My love for you is deeper than the ocean.",
	--	SURFBOARD_ITEM = "My pride and joy!",
		TAFFY = "Maybe I could wax my board with this stuff.",
		TALLBIRD = "I'm glad it can't fly.",
		TALLBIRDEGG = "There's a little dude relaxin' inside.",
		TALLBIRDEGG_COOKED = "Now if I could only get giant toast.",
		TALLBIRDEGG_CRACKED =
		{
			COLD = "Gelatinous...",
			GENERIC = "Raw...",
			HOT = "Hope it doesn't hard boil.",
			LONG = "It's growing...",
			SHORT = "C'mon squirt.",
		},
		TALLBIRDNEST =
		{
			GENERIC = "Bunch of future pecky jerks.",
			PICKED = "The eggs have flown the coop.",
		},
		TEENBIRD =
		{
			GENERIC = "It's got a ton of acne under those feathers. Haha, gross.",
			HUNGRY = "They're always hungry!",
			STARVING = "You've eaten me out of base and camp!",
			SLEEPING = "I slept just as much when I was your age.",
		},
		TENT =
		{
			BURNT = "Great! Just great...",
			GENERIC = "A cool, dry place to catch some Zzz's.",
		},
		TOPHAT = "Not quite my style...",
		TORCH = "Take that, night!",
		TRAILMIX = "Protein!",
		TRANSISTOR = "Thing-y what does the science stuff!",
		TRAP = "Catch me a beach breakfast!",
		TRAP_TEETH = "Ooh, that looks nasty!",
		TRAP_TEETH_MAXWELL = "Must avoid...",
		TREASURECHEST =
		{
			BURNT = "Nooooo! Goodbye shiny, shiny treasure...",
			GENERIC = "Treasure!",
		},
		TREASURECHEST_TRAP = "Tricky treasure!",
		TRINKET_1 = "Gross.",
		TRINKET_2 = "Aww, but I like real kazooing...",
		TRINKET_3 = "Gord only knows what this is good for.",
		TRINKET_4 = "Gnome thanks.",
		TRINKET_5 = "Zzzzzzzoooooom!",
		TRINKET_6 = "Don't wanna touch this with wet hands...",
		TRINKET_7 = "Ooh, I'm good at this!",
		TRINKET_8 = "Hardened what?",
		TRINKET_9 = "I don't care if things are matchy.",
		TRINKET_10 = "I might need these if I ever take a gnarly wipeout.",
		TRINKET_11 = "Hey there lil dude. Wanna come with me?",
		TRINKET_12 = "Desse-what?",
		TRINKET_13 = "Gnome thanks.",
		TRINKET_14 = "Its lime and tiny umbrella wearing days are over.", --Leaky Teacup
		TRINKET_15 = "All that work put on one little dude.", --Pawn
		TRINKET_16 = "All that work put on one little dude.", --Pawn
		TRINKET_17 = "Nothing wrong with be a little bent out of shape.", --Bent Spork
		TRINKET_18 = "Yeah, I'm not falling for that one.", --Trojan Horse
		TRINKET_19 = "I hope my chakras don't get unbalanced like that.", --Unbalanced Top
		TRINKET_20 = "How did I ever scratch my back before this?", --Backscratcher
		TRINKET_21 = "I think the eggs won that battle.", --Egg Beater
		TRINKET_22 = "Poor dude, all frayed.", --Frayed Yarn
		TRINKET_23 = "That thing's too much work to use.", --Shoehorn
		TRINKET_24 = "Has Warly put any grinds in it yet?", --Lucky Cat Jar
		TRINKET_25 = "Pilau!", --Air Unfreshener
		TRINKET_26 = "Somebody loved this dearly, I can tell.", --Potato Cup
		TRINKET_27 = "I try not to get hung up on stuff either.", --Coat Hanger
		TRINKET_28 = "That game requires waaay too much thinking", --Rook
        TRINKET_29 = "That game requires waaay too much thinking", --Rook
        TRINKET_30 = "Return to sender.", --Knight
        TRINKET_31 = "Return to sender.", --Knight
		TRINKET_32 = "I'd rather see the present. I'm good at that already!", --Cubic Zirconia Ball
        TRINKET_33 = "I know a little someone who would look good in this!", --Spider Ring
        TRINKET_34 = "Sheesh. I'd need a really bad day to wish on that.", --Monkey Paw
        TRINKET_35 = "Some ocean water would liven it up.", --Empty Elixir
		TRINKET_36 = "It's kinda hard to eat grinds with these in.", --Faux fangs
		TRINKET_37 = "Now I could take on two vampires at once.", --Broken Stake
		TRINKET_38 = "Bummer. I can't really see anything with these.", -- Binoculars Griftlands trinket
        TRINKET_39 = "I could make it my life's mission to find its pair. Or I could nap. Latter sounds good.", -- Lone Glove Griftlands trinket
        TRINKET_40 = "I prefer snails over scales.", -- Snail Scale Griftlands trinket
        TRINKET_41 = "It's broken, but still warm. Huh.", -- Goop Canister Hot Lava trinket
        TRINKET_42 = "They're much nicer without the bite.", -- Toy Cobra Hot Lava trinket
        TRINKET_43 = "I think my brother had something like this in small kid time.", -- Crocodile Toy Hot Lava trinket
        TRINKET_44 = "I kinda wish it wasn't busted.", -- Broken Terrarium ONI trinket
        TRINKET_45 = "That's one doofy device.", -- Odd Radio ONI trinket
        TRINKET_46 = "I'm pretty good at drying off by myself.", -- Hairdryer ONI trinket

		-- The numbers align with the trinket numbers above.
        LOST_TOY_1  = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_2  = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_7  = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_10 = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_11 = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_14 = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_18 = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_19 = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_42 = "You've got one weird ghost toy, iki keiki.",
        LOST_TOY_43 = "You've got one weird ghost toy, iki keiki.",

		HALLOWEENCANDY_1 = "Nice, I'll just go lay down and chew on this for a while.",
        HALLOWEENCANDY_2 = "I love these things, just pop 'em all in my mouth.",
        HALLOWEENCANDY_3 = "I prefer the candy version.",
        HALLOWEENCANDY_4 = "I'll eat anything at this point.",
        HALLOWEENCANDY_5 = "I'll be taking this, and eating it. Mahalo!",
        HALLOWEENCANDY_6 = "I was told not to eat these... Who am I to follow rules?",
        HALLOWEENCANDY_7 = "I could eat entire boat loads of these and not notice.",
        HALLOWEENCANDY_8 = "I'll enjoy every second of this.",
        HALLOWEENCANDY_9 = "Just gonna gobble this thing right up.",
        HALLOWEENCANDY_10 = "I'm gonna enjoy every second of this.",
        HALLOWEENCANDY_11 = "I'll take twenty. Shoots, more than that.",
		HALLOWEENCANDY_12 = "This isn't exactly what I'd call a treat...", --ONI meal lice candy
        HALLOWEENCANDY_13 = "Busts your jaw like taking a fierce mullering!", --Griftlands themed candy
        HALLOWEENCANDY_14 = "Wicked! I love spicy candy!", --Hot Lava pepper candy
        CANDYBAG = "Maybe more treats will appear if I keep opening and closing it.",
		
		HALLOWEEN_ORNAMENT_1 = "That's a spooky one.",--Ghost
		HALLOWEEN_ORNAMENT_2 = "Someone should hang this up somewhere.",--Bat
		HALLOWEEN_ORNAMENT_3 = "This might look better on a tree. The fake ones at least.",--Spider
		HALLOWEEN_ORNAMENT_4 = "Alright, who's gonna hang this one up?",--Tentacle
		HALLOWEEN_ORNAMENT_5 = "It's only natural to hang this one up.",--Dangling Spider
		HALLOWEEN_ORNAMENT_6 = "This would make a pretty sick charm.",--Crow

		HALLOWEENPOTION_DRINKS_WEAK = "Meh. I'll take what I can get.",
		HALLOWEENPOTION_DRINKS_POTENT = "Totally wicked!",
        HALLOWEENPOTION_BRAVERY = "Comes with a chill pill.",
		HALLOWEENPOTION_FIRE_FX = "Anybody know where I could get ocean in a bottle?", 
		HALLOWEENPOTION_MOON = "Nothing like a little bit of moon.",
		MADSCIENCE_LAB = "I'd prefer a more chill science.",
		LIVINGTREE_ROOT = "I think I see a tiny surfboard in there.", 
		LIVINGTREE_SAPLING = "It's gonna grow into one scary lookin' tree.",
		
		DRAGONHEADHAT = "Pretty feisty, good for show.",
        DRAGONBODYHAT = "Am I stuck in the middle with this thing?",
        DRAGONTAILHAT = "The root chakra. Also the okole.",
        PERDSHRINE =
        {
            GENERIC = "Is it asking something of me?",
            EMPTY = "That shrine could use a bush.",
            BURNT = "Not much use to anyone now.",
        },
        LUCKY_GOLDNUGGET = "This golden thing's pretty lucky lookin'.",
		FIRECRACKERS = "Oh, I remember these!",
        REDLANTERN = "Looks like some kinda festival decoration.",
        PERDFAN = "Sweet relief.",
        REDPOUCH = "Anything good in there?",
		WARGSHRINE = 
        {
            GENERIC = "Is it asking something of me? Sorry, bud, but we know how that'll end.",
            EMPTY = "That shrine could use a torch.",
            BURNT = "Not much use to anyone now.",
        },
        CLAYWARG = 
        {
        	GENERIC = "That thing's moving! And I'm moving out of the way!",
        	STATUE = "Yeesh. I'll keep my distance.",
        },
        CLAYHOUND = 
        {
        	GENERIC = "Anybody have any sticks to fetch!?",
        	STATUE = "It looks real enough to pet!",
        },
        HOUNDWHISTLE = "I keep whistling but I'm not hearing anything. Must be jammed.",
        CHESSPIECE_CLAYHOUND = "Now that's a cute little statue!",
        CHESSPIECE_CLAYWARG = "Protect our humble abode with that fierce mug, shoots?",
		PIGSHRINE =
		{
            GENERIC = "Looks like work.",
            EMPTY = "Needs some meat.",
            BURNT = "Someone wasn't happy with it.",
		},
		PIG_TOKEN = "Looks like heavy duty work. Yuck.",
		PIG_COIN = "Heads I take a nap, tails I go to sleep.",
		YOTP_FOOD1 = "I'm gonna be shoving all of that into my face.",
		YOTP_FOOD2 = "I'll wait until I'm REALLY hungry for this one.",
		YOTP_FOOD3 = "Harsh. But Food's food!",

		PIGELITE1 = "Take like a chill wave and relax, guy.", --BLUE
		PIGELITE2 = "Chill out!", --RED
		PIGELITE3 = "Your sense of chill must be muddied.", --WHITE
		PIGELITE4 = "It's not cool to swing things at people, dude.", --GREEN
		PIGELITEFIGHTER1 = "Take like a chill wave and relax, guy.", --BLUE
		PIGELITEFIGHTER2 = "Chill out!", --RED
		PIGELITEFIGHTER3 = "Your sense of chill must be muddied.", --WHITE
		PIGELITEFIGHTER4 = "It's not cool to swing things at people, dude.", --GREEN

		CARRAT_GHOSTRACER = "It's made out of nightmares now too, huh.",

        YOTC_CARRAT_RACE_START = "I like to call it the finish line.",
        YOTC_CARRAT_RACE_CHECKPOINT = "I'm checkin' it out!",
        YOTC_CARRAT_RACE_FINISH =
        {
            GENERIC = "My legs need a nap and I'm not even racing.",
            BURNT = "I don't like running, but not THAT much.",
            I_WON = "Sweet victory, dude!",
            SOMEONE_ELSE_WON = "Sweet plays, {winner}!",
        },

		YOTC_CARRAT_RACE_START_ITEM = "I'm not one to start things, really.",
        YOTC_CARRAT_RACE_CHECKPOINT_ITEM = "I'll get to it right after I nap.",
		YOTC_CARRAT_RACE_FINISH_ITEM = "Well, I'm glad that it's pau.",

		YOTC_SEEDPACKET = "Sweet, some seeds!",
		YOTC_SEEDPACKET_RARE = "These ones feel extra lucky.",

		MINIBOATLANTERN = "Sweet lil' floater. Hope you're a good guide.",

        YOTC_CARRATSHRINE =
        {
            GENERIC = "Looks like work.",
            EMPTY = "Could use some rabbit food.",
            BURNT = "Someone wasn't happy with it.",
        },

        YOTC_CARRAT_GYM_DIRECTION = 
        {
            GENERIC = "I prefer letting the wind choose my direction.",
            RAT = "Hope I don't get jealous.",
            BURNT = "The carrats won't be happy about this.",
        },
        YOTC_CARRAT_GYM_SPEED = 
        {
            GENERIC = "Good thing it's not my size.",
            RAT = "Look at him, surfing the wind!",
            BURNT = "The carrats won't be happy about this.",
        },
        YOTC_CARRAT_GYM_REACTION = 
        {
            GENERIC = "My little bud could use some surfing practice.",
            RAT = "Better than my time would've been.",
            BURNT = "Someone hated work more than me.",
        },
        YOTC_CARRAT_GYM_STAMINA = 
        {
            GENERIC = "They might be strong enough to surf at this rate.",
            RAT = "Doing good, bud!",
            BURNT = "Someone hated work more than me.",
        }, 

        YOTC_CARRAT_GYM_DIRECTION_ITEM = "Work? I'll pass.",
        YOTC_CARRAT_GYM_SPEED_ITEM = "Someone should put the work into this.",
        YOTC_CARRAT_GYM_STAMINA_ITEM = "You even gotta work before the work out.",
        YOTC_CARRAT_GYM_REACTION_ITEM = "You even gotta work before the work out.",

        YOTC_CARRAT_SCALE_ITEM = "Rate those rats.",           
        YOTC_CARRAT_SCALE = 
        {
            GENERIC = "Don't worry little rat-dude, I won't judge.",
            CARRAT = "It's okay, little guy. You're good enough for me.",
            CARRAT_GOOD = "Look how strong my dude's getting!",
            BURNT = "Man, competition is no fair anymore.",
        },

		YOTB_BEEFALOSHRINE =
        {
            GENERIC = "Looks like work.",
            EMPTY = "Needs some beefalo stuff.",
            BURNT = "Someone wasn't happy with it.",
        },

        BEEFALO_GROOMER =
        {
            GENERIC = "These beefalo are gonna look so good.",
            OCCUPIED = "You're gonna look great, beef-buds.",
            BURNT = "Jealousy is so not the answer.",
        },
        BEEFALO_GROOMER_ITEM = "Ugh. Work.",

		BISHOP_CHARGE_HIT = "Ow! Rude!",
		TRUNKVEST_SUMMER = "Lets my skin breath.",
		TURF_CARPETFLOOR = "Make fists with your toes.",
		TURF_MARSH = "Goopy...",
		TURF_MUD = "Squelchy.",
		TURF_METEOR = "Moon-y...",
        TURF_PEBBLEBEACH = "Not my ideal kind of beach.",
		TURF_SINKHOLE = "I have a sinking feeling about this.",
		TURF_UNDERROCK = "Crunchy.",
		TURKEYDINNER = "I am thankful for this.",
		TWIGS = "Very useful.",
		UMBRELLA = "Keep off, elements!",
		UNAGI = "Ten more, please!",
		WAFFLES = "Brunch!",
		WALL_HAY =
		{
			BURNT = "Not surprising... it was made out of hay...",
			GENERIC = "Better than no wall...",
		},
		WALL_HAY_ITEM = "Better than no wall...",
		WALL_RUINS = "Why wall yourself off from the world?",
		WALL_RUINS_ITEM = "I could build a wall with this. But why bother?",
		WALL_STONE = "Seems strong...",
		WALL_STONE_ITEM = "Seems strong...",
		WALL_WOOD =
		{
			BURNT = "It was wood, after all...",
			GENERIC = "Knock on wood this keeps stuff out.",
		},
		WALL_WOOD_ITEM = "Knock on wood this keeps stuff out.",
		WALL_MOONROCK = "Now that's something you don't see every day.",
		WALL_MOONROCK_ITEM = "Looks spacey.",
		FENCE = "You can't fence me in, dude.",
		FENCE_ITEM = "You mean I gotta build it myself?",
		FENCE_GATE = "The way in and the way out.",
		FENCE_GATE_ITEM = "I may need a nap after building this.",
		WASPHIVE = "The worst kind of hive!",
		WATERBALLOON = "Sweet, now I can carry the sea with me.",
		WATERMELON = "Nothing better on a hot day!",
		WATERMELONHAT = "Next best thing to a watermelon bikini.",
		WATERMELONICLE = "Eat it before it melts!",
		WATERMELON_COOKED = "Hot watermelon...",
		WATERMELON_SEEDS = "I love food... but I hate farm work...",
		WEBBERSKULL = "Not your run of the mill skull.",
		WETGOOP = "Is there dry goop?",
		WHIP = "I'm not much of a whip'n'crack kinda gal.",

		ABIGAIL =
		{
            LEVEL1 =
            {
                "Hang ten, ectobuddy.",
                "Hang ten, ectobuddy.",
            },
            LEVEL2 = 
            {
                "Hang ten, ectobuddy.",
                "Hang ten, ectobuddy.",
            },
            LEVEL3 = 
            {
                "Hang ten, ectobuddy.",
                "Hang ten, ectobuddy.",
            },
		},
		BASALT = "The strong, silent type.",
		BEEMINE_MAXWELL = "Sometimes I dance to the buzzing beat.",
		BIRCHNUTDRAKE = "Chillax, little dude.",
		BISHOP_NIGHTMARE = "This is why I don't play board games!",
		BLOWDART_FIRE = "A real quick way of lighting a firepit when no one's looking.",
		BLOWDART_PIPE = "Do I have to shoot someone with it?",
		BLOWDART_POISON = "Not good for party tricks. I learned the hard way.",
		BLOWDART_SLEEP = "(Sometimes I use it for naps.)",
		BLOWDART_YELLOW = "This one is a shocker.",
		BLUEAMULET = "I like to put it on when I'm feeling a little too steamed.",
		BLUEGEM = "The color of the ocean!",
	    BLUEPRINT =
        { 
            COMMON = "It's making my brain hurt just looking at it.",
            RARE = "Looking at it is giving me a wicked migraine.",
        },
		SKETCH = "I don't get it, might wanna pass this one off to someone else.",
		BLUE_CAP = "I didn't know mushrooms came in blue.",
		BLUE_CAP_COOKED = "Did I make it better or worse?",
		BLUE_MUSHROOM =
		{
			GENERIC = "Cooked or raw, I could eat a ton of these.",
			INGROUND = "It likes to sleep during the day. Just like me!",
			PICKED = "It needs time to unwind.",
		},
		BOAT = "The second best way to travel.",
		BONESHARD = "I don't think a cast will help now.",
		BONESTEW = "It looks like a pretty hearty meal.",
		BUGNET = "Maybe I could pass an afternoon catching butterflies.",
		BUNNYMAN = "Do you give good hugs?",
		BUZZARD = "Not dead yet, friend!",

		SHADOWDIGGER = "A shadow guy that does all your work for you... I want one.",

		CACTUS =
		{
			GENERIC = "It's fine if you just leave it alone.",
			PICKED = "Ouch! I got prickled.",
		},
		CACTUS_FLOWER = "You ever seen anything as pretty?",
		CACTUS_MEAT = "Cactus flesh... d'you think that's vegetarian?",
		CACTUS_MEAT_COOKED = "Looks pretty tasty now.",
		CATCOON = "It's very independent and loves garbage. Me too!",
		CATCOONDEN =
		{
			EMPTY = "Anyone there?",
			GENERIC = "Looks like a nice place to crash.",
		},
		CATCOONHAT = "I can feel the spirits of catcoons that made it.",
		CAVE_BANANA_TREE = "That's not a real banana!",
		CAVE_ENTRANCE = "How much effort will it take to get in there?",
        CAVE_ENTRANCE_RUINS = "Smells like nightmares.",
       
       	CAVE_ENTRANCE_OPEN = 
        {
            GENERIC = "That's rude.",
            OPEN = "I've always wanted to try spelunking.",
            FULL = "I'll have to spelunk another time.",
        },
        CAVE_EXIT = 
        {
            GENERIC = "Guess it's not my time.",
            OPEN = "Time to bail!",
            FULL = "Hey! Let me up! It's dark down here!",
        },
		
		CAVE_FERN = "Nature finds a way, even here.",
		COOKEDMANDRAKE = "Might as well eat it now.",
		COONTAIL = "This would make a stylish hat.",
		CANARY =
		{
			GENERIC = "A cute little bird buddy.",
			HELD = "I hope you like my pockets as much as I do.",
		},
		CANARY_POISONED = "Harsh. Looking green there, friend.",
		
		CRITTERLAB = "What's in there giving me looks?",
        CRITTER_GLOMLING = "I just wanna squish its face into my face!",
        CRITTER_DRAGONLING = "I love it, I hope it loves me too!",
		CRITTER_LAMB = "So fluffy, and so cute! Hemolele!",
        CRITTER_PUPPY = "This little dude loves to hang ten, sleep, and eat. Me too!",
        CRITTER_KITTEN = "You give me peace, little buddy.",
		CRITTER_PERDLING = "It's so cute! Gobble Gobble!",
		CRITTER_LUNARMOTHLING = "I could lay down and watch you all night.",
		
		CROW =
		{
			GENERIC = "Akaw!",
			HELD = "Are we best friends now?",
		},
		CUTLICHEN = "Can I get something to eat now?",
		CUTREEDS = "It was hard work cutting those. Nap time!",
		CUTSTONE = "I don't have to build something now, do I?",
		DEADLYFEAST = "My gut can't handle that!",
		DECIDUOUSTREE =
		{
			BURNING = "Senseless.",
			BURNT = "Saying sorry won't bring it back.",
			CHOPPED = "Thanks for the logs. Sorry about, like, your body.",
			GENERIC = "It comes in so many colors!",
			POISON = "I said I was sorry!",
		},
		DEER = 
		{
			GENERIC = "What are YOU looking at? Nah, just kidding dude.",
			ANTLER = "Nice antler, dude.",
		},
		DEER_ANTLER = "A deer must've dropped this running away. Neat piece.",
        DEER_GEMMED = "Don't worry, I'll figure out a way to save you! ...Eventually.",
		DEERCLOPS = "Yikes! We're in the impact zone now!",
		DEERCLOPS_EYEBALL = "Aw. Kind of reminds me of the boss.",
		DEVTOOL_NODEV = "I couldn't possibly use that.",
		DIVININGRODBASE =
		{
			GENERIC = "What is that?",
			READY = "Maybe it wants a key?",
			UNLOCKED = "Let's get going!",
		},
		DIVININGRODSTART = "I can't wait to find a cool Thing!",
		DRAGONFLY = "I'd never have to put up with this on the open ocean!",
		DRAGONFLYCHEST = "What did I get?!",
		DRAGONFLYFURNACE = 
		{
			HAMMERED = "Something catch your tongue, bud?",
			GENERIC = "Give me something to eat and a blanket and my life's set.", --no gems
			NORMAL = "Wicked!", --one gem
			HIGH = "Full-on flames!", --two gems
		},
		DRAGONFRUIT = "Decadent, and decorative!",
		DRAGONFRUIT_COOKED = "It tastes pretty.",
		DRAGON_SCALES = "I need a suit of these, immediately.",
		DRUMSTICK = "Does someone want to cook it for me?",
		DRUMSTICK_COOKED = "I feel like a warrior.",
		EEL = "If you squint it's actually kind of cute.",
		EEL_COOKED = "I'm not gonna eat just an eel old thing!",
		EGGPLANT = "I've never seen an egg that shade of purple.",
		EGGPLANT_COOKED = "Is this, like, an omelette?",
		FISH = "Blub blub, little chum.",
		FLOWER_CAVE = "I want a whole wreath of them!",
		FLOWER_CAVE_DOUBLE = "I want a whole wreath of them!",
		FLOWER_CAVE_TRIPLE = "I want a whole wreath of them!",
		FRESHFRUITCREPES = "Score!", --Warly
		FROG =
		{
			DEAD = "It was too good for this world.",
			GENERIC = "Keep your tongue to yourself, bucko.",
			SLEEPING = "Sweet dreams.",
		},
		FROGGLEBUNWICH = "Not a bad afternoon snack.",
		FROGLEGS = "I don't really like touching them.",
		FROGLEGS_COOKED = "Tastes like doydoy.",
		GEMSOCKET =
		{
			GEMS = "I think it wants me to jam rocks in it.",
			VALID = "Lookin' good!",
		},
		GHOST = "You deserve a rest.",
		GLOMMER = "Looks like a friend.",
		GLOMMER = 
        {
            GENERIC = "Looks like a friend.",
            SLEEPING = "Wish I was with you, friend.",
        },
		GLOMMERFLOWER =
		{
			DEAD = "Poor lil thing.",
			GENERIC = "Wouldn't it look great in my hair?",
		},
		GLOMMERFUEL = "I'll make sure it goes to good use.",
		GLOMMERWINGS = "Sigh. Fly no more, friend.",
		GOATMILK = "It feels tingly going down.",
		GOOSE_FEATHER = "It's soft but... a bit grimy.",
		GREENAMULET = "This necklace is a creative force. I'm feelin' it.",
		GREENGEM = "The color of nature!",
		GREENSTAFF = "This staff only destroys.",
		GUANO = "It's batilisk poop.",
		HOME = "Is it time for a nap?",
		HONEYCOMB = "It's sweet and full of bee babies.",
		HONEYHAM = "Is it a special occasion?",
		HONEYNUGGETS = "I'll just be lying here, shoving these into my mouth.",
		HORN = "It's a big animal horn.",
		HOTCHILI = "Yow! Burns my tongue SO GOOD.",
		HOUNDBONE = "It's a bunch of bones. How comforting.",
		HOUNDFIRE = "Hot! Too hot!",
		HOUNDMOUND = "I think there are some angry pups lurking nearby.",
		HOUNDSTOOTH = "Highlights the importance of regular dental checkups.",
		ICE = "It knows how to chill.",
		ICEBOX = "A constant reminder to keep it cool.",
		ICECREAM = "Perfect treat to pair with a hammock in the sun.",
		ICEHAT = "Keeps my head cold. And wet.",
		ICEHOUND = "What a chilly pup.",
		KNIGHT = "He looks like he could use a nap.",
		KNIGHT_NIGHTMARE = "He should go to sleep... and not wake up.",
		KOALEFANT_SUMMER = "Hang ten, big guy.",
		KOALEFANT_WINTER = "Hang ten, big snowy guy.",
		LEIF = "Step off, grey belly!",
		LEIF_SPARSE = "Step off, grey belly!",
		LICHEN = "This moss makes me sad.",
		LIGHTBULB = "Naturally bright. Just like me!",
		LIGHTNINGGOAT =
		{
			CHARGED = "Majestic!",
			GENERIC = "Do you think it would let me snuggle it?",
		},
		LIGHTNINGGOATHORN = "I feel a little bad about the dead goat.",
		LITTLE_WALRUS = "How ya doin', tiny?",
		LIVINGTREE = "There's a board in there just begging to get out.",
		MANRABBIT_TAIL = "Fluffy!",
		MAPSCROLL = "I hope nobody's expecting me to write anything down.",
		MARBLE = "Pretty fancy material.",
		MARBLEBEAN = "There's some folklore in here somewhere.",
		MARBLEBEAN_SAPLING = "I'm confused.",
        MARBLESHRUB = "I have many questions.",
		MARBLEPILLAR = "What was it meant to hold up?",
		MARBLETREE = "Super weird.",
		MAXWELL = "He seems like kind of a jerk.",
		MAXWELLHEAD = "Woah, did I eat some bad berries?",
		MAXWELLLIGHT = "Weird.",
		MAXWELLLOCK = "Gives me the heebie jeebies.",
		MAXWELLPHONOGRAPH = "Jaunty, but grating.",
		MAXWELLTHRONE = "Woah. That's rough.",
		MINOTAUR = "Holy horns.",
		MINOTAURCHEST = "Whatcha got for me, magic chest?",
		MINOTAURHORN = "It was an ordeal to get this.",
		MOLE =
		{
			ABOVEGROUND = "Welcome to the surface!",
			HELD = "Are you comfy in there, buddy?",
			UNDERGROUND = "Who's down there?",
		},
		MOLEHAT = "I CAN SEE ALL!",
		MOLEHILL = "Nice hole, mole.",
		MONKEY = "You think he can surf?",
		MONKEYBARREL = "Neat abode, guys.",
		MONSTERTARTARE = "I'm not too keen on putting that in my mouth.", --Warly
		MOOSE = "You're a big one, huh!",
		MOOSEEGG = "It will one day hatch into a moo... a goo... a thing.",
		MOSQUITO =
		{
			GENERIC = "Filthy bloodsucker!",
			HELD = "Don't think this means we're friends.",
		},
		MOSQUITOSACK = "Makes an okay pillow if you plug your nose.",
		MOSSLING = "Don't you just want to pick it up and squeeze it?",
		MOUND =
		{
			DUG = "Thanks for the loot, dude.",
			GENERIC = "Seems rude to mess with it.",
		},
		NIGHTMARELIGHT = "I don't care how weird it is, as long as it keeps darkness away.",
		NIGHTMARE_TIMEPIECE =
		{
			CALM = "I can breathe easy.",
			DAWN = "It's almost over...!",
			NOMAGIC = "I'm not getting anything.",
			STEADY = "I think it's keeping consistent.",
			WANING = "It's leaving...",
			WARN = "Woah! Things are really heating up!",
			WAXING = "It's gaining strength!",
		},
		NIGHTSTICK = "That's one gnarly-lookin' whacker.",
		ORANGEAMULET = "Finally, jewelry that gets me.",
		ORANGEGEM = "The world looks fractured when I peer through it!",
		ORANGESTAFF = "Ah, this staff was made for me!",
		OPALSTAFF = "That's one fancy wand.",
        OPALPRECIOUSGEM = "What's so special about this one?",
		PANDORASCHEST = "Maybe I'll get something nice?",
		PENGUIN = "Hey little dude.",
		PERD = "He's a doofy little guy.",
		PIGGUARD = "Take it easy, big guy.",
		PIGGYBACK = "Was it worth it?",
		PIGHEAD =
		{
			BURNT = "Wasn't it bad enough already?",
			GENERIC = "Tough break, dude.",
		},
		PIGHOUSE =
		{
			BURNT = "Hope they had insurance.",
			FULL = "How do they all fit in there?",
			GENERIC = "Where the pigs kick back and relax.",
			LIGHTSOUT = "Turn it back on! PLEASE!",
		},
		PIGKING = "Haha. Am I supposed to curtsy or something?",
		PIGMAN =
		{
			DEAD = "Bad luck, dude.",
			FOLLOWER = "Let's hit the waves!",
			GENERIC = "Lookin' fine, swine.",
			GUARD = "Wow, you're really loyal. That's admirable.",
			WEREPIG = "Haha! What even is that?",
		},
		PIGTENT = "Doesn't smell too great.",
		PIGTORCH = "It's kind of cute.",
		PIKE_SKULL = "Well that's not very nice.",
		PINECONE = "It's a tree baby.",
        PINECONE_SAPLING = "Grow on, little bud.",
		LUMPY_SAPLING = "I believe in you, little bud.",
		POND = "It's kind of hard to look at myself these days.",
		POND_ALGAE = "It makes me miss the open sea.",
		GIFT = "Oh! You shouldn't have!",
        GIFTWRAP = "Hm, how do I fancy up this bow?",
		POTTEDFERN = "It can no longer grow free.",
		SUCCULENT_POTTED = "Wildlife to a mild life.",
		SUCCULENT_PLANT = "Aloe-ha!",
		SUCCULENT_PICKED = "Taken from its home... I know the feeling.",
	    SENTRYWARD = "I don't like the feeling of always being watched.",
		TOWNPORTAL =
        {
			GENERIC = "Oh man, I can get used to this.",
			ACTIVE = "Take me wherever.",
		},
        TOWNPORTALTALISMAN = 
        {
			GENERIC = "A do-the-walking-for-me machine.",
			ACTIVE = "Ready? Do my walking for me!",
		},
        WETPAPER = "Mush.",
        WETPOUCH = "Surf sewage.",
		MOONROCK_PIECES = "Weird junk bits.",
		MOONBASE =
        {
            GENERIC = "Don't look at me, I've never seen this ritual before.",
            BROKEN = "If anybody asks, it was broken before I got here, shoots?",
            STAFFED = "Sweet staff. What now?",
            WRONGSTAFF = "Whatever it is, it doesn't want that.",
			MOONSTAFF = "Now it's glowing, radical.",
        },
		MOONDIAL = 
        {
			GENERIC = "That thing watches the moon, I guess.",
			NIGHT_NEW = "We got a fresh moon tonight.",
			NIGHT_WAX = "The moon's waxing, I hate waxing.",
			NIGHT_FULL = "A bright shiny full moon for tonight.",
			NIGHT_WANE = "The moon's waning tonight.",
			CAVE = "I can't see anything when I look up, so no moon.",
			GLASSED = "I try not to look too closely.",
        },
		PUMPKIN = "You look nice today, pumpkin.",
		PUMPKINCOOKIE = "Score!",
		PUMPKIN_COOKED = "Smells autumn-y.",
		PUMPKIN_LANTERN = "Spooky!",
		PUMPKIN_SEEDS = "They're fine like this. No need to do any farming.",
		RABBIT =
		{
			GENERIC = "Look at its little ears!",
			HELD = "No need to be scared.",
		},
		RABBITHOLE =
		{
			GENERIC = "It's rabbit-shaped.",
			SPRING = "I guess they needed Me Time.",
		},
		RABBITHOUSE =
		{
			BURNT = "Aw. The bunnies lost their home.",
			GENERIC = "A decent enough place to live.",
		},
		RAINCOAT = "The surf can't touch me now!",
		RAINHAT = "High and dry.",
		RED_CAP = "It might be a bad idea to just eat stuff I find on the ground.",
		RED_MUSHROOM =
		{
			GENERIC = "Now that's a mushroom!",
			INGROUND = "Hiding, little dude?",
			PICKED = "It might grow back on its own.",
		},
		RELIC =
		{
			BROKEN = "I'm not cleaning that up.",
			GENERIC = "Where's the ancient bed?",
		},
		RESEARCHLAB4 =
		{
			BURNT = "Guess I don't get to pull the lever anymore.",
			GENERIC = "That lever's calling to me.",
		},
		ROBIN =
		{
			GENERIC = "A song given wing.",
			HELD = "How are you doing, little guy?",
		},
		ROBIN_WINTER =
		{
			GENERIC = "You have a lovely voice!",
			HELD = "You're not too squished, are you?",
		},
		ROBOT_PUPPET = "Sorry about your existence.",
		ROCKY = "No need to fight!",
		PETRIFIED_TREE = "That tree's frozen solid.",
		ROCK_PETRIFIED_TREE = "That tree's frozen solid.",
		ROCK_PETRIFIED_TREE_OLD = "That tree's been frozen solid. Harsh.",
		ROCK_ICE =
		{
			GENERIC = "At least someone around here knows how to chill!",
			MELTED = "I didn't bring my puddle stompin' boots!",
		},
		ROCK_ICE_MELTED = "I didn't bring my puddle stompin' boots!",
		ROOK = "Check, mate!",
		ROOK_NIGHTMARE = "Is that thing real?!",
		RUBBLE = "A big ol' pile of rocks.",
		RUINS_RUBBLE = "A big ol' pile of old, old rocks.",
		SANITYROCK =
		{
			ACTIVE = "Go off!",
			INACTIVE = "See ya.",
		},
		SLURPER = "It's a little gross, but who am I to judge?",
		SLURPERPELT = "Maybe it doubles as a blanket.",
		SLURPER_PELT = "Maybe it doubles as a blanket.",
		SLURTLE = "It tastes with its feet. Gross.",
		SLURTLEHAT = "I'm a little embarrassed to wear it in public.",
		SLURTLEHOLE = "The snail things come out of there.",
		SLURTLESLIME = "It's hard to hold on to.",
		SLURTLE_SHELLPIECES = "Holding on to these doesn't even seem worth it.",
		SNURTLE = "It tastes with its feet. Gross.",
		SPIDERHOLE = "It's a hole. Full of SPIDERS.",
		SPIDERHOLE_ROCK = "It's a hole. Full of SPIDERS.",
		SPIDER_DROPPER = "How rude of you to drop in!",
		SPIDER_HIDER = "I see you over there!",
		SPIDER_SPITTER = "No thank you!",
		STAFFLIGHT = "Isn't starlight bad for the skin?",
		STAFFCOLDLIGHT = "Now this is my kind of magic. Chill.",
		STAFF_TORNADO = "I'm glad I'm not on the receiving end.",
		STALAGMITE = "A pretty cool rock.",
		STALAGMITE_TALL = "This rock seems uncool.",
		LAVA_POND_ROCK = "A pretty hot rock.",
		
		STATUEGLOMMER =
		{
			EMPTY = "All messed up now.", --Swapped
			GENERIC = "Woah, far out.",
		},
	    STAGEHAND =
        {
			AWAKE = "YIKES! ...I gotta hand it to you, you're a freaky one.",
			HIDING = "Yeah, it's pretty. But what's the point?",
        },
		STATUE_MARBLE = 
        {
        	GENERIC = "That's a pretty fancy statue.",
        	TYPE1 = "Is there some deeper meaning to this that I'm not getting?",
        	TYPE2 = "That's pretty dark.",
        	TYPE3 = "You could fill that with water or something.",
    	},
		STATUEHARP = "No shoes, no... head... no service!",
		STATUEMAXWELL = "Guy's got a lot of stuff we can make fun of him about.",
		STEELWOOL = "It's too hard to make a sweater out of.",
		SWEATERVEST = "I hope none of my surfer buds see me in this.",
		TELEBASE =
		{
			GEMS = "I think it likes purple gems.",
			VALID = "Good to go!",
		},
		TELEPORTATO_BASE =
		{
			ACTIVE = "Do I really want to go through that?",
			GENERIC = "Get your weird face out of here, dude!",
			LOCKED = "It still won't let me through.",
			PARTIAL = "It'd take a lot of effort to put this back together.",
		},
		TELEPORTATO_BOX = "This looks important.",
		TELEPORTATO_CRANK = "You wanna come with me, huh?",
		TELEPORTATO_POTATO = "Heh heh.",
		TELEPORTATO_RING = "I think I should hold on to it.",
		TELESTAFF = "It shows me things I never knew I could see.",
		TENTACLE = "Hands off, bucko!",
		TENTACLESPIKE = "It's pretty flimsy. I can't see it doing much damage.",
		TENTACLESPOTS = "These might help me out of a tight spot.",
		TENTACLE_GARDEN = "This has no right to exist.",
		TENTACLE_PILLAR = "It's all slimed up.",
		TENTACLE_PILLAR_ARM = "Much too slimy!",
		THULECITE = "It's otherworldly.",
		THULECITE_PIECES = "Weird.",
		SACRED_CHEST = 
		{
			GENERIC = "Not a lotta curse left in this chest.",
			LOCKED = "It's thinking about some stuff. Needs to get it off its chest.",
		},
		TREECLUMP = "I didn't want to get through here anyway.",
		TRUNKVEST_WINTER = "I could fall asleep in this.",
		TRUNK_COOKED = "Seems edible, as far as noses go.",
		TRUNK_SUMMER = "It's a meaty-looking trunk.",
		TRUNK_WINTER = "It's a cold, meaty-looking trunk.",
		TUMBLEWEED = "It's on a journey called \"life\".",
		TURF_SANDY = "It's getting in my shoes!",
		TURF_BADLANDS = "Keeps me grounded.",
		TURF_CAVE = "It's ground.",
		TURF_CHECKERFLOOR = "It's ground.",
		TURF_DECIDUOUS = "Keeps me grounded.",
		TURF_DESERTDIRT = "Keeps me grounded.",
		TURF_DIRT = "Dirt.",
		TURF_FOREST = "Keeps me grounded.",
		TURF_FUNGUS = "Keeps me grounded.",
		TURF_FUNGUS_GREEN = "Keeps me grounded.",
		TURF_FUNGUS_RED = "Keeps me grounded.",
		TURF_GRASS = "Grassy.",
		TURF_MAGMAFIELD = "Keeps me grounded.",
		TURF_ROAD = "Keeps me grounded.",
		TURF_ROCKY = "Rocky.",
		TURF_SAVANNA = "Keeps me grounded.",
		TURF_SNAKESKINFLOOR = "Keeps me grounded.",
		TURF_SWAMP = "Swampy.",
		TURF_TIDALMARSH = "Marshy.",
		TURF_WOODFLOOR = "Hardwood. Fancy.",
		TURF_DRAGONFLY = "I could walk barefoot on these babies for miles!",
		TURF_SHELLBEACH = "Ugh, I can't get homesick at this point.",
		TURF_MONKEY_GROUND = "Keeps me grounded.",
		UNIMPLEMENTED = "If it was my job, it'd never get finished.",
		WALRUS = "Hey grey belly! How's life treatin' ya?",
		WALRUSHAT = "This plaid ain't bad.",
		WALRUS_CAMP =
		{
			EMPTY = "No one home.",
			GENERIC = "Sweet digs.",
		},
		WALRUS_TUSK = "That's a pretty impressive tooth.",
		WARDROBE = 
		{
			GENERIC = "Changing clothes is too much effort.",
            BURNING = "That's not cool.",
			BURNT = "It's not much use to anyone now.",
		},
		WARG = "What's your deal, man?",
		WARGLET = "Those bites mean business.",
		WINTERHAT = "It's like a hug for my head.",
		WINTEROMETER =
		{
			BURNT = "Not much use to anyone now.",
			GENERIC = "Why put numbers on what you can feel?",
		},
		
		WINTER_TREE =
        {
			BURNT = "That's a huge bummer. I hope some apologies are due.",
			BURNING = "That's not cool.",
			CANDECORATE = "Yo! Merry Winter's Feast!",
			YOUNG = "That little guy's looking great.",
        },
        WINTER_TREESTAND = "It could really use a pine cone.",
        WINTER_ORNAMENT = "This would probably float well.",
        WINTER_ORNAMENTLIGHT = "Brightens up my day!",
		WINTER_ORNAMENTBOSS = "That's one fancy ornament.",
		WINTER_ORNAMENTFORGE = "Kinda smells like bacon.",
		WINTER_ORNAMENTGORGE = "That's a lot of work for an ornament.",
        
        WINTER_FOOD1 = "These little guys make winter not so bad!", --gingerbread cookie
        WINTER_FOOD2 = "Oh man, I would'a been all over these during my small kid time!", --sugar cookie
        WINTER_FOOD3 = "I prefer the more fruity kind.", --candy cane
        WINTER_FOOD4 = "Yuck, this thing sinks my board.", --fruitcake
		WINTER_FOOD5 = "I never tried this tradition before. Not missing my chance now!", --yule log cake
        WINTER_FOOD6 = "Who wants to see me eat this puddy-thing in a single bite?", --plum pudding
        WINTER_FOOD7 = "I'd love to sip this by the sea.", --apple cider
        WINTER_FOOD8 = "Kokoleka wela? Out here? Don't mind if I do!", --hot cocoa
        WINTER_FOOD9 = "Sweet nog, dude!", --eggnog

		WINTERSFEASTOVEN =
		{
			GENERIC = "Someone should cook something in that.",
			COOKING = "That sweet smell of grinds is driving me lolo!",
			ALMOST_DONE_COOKING = "My mouth's got high tide right now!",
			DISH_READY = "Lu'au!",
		},
		BERRYSAUCE = "I love it berry much!",
		BIBINGKA = "Squishy and soft. Like me!",
		CABBAGEROLLS = "I'm ready to rock and roll with these cabbages.",
		FESTIVEFISH = "Like bringing the holidays to the beach.",
		GRAVY = "I could drown in this sick grav-wave!",
		LATKES = "My wonderful spud buds.",
		LUTEFISK = "I'm always open to new food adventures!",
		MULLEDDRINK = "I could swim in an ocean of this stuff!",
		PANETTONE = "This stuff's all looking way tastier than usual.",
		PAVLOVA = "It's cake! I'm hungry! We were destined to meet.",
		PICKLEDHERRING = "Righteous! Tastes like the ocean floor.",
		POLISHCOOKIE = "Someone bring me a boat load of these.",
		PUMPKINPIE = "I could eat a choke ton by myself if I wanted, and I'm wanting!",
		ROASTTURKEY = "I can hear it calling out to me. \"Eat me, Walani, eat me!!\"",
		STUFFING = "I'll be stuffing my mouth full of this stuff!",
		SWEETPOTATO = "‘Onolicious!",
		TAMALES = "It's like a warm, husky hug.",
		TOURTIERE = "Meat pie, meet stomach.",
		TABLE_WINTERS_FEAST = 
		{
			GENERIC = "Sweet. A table!",
			HAS_FOOD = "‘Ono grinds! I am gonna EAT that!",
			WRONG_TYPE = "I'm a fan, but it's not really \"in\".",
			BURNT = "Not cool, dude.",
		},
		GINGERBREADWARG = "Guess it's our time to be eaten?", 
		GINGERBREADHOUSE = "Edible boards! I must be dreaming.", 
		GINGERBREADPIG = "Hey, come back little guy!",
		CRUMBS = "So it's fine for them to leave crumbs but not me?",
		WINTERSFEASTFUEL = "The heart of the holiday.",

        KLAUS = "Times like these make me really miss the open waters.",
        KLAUS_SACK = "I hope something good's inside!",
		KLAUSSACKKEY = "Fancy antler, key. What's it open?",
		WORM =
		{
			DIRT = "Something's taking a dirt nap.",
			PLANT = "Nothing to see here.",
			WORM = "That dude can pull off some sweet aerials.",
		},
		WORMHOLE =
		{
			GENERIC = "Is that its front end, or back?",
			OPEN = "Well, I see no reason not to jump in.",
		},
		WORMHOLE_LIMITED = "I don't think it's feeling well.",
		WORMLIGHT = "I find myself strangely attracted to it.",
		WORMLIGHT_LESSER = "Looks like it's been out swimming for a bit too long.",
		WORMLIGHT_PLANT = "Looks good to me.",
		YELLOWAMULET = "People tell me I glow when I wear it!",
		YELLOWGEM = "It's got a nice energy.",
		YELLOWSTAFF = "Isn't starlight bad for the skin?",
		
		REVIVER = "This'll make sure your root chakra doesn't take five.",
		SHADOWHEART = "Looks like someone's chakras were all outta wack.",
		ATRIUM_RUBBLE = 
        {
			LINE_1 = "This weird drawing's throwing off my chill vibes.",
			LINE_2 = "Looks like just a rock to me.",
			LINE_3 = "That's one wicked wave crashing over those people.",
			LINE_4 = "I might have nightmares about this one.",
			LINE_5 = "Looks like a sick place to have a party.",
		},
        ATRIUM_STATUE = "You could use a major cosmic cleansing.",
        ATRIUM_LIGHT = 
        {
			ON = "This light's really throwing off my chill vibe.",
			OFF = "Looks like busywork to me.",
		},
        ATRIUM_GATE =
        {
			ON = "It's on, but the vibes are way off!",
			OFF = "I wonder what this does...",
			CHARGING = "It's doing some creepy charging stuff.",
			DESTABILIZING = "It's blowing its top!",
			COOLDOWN = "It needs some time to chill.",
        },
        ATRIUM_KEY = "I wouldn't trust me with a key that important.",
		LIFEINJECTOR = "If you crash like a sick wave, you're gonna need this.",
		SKELETON_PLAYER =
		{
			MALE = "%s took a gnarly wipeout from %s.",
			FEMALE = "%s took a gnarly wipeout from %s.",
			ROBOT = "%s took a gnarly wipeout from %s.",
			DEFAULT = "%s's life-board was broken by %s.",
		},
		-- Custom
		HUMANMEAT = "Am I really hungry enough?",
		HUMANMEAT_COOKED = "It still looks awful.",
		HUMANMEAT_DRIED = "Does drying make it not human? Am I still human?",
		
		MOONROCKNUGGET = "I doubt it tastes good.",
		ROCK_MOON = "Moon rock. Sweet!",
		MOONROCKCRATER = "Something needs to be jammed in there.",
        REDMOONEYE = "It's on lifeguard duty.",
        PURPLEMOONEYE = "I'll get lost in the woods a lot less now.",
        GREENMOONEYE = "It's like marking a treasure map! Without the treasure.",
        ORANGEMOONEYE = "Now they won't have to send search parties after me.",
        YELLOWMOONEYE = "Color-coded so you don't trip over it while walking, like me.",
        BLUEMOONEYE = "I feel like it's always watching me...",
		--Arena Event
        LAVAARENA_BOARLORD = "You don't seem so kingly to me, guy.",
        BOARRIOR = "We'll knock you down a size! Right, guys! ...Right?",
        BOARON = "Don't make me have to raise my hand, little guy!",
        PEGHOOK = "Gag! He's got a spicy spitter!",
        TRAILS = "Catch a break, dude!",
        TURTILLUS = "That thing needs to relax!",
        SNAPPER = "Watch out for this bud's bodacious bite!",  
		RHINODRILL = "Hey, bro! Could you chill the drill?",
		BEETLETAUR = "Aw, this guy's just a big baby!",		
        LAVAARENA_PORTAL = 
        {
            ON = "I guess I'll just get going or something. Later!",
            GENERIC = "I can't see any sea in there. Is it too late to bail?",
        },
		LAVAARENA_KEYHOLE = "Maybe it needs a key?",
		LAVAARENA_KEYHOLE_FULL = "Let's get going already!",
		LAVAARENA_BATTLESTANDARD = "Get rid of that flag thing!",
        LAVAARENA_SPAWNER = "I'll take a mental note to forever avoid that.",
		HEALINGSTAFF = "It gives off the goodest of good vibes.",
        FIREBALLSTAFF = "This'll show 'em who's queen boss now!",
        HAMMER_MJOLNIR = "That's for the less fun alternative to breaking things.",
        SPEAR_GUNGNIR = "That's a bit aggressive.",
        BLOWDART_LAVA = "Now this I can use, all I have to do is breathe!",
        BLOWDART_LAVA2 = "Ugh, breathing's never been this hard.",
        LAVAARENA_LUCY = "I don't think I wanna be throwing that.",
        WEBBER_SPIDER_MINION = "Aww, kinda cute. But gross.",
        BOOK_FOSSIL = "Keep that work to the readers.",
		LAVAARENA_BERNIE = "Hang in there, lil' bear.",
		SPEAR_LANCE = "Looks a bit brutal.",
		BOOK_ELEMENTAL = "This book's way out of my league.",
		LAVAARENA_ELEMENTAL = "Feels kinda wrong, the whole no free-will part.",
		
		LAVAARENA_ARMORHEAVY = "I'd rather skip these stones.",
		LAVAARENA_TIARAFLOWERPETALSHAT = "This piece really gets me.",
		LAVAARENA_HEALINGGARLANDHAT = "That has to be the healthiest headpiece I've seen.",
		LAVAARENA_FEATHERCROWNHAT = "I don't like running, no matter how fast.",
        LAVAARENA_HEALINGFLOWERHAT = "Aw! Cute!",
		LAVAARENA_RECHARGERHAT = "See, I told you about the power of crystals!",
		LAVAARENA_EYECIRCLETHAT = "I can dig the style.",
		LAVAARENA_ARMORLIGHT = "This thing's itchy.",
		LAVAARENA_ARMORLIGHTSPEED = "Scratchy, but fashionable.",
		LAVAARENA_ARMORMEDIUM = "Probably not good for swimming in.",
		LAVAARENA_ARMORMEDIUMDAMAGER = "It makes me punch harder, but doesn't give me the energy to.",
		LAVAARENA_ARMORMEDIUMRECHARGER = "I kinda expected it to be breezier.",
		LAVAARENA_ARMOREXTRAHEAVY = "This is like, way too sturdy.",
        LAVAARENA_LIGHTDAMAGERHAT = "Leaves a scratch. On the enemy guys.",
        LAVAARENA_STRONGDAMAGERHAT = "It could make me punch harder, but I'd rather not.",
        LAVAARENA_CROWNDAMAGERHAT = "I'm not really the 'destroyer of worlds' kinda gal.",
		LAVAARENA_ARMOR_HP = "Anything to help from taking a gnarly wipeout.",
		LAVAARENA_FIREBOMB = "I feel powerful holding this.",
		LAVAARENA_HEAVYBLADE = "I need to take a nap just looking at that.",
		--Quagmire
        QUAGMIRE_ALTAR = 
        {
        	GENERIC = "Anybody here good at preparing some serious kau kau?",
        	FULL = "I can barely digest anything going on. Let's hope you feel different.",
    	},
		QUAGMIRE_ALTAR_STATUE1 = "Pretty old, guy.",
		QUAGMIRE_PARK_FOUNTAIN = "You'd be a lot happier with some water flowing through you.",
		
        QUAGMIRE_HOE = "Ugh. Farming.",
        
        QUAGMIRE_TURNIP = "A good turnip never turndowns.",
        QUAGMIRE_TURNIP_COOKED = "Looks like our luck is about to turnip!",
        QUAGMIRE_TURNIP_SEEDS = "Ugh, work.",
        
        QUAGMIRE_GARLIC = "There's nothing that this stuff can't make taste good!",
        QUAGMIRE_GARLIC_COOKED = "I can hear it sizzling.",
        QUAGMIRE_GARLIC_SEEDS = "They don't look like garlic. Do we gotta do the work?",
        
        QUAGMIRE_ONION = "Layered with love.",
        QUAGMIRE_ONION_COOKED = "Looks a little tastier.",
        QUAGMIRE_ONION_SEEDS = "I could grow something with these.",
        
        QUAGMIRE_POTATO = "I love eating these things!",
        QUAGMIRE_POTATO_COOKED = "Mmmm...",
        QUAGMIRE_POTATO_SEEDS = "Ugh, I guess I should plant these?",
        
        QUAGMIRE_TOMATO = "Maybe if I smush 'em I'll get some paste for my fries.",
        QUAGMIRE_TOMATO_COOKED = "Yummy!",
        QUAGMIRE_TOMATO_SEEDS = "You mean I gotta plant these myself?",
        
        QUAGMIRE_FLOUR = "Like an ocean of... flour.",
        QUAGMIRE_WHEAT = "PLACEHOLDER",
        QUAGMIRE_WHEAT_SEEDS = "PLACEHOLDER",
        --NOTE: raw/cooked carrot uses regular carrot strings
        QUAGMIRE_CARROT_SEEDS = "Those are a lotta work.",
        
        QUAGMIRE_ROTTEN_CROP = "Yuck.",
        
		QUAGMIRE_SALMON = "Such tasty colors!",
		QUAGMIRE_SALMON_COOKED = "Sorry, man. It was you or me.",
		QUAGMIRE_CRABMEAT = "Sorry. we still gotta cook.",
		QUAGMIRE_CRABMEAT_COOKED = "Delicious, delicious guilt.",
		QUAGMIRE_SUGARWOODTREE = 
		{
			GENERIC = "Stay sweet, tree.",
			STUMP = "Guess we needed it.",
			TAPPED_EMPTY = "We could use some sap, tree bud.",
			TAPPED_READY = "Mmm!",
			TAPPED_BUGS = "Gag!",
			WOUNDED = "Illin'.",
		},
		QUAGMIRE_SPOTSPICE_SHRUB = 
		{
			GENERIC = "Freaky fauna.",
			PICKED = "I think we got it.",
		},
		QUAGMIRE_SPOTSPICE_SPRIG = "Smells organic.",
		QUAGMIRE_SPOTSPICE_GROUND = "Take a whiff of THIS!",
		QUAGMIRE_SAPBUCKET = "It's fun to fill.",
		QUAGMIRE_SAP = "I wanna chug it.",
		QUAGMIRE_SALT_RACK =
		{
			READY = "Sweet saltiness!",
			GENERIC = "Everything takes time.",
		},
		
		QUAGMIRE_POND_SALT = "I couldn't swim in it, that's for sure.",
		QUAGMIRE_SALT_RACK_ITEM = "Someone should build it.",

		QUAGMIRE_SAFE = 
		{
			GENERIC = "Keeps things safe and sound.",
			LOCKED = "You won't be safe from me forever!",
		},

		QUAGMIRE_KEY = "I think we gotta keep it.",
		QUAGMIRE_KEY_PARK = "Park it up!",
        QUAGMIRE_PORTAL_KEY = "Get me outta here!",
		
		QUAGMIRE_MUSHROOMSTUMP =
		{
			GENERIC = "Funky fungus, guy!",
			PICKED = "I'm stumped...",
		},
		QUAGMIRE_MUSHROOMS = "It's fun to have around.",
        QUAGMIRE_MEALINGSTONE = "You mean I gotta WORK too? Ugh!",
		QUAGMIRE_PEBBLECRAB = "Awe! Look at its little abode!",

		QUAGMIRE_RUBBLE_CARRIAGE = "Totally blown out.",
        QUAGMIRE_RUBBLE_CLOCK = "Time doesn't always tick forward, I guess.",
        QUAGMIRE_RUBBLE_CATHEDRAL = "Bits and more bits.",
        QUAGMIRE_RUBBLE_PUBDOOR = "I won't walk through it.",
        QUAGMIRE_RUBBLE_ROOF = "Bummer.",
        QUAGMIRE_RUBBLE_CLOCKTOWER = "Junk.",
        QUAGMIRE_RUBBLE_BIKE = "All junked up.",
        QUAGMIRE_RUBBLE_HOUSE =
        {
            "Totally blown out.",
            "Totally blown down.",
            "Totally blown over.",
        },
        QUAGMIRE_RUBBLE_CHIMNEY = "Should I be walking around here, or is it kapu?",
        QUAGMIRE_RUBBLE_CHIMNEY2 = "Yeesh. This place is dire.",
        QUAGMIRE_MERMHOUSE = "Heh. Kinda charming.",
        QUAGMIRE_SWAMPIG_HOUSE = "I hope mine doesn't look like that if I get back...",
        QUAGMIRE_SWAMPIG_HOUSE_RUBBLE = "Is this what happens when you don't upkeep?",
        QUAGMIRE_SWAMPIGELDER =
        {
            GENERIC = "Hey there, king guy?",
            SLEEPING = "He deserves a nap.",
        },
        QUAGMIRE_SWAMPIG = "What's up? Actually, don't answer that.",
        
        QUAGMIRE_PORTAL = "I'm ready to hele on out of this place.",
        QUAGMIRE_SALTROCK = "You could just lick it.",
        QUAGMIRE_SALT = "Sweet and salty! Well, just salty.",
        --food--
        QUAGMIRE_FOOD_BURNT = "That's harsh.",
        QUAGMIRE_FOOD =
        {
        	GENERIC = "Good enough to eat!",
            MISMATCH = "Yeah, no.",
            MATCH = "Good enough to eat! ...for that gnaw.",
            MATCH_BUT_SNACK = "It's a little light on the carbs.",
        },
        
        QUAGMIRE_FERN = "Nature finds a way, even here.",
        QUAGMIRE_FOLIAGE_COOKED = "I guess you can do that too.",
        QUAGMIRE_COIN1 = "Sweet! Mahalo!",
        QUAGMIRE_COIN2 = "Shiny!",
        QUAGMIRE_COIN3 = "Money can't buy happiness. But I guess it can buy freedom.",
        QUAGMIRE_COIN4 = "Show me the way, coin!",
        QUAGMIRE_GOATMILK = "Eh, bottom's up!",
        QUAGMIRE_SYRUP = "Chug it!",
        QUAGMIRE_SAP_SPOILED = "That harshes my mellow. AND my sap.",
        QUAGMIRE_SEEDPACKET = "Cool. What's in it?",
        
        QUAGMIRE_POT = "We can fill it up a lil' more.",
        QUAGMIRE_POT_SMALL = "Kinda small.",
        QUAGMIRE_POT_SYRUP = "Needs a little sweetness.",
        QUAGMIRE_POT_HANGER = "I try not to get hung up on things.",
        QUAGMIRE_POT_HANGER_ITEM = "I try not to get hung up on things.",
        QUAGMIRE_GRILL = "While it's grillin', I'll be chillin'.",
        QUAGMIRE_GRILL_ITEM = "Pretty chill grill.",
        QUAGMIRE_GRILL_SMALL = "While it's grillin', I'll be chillin'.",
        QUAGMIRE_GRILL_SMALL_ITEM = "Pretty lil' chill grill.",
        QUAGMIRE_OVEN = "Someone should put something in there.",
        QUAGMIRE_OVEN_ITEM = "We gotta build 'em too?",
        QUAGMIRE_CASSEROLEDISH = "I can roll with this!",
        QUAGMIRE_CASSEROLEDISH_SMALL = "Rock 'n' Casserole!",
        QUAGMIRE_PLATE_SILVER = "You'd never find me on it.",
        QUAGMIRE_BOWL_SILVER = "Pretty fancy.",
        
        QUAGMIRE_MERM_CART1 = "Any more good stuff, bud?", --sammy's wagon
        QUAGMIRE_MERM_CART2 = "Got anything good in there?", --pipton's cart
        QUAGMIRE_PARK_ANGEL = "Killer wings, dude.",
        QUAGMIRE_PARK_ANGEL2 = "Sick wings, guy.",
        QUAGMIRE_PARK_URN = "Are they still in there?",
        QUAGMIRE_PARK_OBELISK = "It sure does exist, huh.",
        QUAGMIRE_PARK_GATE =
        {
            GENERIC = "Hey, it's open!",
            LOCKED = "Let me iiiinn!",
        },
        QUAGMIRE_PARKSPIKE = "Spiky.",
        QUAGMIRE_CRABTRAP = "The little guys go in there.",
        QUAGMIRE_TRADER_MERM = "Lookin' fishy there, guy.",
        QUAGMIRE_TRADER_MERM2 = "Lookin' fishy there, guy.",
        
        QUAGMIRE_GOATMUM = "Auntie, you could use a hug, and a chill pill.",
        QUAGMIRE_GOATKID = "Need a hug, little guy?",
        QUAGMIRE_PIGEON =
        {
            DEAD = "Yeesh...",
            GENERIC = "They're sort of cute.",
            SLEEPING = "Wish I was doing just that, buddy.",
        },
        QUAGMIRE_LAMP_POST = "Pretty sure that's a lamp.",

        QUAGMIRE_BEEFALO = "You're looking rough, buddy.",
        QUAGMIRE_SLAUGHTERTOOL = "I don't gotta, right?",

        QUAGMIRE_SAPLING = "All done too, huh?",
        QUAGMIRE_BERRYBUSH = "Huh, it's not growing back.",

        QUAGMIRE_ALTAR_STATUE2 = "Something catch your eyes, dude?",
        QUAGMIRE_ALTAR_QUEEN = "How ya doin', queeny?",
        QUAGMIRE_ALTAR_BOLLARD = "I like doing nothing too.",
        QUAGMIRE_ALTAR_IVY = "This place is choke full of this stuff.",

        QUAGMIRE_LAMP_SHORT = "Hey there, little lamp!",

		--v2 Winona
        WINONA_CATAPULT = 
        {
        	GENERIC = "Think I could take it for a ride?",
        	OFF = "It's taking a fuel nap.",
        	BURNING = "Who did that?",
        	BURNT = "Wiped out.",
        },
        WINONA_SPOTLIGHT = 
        {
        	GENERIC = "Think you could spot me in the open ocean with this?",
        	OFF = "It's taking a fuel nap.",
        	BURNING = "Who did that?",
        	BURNT = "Wiped out.",
        },
        WINONA_BATTERY_LOW = 
        {
        	GENERIC = "Like an ocean of bolts.",
        	LOWPOWER = "Ughhh, someone should refill it.",
        	OFF = "I'll let it rest.",
        	BURNING = "Who did that?",
        	BURNT = "Wiped out.",
        },
        WINONA_BATTERY_HIGH = 
        {
        	GENERIC = "Well, if it does work for me I'm happy.",
        	LOWPOWER = "Getting kinda weepy.",
        	OFF = "I'll let it rest.",
        	BURNING = "Who did that?",
        	BURNT = "Wiped out.",
        },

		COMPOSTWRAP = "Plants gotta feel good too.",
        ARMOR_BRAMBLE = "How will I hug trees in this?",
        TRAP_BRAMBLE = "Don't wanna step on that.",
		-- Walter
        WOBYBIG = 
        {
            "Woah there, big gal!",
            "Woah there, big gal!",
        },
        WOBYSMALL = 
        {
            "I could just squish my face into yours all day!",
            "I could just squish my face into yours all day!",
        },
		WALTERHAT = "I'm less forest-type and more beach-type.",
		SLINGSHOT = "Like a little hammock for rocks.",
		SLINGSHOTAMMO_ROCK = "At least it's not from the sky.",		
		SLINGSHOTAMMO_MARBLE = "At least it's not from the sky.",		
		SLINGSHOTAMMO_THULECITE = "At least it's not from the sky.",	
        SLINGSHOTAMMO_GOLD = "At least it's not from the sky.",
        SLINGSHOTAMMO_SLOW = "At least it's not from the sky.",
        SLINGSHOTAMMO_FREEZE = "At least it's not from the sky.",
		SLINGSHOTAMMO_POOP = "Heh. Grody.",
        PORTABLETENT = "Now I can nap anywhere, anytime.",
        PORTABLETENT_ITEM = "My dreams have finally come true. Literally.",

		-- Wigfrid
        BATTLESONG_DURABILITY = "Think these would go well with a ukulele?",
        BATTLESONG_HEALTHGAIN = "Think these would go well with a ukulele?",
        BATTLESONG_SANITYGAIN = "Think these would go well with a ukulele?",
        BATTLESONG_SANITYAURA = "Think these would go well with a ukulele?",
        BATTLESONG_FIRERESISTANCE = "This one's got a lotta heart...burn.",
        BATTLESONG_INSTANT_TAUNT = "This must be a lullaby cause I'm ready to take a nap.",
        BATTLESONG_INSTANT_PANIC = "This must be a lullaby cause I'm ready to take a nap.",

        -- Webber
        MUTATOR_WARRIOR = "Sweet treats, little buddy!",
        MUTATOR_DROPPER = "I bet these taste great, bud.",
        MUTATOR_HIDER = "Sweet treats, little buddy!",
        MUTATOR_SPITTER = "I bet these taste great, bud.",
        MUTATOR_MOON = "Sweet treats, little buddy!",
        MUTATOR_HEALER = "I bet these taste great, bud.",
        SPIDER_WHISTLE = "I never knew spiders loved to party.",
        SPIDERDEN_BEDAZZLER = "You're making masterpieces already, bud!",
        SPIDER_HEALER = "Even spiders need a little bit of chakra cleansing.",
		SPIDER_REPELLENT = "For when we're done crawling with the creeps.",
        SPIDER_HEALER_ITEM = "I don't feel much different when I taste it.",

		-- Wendy
		GHOSTLYELIXIR_SLOWREGEN = "Nice brew, dude.",
		GHOSTLYELIXIR_FASTREGEN = "Nice brew, dude.",
		GHOSTLYELIXIR_SHIELD = "Nice brew, dude.",
		GHOSTLYELIXIR_ATTACK = "Nice brew, dude.",
		GHOSTLYELIXIR_SPEED = "Nice brew, dude.",
		GHOSTLYELIXIR_RETALIATION = "Nice brew, dude.",
		SISTURN =
		{
			GENERIC = "Maybe I'll send some flowers out to sea for my brother...",
			SOME_FLOWERS = "I know of some real rad flowers that'd look great on it.",
			LOTS_OF_FLOWERS = "I bet she loves it.",
		},
        --v2 Warly
        PORTABLECOOKPOT_ITEM =
        {
            GENERIC = "That's where Warly cooks up something gnarly.",
            DONE = "Warly cooked something. ‘Ono!",
			COOKING_LONG = "So much work goes into that. Work that I don't gotta do!",
			COOKING_SHORT = "Food incoming!",
			EMPTY = "Warly's grinds broke the mouth! When's seconds?",
        },
        PORTABLEBLENDER_ITEM = "A wicked ocean twister for food.",
        PORTABLESPICER_ITEM =
        {
            GENERIC = "Spices up my life SO good!",
            DONE = "I'd rather be eating that all day than looking at it all day.",
        },
		SPICE_GARLIC = "This stuff can make ANYTHING good.",
        SPICE_SUGAR = "Sticky sweet sugar!",
        SPICE_CHILI = "Gives the best kind of burns.",
		SPICE_SALT = "Makes already sweet‘ono grinds taste like the sea!",
		ASPARAGUSSOUP = "You can never go wrong with soup.",
        VEGSTINGER = "Hipahipa!",
        BANANAPOP = "The best way to keep cool. And very little preparation!",
        CEVICHE = "All I need is sunshine and some good food.",
		SALSA = "Some chips, some sun, and I'm happy for a lifetime.",
        PEPPERPOPPER = "It's a party for my mouth!",
		FROGFISHBOWL = "It's like I'm eating a swamp.",
        POTATOTORNADO = "This one really twists my taters.",
        DRAGONCHILISALAD = "I always knew salads needed some spice.",
        GLOWBERRYMOUSSE = "It's like an ice cream that knows how to hang ten!",
        VOLTGOATJELLY = "I hope it doesn't sting on the way down.",
        NIGHTMAREPIE = "I'm all for pies but this one's throwing off my vibes.",
        BONESOUP = "Nice warm soup.",
        MASHEDPOTATOES = "All mushed up. ‘Onolicious!",
        POTATOSOUFFLE = "Good enough to eat!",
        MOQUECA = "I could shove my face right in this.",
        GAZPACHO = "This drink's as chill as me!",
		--
		TURNIP = "A good turnip never turndowns.",
        TURNIP_COOKED = "Looks like our luck is about to turnip!",
        TURNIP_SEEDS = "Ugh, work.",
        GARLIC = "There's nothing that this stuff can't make taste good!",
        GARLIC_COOKED = "I can hear it sizzling!",
        GARLIC_SEEDS = "They don't look like food. Do we gotta do the work?",
        ONION = "Layered with love.",
        ONION_COOKED = "Looks a little tastier.",
        ONION_SEEDS = "I could grow any kine with these.",
        POTATO = "I love eating these things!",
        POTATO_COOKED = "Mmmm...",
        POTATO_SEEDS = "Ugh, I guess I should plant these?",
        TOMATO = "Red, the most ‘onolicious color.",
        TOMATO_COOKED = "Maybe if I smush 'em I'll get some paste for my fries.",
        TOMATO_SEEDS = "You mean I gotta plant these myself?",
		ASPARAGUS = "It's like they're wearing little vegetable hats.", 
        ASPARAGUS_COOKED = "I'm gonna eat it!",
        ASPARAGUS_SEEDS = "Too much work goes into farming.",
		PEPPER = "Wicked spicy!",
        PEPPER_COOKED = "I thought these were tears of joy, but it's just super spicy.",
        PEPPER_SEEDS = "Should I plant these or eat 'em like this?",
		--woodie
		WEREITEM_BEAVER = "I bet this'd float.",
        WEREITEM_GOOSE = "Gnarly, it's full of meat.",
        WEREITEM_MOOSE = "Not really into meaty idols.",
		--
		MERMHAT = "Sweet! I can finally see through the eyes of a fish.",
		MERMTHRONE =
        {
            GENERIC = "Seems chill enough to me.",
            BURNT = "Guess that's how the hierarchy crumbles.",
        },        
        MERMTHRONE_CONSTRUCTION =
        {
            GENERIC = "Geez, even fish do more work than me.",
            BURNT = "That's a more familiar shade of work, I guess.",
        },  
        MERMHOUSE_CRAFTED = 
        {
            GENERIC = "I think you did pretty well, little fish-bud.",
            BURNT = "Ugh, bummer. That was someone's hard work.",
        },
        MERMWATCHTOWER_REGULAR = "Where the workers take five.",
        MERMWATCHTOWER_NOKING = "I don't wanna be caught up in a revolution.",
        MERMKING = "Huh. Didn't expect Poseidon to be so... smelly.",
        MERMGUARD = "You don't mind working for a living.",
        MERM_PRINCE = "You could use a fresh dunk, bud.",
		--RoT
        BOATFRAGMENT03 = "Boat chunks...",
        BOATFRAGMENT04 = "Boat chunks...",
        BOATFRAGMENT05 = "Boat chunks...",
		BOAT_LEAK = "My floater's been dinged!",
        MAST = "A blow boater's best friend.",
		SEASTACK = "I'd wanna avoid crashing into that.",
        FISHINGNET = "Lootin' and cruisin'.", --unimplemented
        ANTCHOVIES = "I thought anchovies couldn't get any worse.", --unimplemented
        STEERINGWHEEL = "That's a wheel lotta work. I'll be in the sleeping quarters.",
        ANCHOR = "I am NOT lifting that.",
        BOATPATCH = "Surfboard maintenance is essential!",
		BOAT_ITEM = "The second best way to travel.",
		BOAT_GRASS_ITEM = "I've definitely seen better boats.",
		STEERINGWHEEL_ITEM = "I don't need a wheel to steer my board.",
		ANCHOR_ITEM = "Huh. Not as heavy when it's like this.",
        MAST_ITEM = "I'd rather build a board.",
		WALKINGPLANK = "I don't wanna walk it. I don't even feel like walking!",
		WALKINGPLANK_GRASS = "I don't wanna walk it. I don't even feel like walking!",
        OAR = "I'd really rather board.",
		OAR_DRIFTWOOD = "Aww, rowing? Ugh.",
		MINIFLARE = "Hope this means I'll get lost a little less.",
		MEGAFLARE = "Getting on everyone's bad side is the last thing I wanna do.",
		BATHBOMB = "I hope that's organic.",
		ICEBERG =
        {
            GENERIC = "We gotta avoid those. Trust me.",
            MELTED = "That berg's all melted up.",
        },
		ICEBERG_MELTED = "That berg's all melted up.",

		MOON_FISSURE = 
		{
			GENERIC = "This thing's aura is all off wack.", 
			NOLIGHT = "I'm gonna avoid getting my boot stuck in that.",
		},
        MOON_ALTAR =
        {
            MOON_ALTAR_WIP = "You owe me big time after I'm done, spirits.",
            GENERIC = "Freaky, you're giving me serious chicken skin.",
        },

        MOON_ALTAR_IDOL = "I don't like working, but you're really tuggin' on my mind.",
        MOON_ALTAR_GLASS = "I'm really not the lugs spiritual sculpts around type, guy.",
        MOON_ALTAR_SEED = "I'll give you a home if you promise me the same, dude.",

        MOON_ALTAR_ROCK_IDOL = "If you hold it up to your ear you can hear everything but the ocean.",
        MOON_ALTAR_ROCK_GLASS = "If you hold it up to your ear you can hear everything but the ocean.",
        MOON_ALTAR_ROCK_SEED = "If you hold it up to your ear you can hear everything but the ocean.",

		MOON_ALTAR_CROWN = "If you lived makai I'd have you home in no time.",
        MOON_ALTAR_COSMIC = "We're all waitin' for something, man. I'll sleep as long as it takes.",

        MOON_ALTAR_ASTRAL = "Sheesh. Time for a looong nap.",
        MOON_ALTAR_ICON = "You ready to go home yet, bud? That makes one of us!",
        MOON_ALTAR_WARD = "Do I really gotta carry you all over?", 

		DRIFTWOOD_LOG = "Seen plenty of sea refuge in my day.",
		DEAD_SEA_BONES = "A bunch of beached bones. How comforting.",
		TRAP_STARFISH =
        {
            GENERIC = "Cute! Think it'd make a good headpiece?",
            CLOSED = "No, definitely not a good headpiece.",
        },
		DUG_TRAP_STARFISH = "I can't be fooled three times. Maybe four. But not three.",
        SPIDER_MOON = 
        {
        	GENERIC = "Why must you exist so close to me.",
        	SLEEPING = "Yeah, I'll pass.",
        	DEAD = "Grody.",
        },
		DRIFTWOOD_TREE = 
        {
            BURNING = "Kinda hard to look at that.",
            BURNT = "Harsh.",
            CHOPPED = "Maybe we could still save it?",
            GENERIC = "Really makes you sad looking at it. Is it just me?",
        },
		HOTSPRING = 
        {
        	GENERIC = "I could use a nice relaxing ocean bath.",
        	BOMBED = "Neat. What's it doing to my water?",
        	GLASS = "You can't surf on glass.",
			EMPTY = "Everything's worse without water.",
        },
		SEAFARING_PROTOTYPER =
        {
            GENERIC = "You mean I gotta think too? Ugh.",
            BURNT = "I can't sea the science anymore.",
        },
		PUFFIN =
        {
            GENERIC = "Beautiful plumage, puffin-dude!",
            HELD = "As soft as I imagined!",
            SLEEPING = "All shagged out.",
        },
		FRUITDRAGON =
		{
			GENERIC = "Little dude needs some space to bloom.",
			RIPE = "Ripe! Proud of you, little dude.",
			SLEEPING = "That's the right attitude.",
		},
		CARRAT = 
        {
        	DEAD = "Aww... poor little guy.",
        	GENERIC = "Kinda cute, in a rat kinda way.",
        	HELD = "You smell like both a rat and a carrot. Grody.",
        	SLEEPING = "Sleep tight, little carrot dude.",
        },
		MOONBUTTERFLY = 
        {
        	GENERIC = "Butterflies on the moon are just as pretty.",
        	HELD = "Don't worry bud, I'll keep you safe.",
        },
		MOONBUTTERFLY_SAPLING = "Aww... cute!",
        ROCK_AVOCADO_FRUIT = "I'd eat it, but I kinda need my teeth.",
        ROCK_AVOCADO_FRUIT_RIPE = "Finally, my teeth are safe.",
        ROCK_AVOCADO_FRUIT_RIPE_COOKED = "Mm! Fresh cooked avocados!",
        ROCK_AVOCADO_FRUIT_SPROUT = "When you gotta grow, you gotta grow.",
        ROCK_AVOCADO_BUSH = 
        {
        	BARREN = "It's a little tired.",
			WITHERED = "I thought this was from the moon, not the sun.",
			GENERIC = "Sweet! Moon berries!",
			PICKED = "Ugh. I have to wait for more.",
			DISEASED = "It's lookin' pretty ill.",
            DISEASING = "You alright there, bush?",
			BURNING = "I wish that hadn't happened.",
		},
		MOON_TREE = 
        {
            BURNING = "That's a drag on my mood.",
            BURNT = "Hope it didn't suffer.",
            CHOPPED = "Sorry, tree.",
            GENERIC = "Wicked ethereal beauty, tree.",
        },
		MOON_TREE_BLOSSOM = "So pretty! Think it'd look good on me?",
		BULLKELP_PLANT = 
        {
            GENERIC = "Gets caught on my board.",
            PICKED = "Been all swept up.",
        },
		MUTATEDHOUND = 
        {
        	DEAD = "Yeesh. What a way to go.",
        	GENERIC = "Someone's in need of a major chakra cleansing.",
        	SLEEPING = "They're better this way.",
        },
        MUTATED_PENGUIN = 
        {
			DEAD = "Ugh! Gnarly.",
			GENERIC = "Too close, scary dude!",
			SLEEPING = "It almost looks peaceful like that.",
		},
		MOONBUTTERFLYWINGS = "I doubt the moon would be happy about that.",
		MOONSPIDERDEN = "Different. But I'm still not touching it.",
		MOONGLASS = "Sweet. Seems organic.",
		MOONGLASS_CHARGED = "Somebody else better take this before it's junk.",
        MOONGLASS_ROCK = "It can be hard to look at myself these days.",
		KELPHAT = "This isn't so bad! I've had worse on my head after rag-dolling under some bomb waves.",
		KELP = "Gets caught on my board.",
		KELP_COOKED = "It's better with some crunch.",
		KELP_DRIED = "A little better I guess...",
		BULLKELP_ROOT = "Now I can plant it where I want.",
		GESTALT = "What's that little dude? You know how to get me back to the beach?",
		GESTALT_GUARD = "Yeesh, don't wanna step near that guy.",
		MOONGLASSAXE = "That's some tough glass.",
		GLASSCUTTER = "Not really my style.",
		COOKIECUTTER = "Knock off from my board, dude!",
		COOKIECUTTERSHELL = "All's well, that shell.",
		COOKIECUTTERHAT = "Sheesh. This hat's a menace.",
		SALTSTACK =
		{
			GENERIC = "This place has got some serious buildup.",
			MINED_OUT = "Hey, where'd all the sea salt go?",
			GROWING = "The sea never runs out of salt!",
		},
		SALTROCK = "Who's gonna stop me from licking it?",
		SALTBOX = "It keeps my food fresh, like me!",
		TACKLESTATION = "All the tools you need for a nice relaxing fishing trip.",
		TACKLESKETCH = "That's a nice drawing. I don't gotta make it do I?",

		GNARWAIL =
        {
            GENERIC = "This dude's super gnar. Literally.",
            BROKENHORN = "I hope that didn't hurt.",
            FOLLOWER = "This is the kind of cute I live for.",
            BROKENHORN_FOLLOWER = "If I don't protect my sea buds, who will?",
        },
        GNARWAIL_HORN = "Gnar- Hey, wait. That's my line!",

        MALBATROSS = "Gnarly! You're riding some serious negative waves!",
        MALBATROSS_FEATHER = "It's so soft!",
        MALBATROSS_BEAK = "Gag! I can smell its lunch!",
        MAST_MALBATROSS_ITEM = "Cruel. This is why I'd rather board.",
        MAST_MALBATROSS = "Might as well make the best of it.",
		MALBATROSS_FEATHERED_WEAVE = "I could nap in this forever.",
		
		OCEANFISHINGROD = "I'm ready to find a nice spot to relax with this.",
        OCEANFISHINGBOBBER_BALL = "Sometimes you just need the basics.",
        OCEANFISHINGBOBBER_OVAL = "Simple, but effective.",
		OCEANFISHINGBOBBER_CROW = "All geared up and nowhere to crow.",
		OCEANFISHINGBOBBER_ROBIN = "Feathers work for anything. Unlike me.",
		OCEANFISHINGBOBBER_ROBIN_WINTER = "Feathers work for anything. Unlike me.",
		OCEANFISHINGBOBBER_CANARY = "Feathers work for anything. Unlike me.",
		OCEANFISHINGBOBBER_GOOSE = "If I was a fish I'd love a soft feather too.",
		OCEANFISHINGBOBBER_MALBATROSS = "You don't need anything fancy for fish. But I won't speak for them.",

		OCEANFISHINGLURE_SPINNER_RED = "I bet it tastes great, but I'm not a fish.",
		OCEANFISHINGLURE_SPINNER_GREEN = "I bet it tastes great, but I'm not a fish.",
		OCEANFISHINGLURE_SPINNER_BLUE = "I bet it tastes great, but I'm not a fish.",
		OCEANFISHINGLURE_SPOON_RED = "I bet it tastes great, but I'm not a fish.",
		OCEANFISHINGLURE_SPOON_GREEN = "I bet it tastes great, but I'm not a fish.",
		OCEANFISHINGLURE_SPOON_BLUE = "I bet it tastes great, but I'm not a fish.",
		OCEANFISHINGLURE_HERMIT_RAIN = "I could think of better weather for fishing.",
		OCEANFISHINGLURE_HERMIT_SNOW = "A fish's worst nightmare.",
		OCEANFISHINGLURE_HERMIT_DROWSY = "I'm drowsy all the time too!",
		OCEANFISHINGLURE_HERMIT_HEAVY = "Hope it's not too heavy for the dudes below.",

		OCEANFISH_SMALL_1 = "Hey there, little guy.",
		OCEANFISH_SMALL_2 = "Well, they can't all be winners.",
		OCEANFISH_SMALL_3 = "Pretty small, guy.",
		OCEANFISH_SMALL_4 = "Maybe I could go for a bigger one next time.",
		OCEANFISH_SMALL_5 = "I hope it tastes better than the guilt.",
		OCEANFISH_SMALL_6 = "Even out at sea I still gotta look at leaves.",
		OCEANFISH_SMALL_7 = "Wow, you're a pretty one, huh?",
		OCEANFISH_SMALL_8 = "Yowza, you're real hot!",
		OCEANFISH_SMALL_9 = "Is it spitting? Haha, gross.",

		OCEANFISH_MEDIUM_1 = "Its doofiness is kinda endearing.",
		OCEANFISH_MEDIUM_2 = "Hope it was worth the work.",
		OCEANFISH_MEDIUM_3 = "I love lion-around too, bud.",
		OCEANFISH_MEDIUM_4 = "If I don't eat you will you still give me bad luck?",
		OCEANFISH_MEDIUM_5 = "I can't kill that! Look how cute it is!",
		OCEANFISH_MEDIUM_6 = "Lookin' koi, bud!",
		OCEANFISH_MEDIUM_7 = "Lookin' koi, bud!",
		OCEANFISH_MEDIUM_8 = "These waters might be less tropical than I thought.",
		OCEANFISH_MEDIUM_9 = "You're one of the sweetest fish I've ever seen. Literally.",

		PONDFISH = "Blub blub, little chum.",
		PONDEEL = "If you squint it's actually kind of cute.",
        FISHMEAT = "Needs more fire.",
        FISHMEAT_COOKED = "From my own personal recipe.",
        FISHMEAT_SMALL = "Small fish from a big pond.",
        FISHMEAT_SMALL_COOKED = "Is it snacktime?",
		SPOILED_FISH = "Gag material!",

		FISH_BOX = "They're really packed in there, huh.",
        POCKET_SCALE = "Just as good at weighing.",
        TACKLECONTAINER = "I'm diggin' this extra space.",
		SUPERTACKLECONTAINER = "That's one problem tackled.",
		TROPHYSCALE_FISH =
		{
			GENERIC = "I'm not a fan of turning it into a contest.",
			HAS_ITEM = "Weight: {weight}\nCaught by: {owner}",
			HAS_ITEM_HEAVY = "Weight: {weight}\nCaught by: {owner}\nSick catch, dude!",
			BURNING = "That's what happens when you brag too fast.",
			BURNT = "That's a huge bummer.",
			OWNER = "Weight: {weight}\nCaught by: {owner}\n...I guess I'm pretty good at that?",
			OWNER_HEAVY = "Weight: {weight}\nCaught by: {owner}\nAre we gonna eat it or...?",
		},
		OCEANFISHABLEFLOTSAM = "Heh. Grody.",
	    SQUID = "Hey, it's kinda cute!",
		CALIFORNIAROLL = "I could eat a boat load of that stuff.",
		SEAFOODGUMBO = "My favorite!",
		SURFNTURF = "Can I eat it on my surfboard?",
		WOBSTER_SHELLER = "Yummy sea bug.", 
        WOBSTER_DEN = "Huh. I thought they liked sleeping more.",
        WOBSTER_SHELLER_DEAD = "Soon to be good eats.",
        WOBSTER_SHELLER_DEAD_COOKED = "Now that's good eats!",
        LOBSTERBISQUE = "Looks fancy. And delicious.",
        LOBSTERDINNER = "Hey wobster, what's eatin' you? Oh, me!",
        WOBSTER_MOONGLASS = "Nice glass coating, sea bug.",
        MOONGLASS_WOBSTER_DEN = "They looked different just a moment ago?",
		TRIDENT = "It's time to rock'n'roll, fishes.",

		WINCH =
		{
			GENERIC = "A winch that likes a good pinch.",
			RETRIEVING_ITEM = "Finally, the do-the-work-for-me machine!",
			HOLDING_ITEM = "Finally, the do-the-work-for-me machine!",
		},
        HERMITHOUSE = {
            GENERIC = "It could use some love.",
            BUILTUP = "Sweet abode for taking a killer nap.",
        }, 
        SHELL_CLUSTER = "I can hear the ocean in there.",
        --
		SINGINGSHELL_OCTAVE3 =
		{
			GENERIC = "I can get down with this!",
		},
		SINGINGSHELL_OCTAVE4 =
		{
			GENERIC = "Lovely voice, dude.",
		},
		SINGINGSHELL_OCTAVE5 =
		{
			GENERIC = "Makes me wish I brought my uke.",
        },
        CHUM = "Mmm. Fish food.",
        SUNKENCHEST =
        {
            GENERIC = "Some real nice shells in there.",
            LOCKED = "I don't gotta lock pick, right?",
        },
        RESKIN_TOOL = "Ugh. Cleaning.",
        MOON_FISSURE_PLUGGED = "Whew. If it works, it works.",
		GHOSTFLOWER = "That's one mystical flower.",
        SMALLGHOST = "Their cuteness hides all the sadness.",
        CRABKING = 
        {
            GENERIC = "Tone all the crab down, will ya?",
            INERT = "It's time for crab.",
        },
		CRABKING_CLAW = "Yikes! Claws off my board, dude!",
		MESSAGEBOTTLE = "I wonder how far it's traveled.",
		MESSAGEBOTTLEEMPTY = "Maybe I could put something in it... eventually.",
        MEATRACK_HERMIT =
        {
			BURNT = "Your days of jerkying are over.",
			DONE = "That's some good looking jerky.",
			DRYING = "See you later, water.",
			DRYINGINRAIN = "Water doesn't make things dry.",
			GENERIC = "This rack could use some meat.",
			DONE_NOTMEAT = "That's some good looking food.",
            DRYING_NOTMEAT = "See you later, water.",
            DRYINGINRAIN_NOTMEAT = "Water doesn't make things dry.",
        },
        BEEBOX_HERMIT =
        {
			BURNT = "Maybe the bees did some redecorating?",
			FULLHONEY = "It's overflowing with sweet, sweet honey!",
			GENERIC = "That box has bees in and around it.",
			NOHONEY = "Would it be hypocritical to tell the bees to get to work?",
			SOMEHONEY = "Looks like the bees made me some honey! Thanks!",
			READY = "It's overflowing with sweet, sweet honey!",
        },
        HERMITCRAB = "Need small kine company, aunty?",
        HERMIT_PEARL = "This is one pretty pearl, Pearl.",
        HERMIT_CRACKED_PEARL = "Uh... that wasn't me.",

        WATERPLANT = "Bodacious barnies, bud!",
        WATERPLANT_BOMB = "Those knuckles should stay on the hand.",
        WATERPLANT_BABY = "Cute!",
        WATERPLANT_PLANTER = "I've always wanted a garden at sea.",

        SHARK = "You're a surfer's worst nightmare, bud.",

        MASTUPGRADE_LAMP_ITEM = "Night-surfing here I come!",
        MASTUPGRADE_LIGHTNINGROD_ITEM = "I don't remember lightning and waves mixing well.",

        WATERPUMP = "Ugh. I'd rather stow barrels.",

        BARNACLE = "Ugh. I hate scrubbing these off my board.",
        BARNACLE_COOKED = "Yeesh. We've hit rock bottom.",

        BARNACLEPITA = "I never thought I'd be drooling over barnacles!",
        BARNACLESUSHI = "I've got my hands and some sweet knuckles. All I need!",
        BARNACLINGUINE = "Sweet, salty, barnacles.",
        BARNACLESTUFFEDFISHHEAD = "I'll eat it, I won't like it, but I'll eat it.",

        LEAFLOAF = "Is it more leaf than loaf or loaf than leaf?",
        LEAFYMEATBURGER = "A burger's still a burger!",
        LEAFYMEATSOUFFLE = "Yuck. Looks like something I'd cook.",
        MEATYSALAD = "Confuses my brain, but fills my stomach. That's all I need!",

		MOLEBAT = "Guy's got a lotta nose, no snout about it.",
		MOLEBATHILL = "That's no home of mine. I think.",
        BATNOSE = "Somebody dropped a nose. Grody.",
        BATNOSE_COOKED = "It's kinda weird to smell a nose.",
        BATNOSEHAT = "Sweet! I don't have to feed myself!",

        MUSHGNOME = "This dude knows how to get around the bend.",

        SPORE_MOON = "Don't breathe this.",

        MOON_CAP = "I'll try anything once! Probably twice!",
        MOON_CAP_COOKED = "It does unspeakable things to my psyche...",

        MUSHTREE_MOON = "I dig your fun vibes, guy.",

        LIGHTFLIER = "You've got one wicked light, bug-buddies.",
        GROTTO_POOL_BIG = "I could sit by it for hours...",
        GROTTO_POOL_SMALL = "I could sit by it for hours...",
        DUSTMOTH = "What'cha doing all that work for?",

        DUSTMOTHDEN = "How do I get me one of those?",

        ARCHIVE_LOCKBOX = "I'll try to figure this one out after a good nap.",
        ARCHIVE_CENTIPEDE = "Yikes! Never wake something up from a good nap!",
        ARCHIVE_CENTIPEDE_HUSK = "Guess that's junk.",

        ARCHIVE_COOKPOT =
        {
			COOKING_LONG = "If I watch it it'll never boil.",
			COOKING_SHORT = "Food incoming!",
			DONE = "Pau! Yum!",
			EMPTY = "We cooking on this old thing or what?",
			BURNT = "The food might taste a bit burnt.",
        },

        ARCHIVE_MOON_STATUE = "I've seen the moon before, I can't blame 'em.",
        ARCHIVE_RUNE_STATUE = 
        {
            LINE_1 = "No clue what kine thing they were going for.",
            LINE_2 = "I think we've seen da kine of symbol here. No, wait.",
            LINE_3 = "No clue what kine thing they were going for.",
            LINE_4 = "I think we've seen da kine of symbol here. Maybe?",
            LINE_5 = "No clue what kine thing they were going for.",
        },        

        ARCHIVE_RESONATOR = {
            GENERIC = "If it's not taking us makai I'm gonna be bummed.",
            IDLE = "Finally! I never wanna work another day in my life.",
        },

        ARCHIVE_RESONATOR_ITEM = "It sure does a lot more work than I do.",

        ARCHIVE_LOCKBOX_DISPENCER = {
          POWEROFF = "Maybe it'll start working after a nap or two.",
          GENERIC =  "I can take a sip if nobody else wants too.",
        },

        ARCHIVE_SECURITY_DESK = {
            POWEROFF = "Me too, me too.",
            GENERIC = "I'm not getting a good aura from this.",
        },

        ARCHIVE_SECURITY_PULSE = "Woah, this thing's got currents!",

        ARCHIVE_SWITCH = {
            VALID = "Shoots, I'll bite. What's going on?",
            GEMS = "I think it wants me to jam rocks in it.",
        },

        ARCHIVE_PORTAL = {
            POWEROFF = "I don't wanna get my hopes up over this lolo puka...",
            GENERIC = "Shoots, got it. Never have hopes.",
        },

        WALL_STONE_2 = "Seems strong...",
        WALL_RUINS_2 = "Why wall yourself off from the world?",

        REFINED_DUST = "All that work, it better taste good.",
        DUSTMERINGUE = "Looks like someone's been slacking off, and it wasn't me!",

        SHROOMCAKE = "Cake's cake! Any cake aches are gonna be future Walani's problem!",

        NIGHTMAREGROWTH = "This thing's throwing my chakras all off course!",

        TURFCRAFTINGSTATION = "Nothin' keeps you grounded more than the ground.",

        MOON_ALTAR_LINK = "I think a tide is rolling in.",

        -- FARMING (TODO)
        COMPOSTINGBIN =
        {
            GENERIC = "All that work and ya reek at the end? Pass.",
            WET = "Totally soggy.",
            DRY = "Way too crusty.",
            BALANCED = "Seems fine to me.",
            BURNT = "I really wish I couldn't smell right now.",
        },
        COMPOST = "Why can't those plants use their hands to eat, like me.",
        SOIL_AMENDER = 
		{ 
			GENERIC = "I think I'll wait it out far, far away.",
			STALE = "Still brewing? Looks like I got some extra sleep hours!",
			SPOILED = "I think it's almost done, but my nose doesn't wanna check.",
		},

		SOIL_AMENDER_FERMENTED = "Pau-ew!",

        WATERINGCAN = 
        {
            GENERIC = "For little plant showers!",
            EMPTY = "Time to go catch a little bit of what I love.",
        },
        PREMIUMWATERINGCAN =
        {
            GENERIC = "If I love these sprinkles, so will the little keiki.",
            EMPTY = "I know just the watering spot.",
        },

		FARM_PLOW = "Is this.. no. It couldn't be... a do-the-work-for-me machine?",
		FARM_PLOW_ITEM = "If all I gotta do is place it, consider me a full-time farmer.",
		FARM_HOE = "Ugh. Farming.",
		GOLDEN_FARM_HOE = "If I gotta farm I might as well look good doing it.",
		NUTRIENTSGOGGLESHAT = "I spy with this hat's eye some... dirt.",
		PLANTREGISTRYHAT = "I'm pretty good at being one with nature.",

        FARM_SOIL_DEBRIS = "Somebody will clean this up, eventually.",

		FIRENETTLES = "I'm all for spices but... maybe not these ones.",
		FORGETMELOTS = "These flowers are beautiful, man!",
		SWEETTEA = "That hits the spot... I'm gonna need a looot more cups of this.",
		TILLWEED = "You're gonna have to squat elsewhere, bud.",
		TILLWEEDSALVE = "This is why we gotta treat weeds better.",
		WEED_IVY = "That's one mean mea kanu.",
        IVY_SNARE = "Hey! Lay off the goods, guy!",

		TROPHYSCALE_OVERSIZEDVEGGIES =
		{
			GENERIC = "Weird dinner table but I'll take it.",
			HAS_ITEM = "Weight: {weight}\nHarvested on day: {day}\nI'm getting hungry over here.",
			HAS_ITEM_HEAVY = "Weight: {weight}\nHarvested on day: {day}\nCan we start chowin' down on it already?",
            HAS_ITEM_LIGHT = "I'm down for a light snack.",
			BURNING = "I don't remember harvesting a fire fruit.",
			BURNT = "Wiped out.",
        },
        
        CARROT_OVERSIZED = "I feel like the luckiest rabbit in the world!",
        CORN_OVERSIZED = "Those kernels are the size of my knuckles!",
        PUMPKIN_OVERSIZED = "I'm ready for the best pumpkin pie of my life.",
        EGGPLANT_OVERSIZED = "This has to be the most lolo egg I've ever seen.",
        DURIAN_OVERSIZED = "I hope that smell isn't nui too, sheesh!",
        POMEGRANATE_OVERSIZED = "Man, I could feed my whole self with this!",
        DRAGONFRUIT_OVERSIZED = "Please let there not be eyes on this dragon.",
        WATERMELON_OVERSIZED = "You better believe I'm eating every last bite.",
        TOMATO_OVERSIZED = "Think of the sandwiches this thing could go on...",
        POTATO_OVERSIZED = "I can already feel the kanak attack coming on...",
        ASPARAGUS_OVERSIZED = "I could think of worse food to have a lot of.",
        ONION_OVERSIZED = "That thing could make a whole crew cry.",
        GARLIC_OVERSIZED = "Your one-stop shop for wiping out vampires.",
        PEPPER_OVERSIZED = "I bet it's only the tenth spiciest thing I'll eat today.",
        
        VEGGIE_OVERSIZED_ROTTEN = "Pilau... I'm gonna need a moment.",

		FARM_PLANT =
		{
			GENERIC = "Howzit hangin'?",
			SEED = "Nap time!",
			GROWING = "Wake me when it's done.",
			FULL = "Anybody gonna pick that?",
			ROTTEN = "This bums me out.",
			FULL_OVERSIZED = "Think of all the grinds we're gettin' outta this!",
			ROTTEN_OVERSIZED = "Now this REALLY bums me out.",
			FULL_WEED = "Dude just needed a place to grow, is all.",

			BURNING = "They've still got a chance. Maybe I should do something...",
		},

        FRUITFLY = "Shoo off, guy!",
        LORDFRUITFLY = "Hey! Knock it off ya weirdo!",
        FRIENDLYFRUITFLY = "Hey, you're makin' the plants happy! Me next!",
        FRUITFLYFRUIT = "I'm not exactly the queenly type.",

        SEEDPOUCH = "What every little seed needs.",

		-- Crow Carnival
		CARNIVAL_HOST = "I like your energy. Think that beak can surf?",
		CARNIVAL_CROWKID = "Howzit hangin', little guy?",
		CARNIVAL_GAMETOKEN = "One of favorite kinda coins, only second to chocolate ones.",
		CARNIVAL_PRIZETICKET =
		{
			GENERIC = "One's enough for me.",
			GENERIC_SMALLSTACK = "I'm liking my odds here!",
			GENERIC_LARGESTACK = "We got choke tickets! A whole boat load!",
		},

		CARNIVALGAME_FEEDCHICKS_NEST = "Hiding any snacks under there?",
		CARNIVALGAME_FEEDCHICKS_STATION =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "Feeding myself is already pretty fun.",
		},
		CARNIVALGAME_FEEDCHICKS_KIT = "Setting it up is the least fun part.",
		CARNIVALGAME_FEEDCHICKS_FOOD = "Open wide, little keikis!",

		CARNIVALGAME_MEMORY_KIT = "Setting it up is the least fun part.",
		CARNIVALGAME_MEMORY_STATION =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "I bet I've got the best memory here! What're we doin' again...?",
		},
		CARNIVALGAME_MEMORY_CARD =
		{
			GENERIC = "You hiding any snacks under there?",
			PLAYING = "It's totally this one! No promises.",
		},

		CARNIVALGAME_HERDING_KIT = "Setting it up is the least fun part.",
		CARNIVALGAME_HERDING_STATION =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "Are you sure I can't just ask them nicely to explode?",
		},
		CARNIVALGAME_HERDING_CHICK = "Don't you ever get tired?",

		CARNIVALGAME_SHOOTING_KIT = "Setting it up is the least fun part.",
		CARNIVALGAME_SHOOTING_STATION =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "I'm better off without thinking about the implications.",
		},
		CARNIVALGAME_SHOOTING_TARGET =
		{
			GENERIC = "You hiding any snacks under there?",
			PLAYING = "Peace was never an option.",
		},

		CARNIVALGAME_SHOOTING_BUTTON =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "I like to think I'm good at pressing buttons.",
		},

		CARNIVALGAME_WHEELSPIN_KIT = "Setting it up is the least fun part.",
		CARNIVALGAME_WHEELSPIN_STATION =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "This is way less pressure than a steering wheel.",
		},

		CARNIVALGAME_PUCKDROP_KIT = "Setting it up is the least fun part.",
		CARNIVALGAME_PUCKDROP_STATION =
		{
			GENERIC = "Ugh. Nothing fun's ever free, is it?",
			PLAYING = "Be free, little egg.",
		},

		CARNIVAL_PRIZEBOOTH_KIT = "This better be rewarding.",
		CARNIVAL_PRIZEBOOTH =
		{
			GENERIC = "You've got some good stuff up for grabs.",
		},

		CARNIVALCANNON_KIT = "This is getting a little too familiar.",
		CARNIVALCANNON =
		{
			GENERIC = "Glad I don't have to stock these ones.",
			COOLDOWN = "Heh. A lot nicer than the real deal.",
		},

		CARNIVAL_PLAZA_KIT = "Wonder if it's got little birds in it?",
		CARNIVAL_PLAZA =
		{
			GENERIC = "I bet I could help liven this thing up.",
			LEVEL_2 = "Small kine more and we got ourselves a party.",
			LEVEL_3 = "Now THIS is a party!",
		},

		CARNIVALDECOR_EGGRIDE_KIT = "I'll try not to scramble this.",
		CARNIVALDECOR_EGGRIDE = "Looks so relaxing.",

		CARNIVALDECOR_LAMP_KIT = "Do we gotta set up everything here?",
		CARNIVALDECOR_LAMP = "Good little light.",
		CARNIVALDECOR_PLANT_KIT = "Even the trees needs work.",
		CARNIVALDECOR_PLANT = "I'd love a hut full'a these.",
		CARNIVALDECOR_BANNER_KIT = "If it was up to me I'd leave it like this.",
		CARNIVALDECOR_BANNER = "Hang this near a hammock and I'm totally gone for the day.",

		CARNIVALDECOR_FIGURE =
		{
			RARE = "Choka! We got a rare... thing!",
			UNCOMMON = "Oh, hey! That's a new one! I think?",
			GENERIC = "Another one? Sweet!",
		},
		CARNIVALDECOR_FIGURE_KIT = "I'm no stranger to pushing my luck.",
		CARNIVALDECOR_FIGURE_KIT_SEASON2 = "I'm no stranger to pushing my luck.",

        CARNIVAL_BALL = "I sure am having a ball.",
		CARNIVAL_SEEDPACKET = "Sweet! Seeds!",
		CARNIVALFOOD_CORNTEA = "The chunks add flavor.",

        CARNIVAL_VEST_A = "Really matches my hair, I think.",
        CARNIVAL_VEST_B = "Almost as good as a towel!",
        CARNIVAL_VEST_C = "For those long beach days.",

        -- YOTB
        YOTB_SEWINGMACHINE = "I've never sewn and I'm not about to start!",
        YOTB_SEWINGMACHINE_ITEM = "Ugh. I don't want work on top of work!",
        YOTB_STAGE = "Do you take naps in there?",
        YOTB_POST =  "It keeps beefalo posted.",
        YOTB_STAGE_ITEM = "Somebody should set that up.",
        YOTB_POST_ITEM =  "I won't set it up, but someone should.",


        YOTB_PATTERN_FRAGMENT_1 = "Can somebody sew these for me?",
        YOTB_PATTERN_FRAGMENT_2 = "Can somebody sew these for me?",
        YOTB_PATTERN_FRAGMENT_3 = "Can somebody sew these for me?",

        YOTB_BEEFALO_DOLL_WAR = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_DOLL = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_FESTIVE = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_NATURE = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_ROBOT = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_ICE = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_FORMAL = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_VICTORIAN = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },
        YOTB_BEEFALO_DOLL_BEAST = {
            GENERIC = "Looks like I got a new napping buddy.",
            YOTB = "I love it, that judge better love it too.",
        },

        WAR_BLUEPRINT = "Fierce!",
        DOLL_BLUEPRINT = "Somebody else should sew these on.",
        FESTIVE_BLUEPRINT = "It'd look good if somebody wanted to sew it.",
        ROBOT_BLUEPRINT = "I was told I need to sew... uh, da kine? I don't remember I wasn't listening.",
        NATURE_BLUEPRINT = "I love all-natural patterns.",
        FORMAL_BLUEPRINT = "Bet it'd look good all finished.",
        VICTORIAN_BLUEPRINT = "I can't argue with ruffles.",
        ICE_BLUEPRINT = "Way past cool!",
        BEAST_BLUEPRINT = "Must be our lucky day, beef-bud.",

        BEEF_BELL = "All my natural good vibes in a single ring!",
		-- YOT Catcoon
		KITCOONDEN = 
		{
			GENERIC = "That looks cozy. I want in.",
            BURNT = "Saying sorry won't bring it back.",
			PLAYING_HIDEANDSEEK = "I could hide in a bed right about now.",
			PLAYING_HIDEANDSEEK_TIME_ALMOST_UP = "Think they left and took a nap instead?",
		},

		KITCOONDEN_KIT = "Lotta busywork, but the kittens love it.",

		TICOON = 
		{
			GENERIC = "You're the cat captain!",
			ABANDONED = "Now I have to do all the work. Major bummer.",
			SUCCESS = "Good job, lil' buddy!",
			LOST_TRACK = "We got beat to it. Bummer.",
			NEARBY = "Sweet! Must be something around here?",
			TRACKING = "Take lead, I'll follow behind! Like, way behind. Shoots, I might just fall asleep.",
			TRACKING_NOT_MINE = "That's some other ship's hairy captain.",
			NOTHING_TO_TRACK = "The vibes must not be right.",
			TARGET_TOO_FAR_AWAY = "I think we're a little off-path.",
		},
		
		YOT_CATCOONSHRINE =
        {
            GENERIC = "Looks like more work.",
            EMPTY = "I could think up some things cats like, but that takes effort.",
            BURNT = "Someone wasn't happy with it.",
        },

		KITCOON_FOREST = "You're so sweet! I could nap in that fur!",
		KITCOON_SAVANNA = "You're so sweet! I could nap in that fur!",
		KITCOON_MARSH = "You're so sweet! I could nap in that fur!",
		KITCOON_DECIDUOUS = "You're so sweet! I could nap in that fur!",
		KITCOON_GRASS = "You're so sweet! I could nap in that fur!",
		KITCOON_ROCKY = "You're so sweet! I could nap in that fur!",
		KITCOON_DESERT = "You're so sweet! I could nap in that fur!",
		KITCOON_MOON = "You're so sweet! I could nap in that fur!",
		KITCOON_YOT = "You're so sweet! I could nap in that fur!",

        -- Moon Storm
        ALTERGUARDIAN_PHASE1 = {
            GENERIC = "Ugh, party-crashers.",
            DEAD = "Can I go back to bed now?",
        },
        ALTERGUARDIAN_PHASE2 = {
            GENERIC = "Nothing knows how to chill out anymore!",
            DEAD = "Seriously, I could use a nap. Forever.",
        },
        ALTERGUARDIAN_PHASE2SPIKE = "Nice try, I've dodged gnarlier waves than you!",
        ALTERGUARDIAN_PHASE3 = "I think it's way past the ability to chillax.",
        ALTERGUARDIAN_PHASE3TRAP = "I'm all for a nap, but I'm also all for not dying!",
        ALTERGUARDIAN_PHASE3DEADORB = "If I was that thing, I'd never get up again.",
        ALTERGUARDIAN_PHASE3DEAD = "Pau hana time!",

        ALTERGUARDIANHAT = "I could do a sick dance to its tune.",
        ALTERGUARDIANHATSHARD = "These would make some killer earrings, ya?",

        MOONSTORM_GLASS = {
            GENERIC = "All glassed up.",
            INFUSED = "Far out! That's totally ethereal!"
        },

        MOONSTORM_STATIC = "Whatever it is, I'm glad I'm not the one workin' on it.",
        MOONSTORM_STATIC_ITEM = "Lotta busywork, whatever it's doing.",
        MOONSTORM_SPARK = "This thing's sending chills down my spine.",

        BIRD_MUTANT = "Nasty! What happened, bud?",
        BIRD_MUTANT_SPITTER = "You're lookin' harsh, bud.",

        WAGSTAFF_NPC = "You need a hand, unko? I might need a break first.",
        ALTERGUARDIAN_CONTAINED = "Sick drainage!",

        WAGSTAFF_TOOL_1 = "Listen, I don't know my tools for a reason.",
        WAGSTAFF_TOOL_2 = "I gotta pass him da kine or whatever, ya?",
        WAGSTAFF_TOOL_3 = "This sure looks like something he'd need!",
        WAGSTAFF_TOOL_4 = "All this work's for what again?",
        WAGSTAFF_TOOL_5 = "I had to get da kine with the whatsit?",

        MOONSTORM_GOGGLESHAT = "If it works it works, I don't need an explanation! Those always make me fall asleep.",

        MOON_DEVICE = {
            GENERIC = "Whatever it's for, it's probably not for me.",
            CONSTRUCTION1 = "Our pau hana time better be wicked good after all this.",
            CONSTRUCTION2 = "Sheesh, is it done yet?",
        },
		-- Wanda
        POCKETWATCH_HEAL = {
			GENERIC = "The only time I track is nap time.",
			RECHARGING = "Finally, some time to chill.",
		},

        POCKETWATCH_REVIVE = {
			GENERIC = "The only time I track is lunch time.",
			RECHARGING = "Finally, some time to chill.",
		},

        POCKETWATCH_WARP = {
			GENERIC = "The only time I track is surf time.",
			RECHARGING = "Finally, some time to chill.",
		},

        POCKETWATCH_RECALL = {
			GENERIC = "I only keep track of the tides, and not the time kind.",
			RECHARGING = "Finally, some time to chill.",
		},

        POCKETWATCH_PORTAL = {
			GENERIC = "I only keep track of the tides, and not the time kind.",
			RECHARGING = "Finally, some time to chill.",
		},

        POCKETWATCH_WEAPON = {
			GENERIC = "Man, I don't think even I could help her chillax.",
--fallback to speech_wilson.lua 			DEPLETED = "only_used_by_wanda",
		},

        POCKETWATCH_PARTS = "Good thing these aren't mine, no clue what they're for.",
        POCKETWATCH_DISMANTLER = "That's a lotta tools, I just check the sky for my time.",

        POCKETWATCH_PORTAL_ENTRANCE = 
		{
			GENERIC = "Never thought I'd be surfing through time.",
			DIFFERENTSHARD = "Never thought I'd be surfing through time.",
		},
        POCKETWATCH_PORTAL_EXIT = "I think it's time to sleep at this point.",
		-- Waterlog
        WATERTREE_PILLAR = "This must be the ali'i nui of the trees here.",
        OCEANTREE = "Even the trees here like to surf.",
        OCEANTREENUT = "If you hold it up to your ear you can hear the ocean.",
        WATERTREE_ROOT = "I'll just paddle around these.",

        OCEANTREE_PILLAR = "I could chill under its ocean breeze forever.",
        
        OCEANVINE = "Pretty loose compared to the ones I've seen.",
        FIG = "These are total palette cleansers.",
		FIG_COOKED = "Pair this with Okolehao and I'm set for life.",

        SPIDER_WATER = "Figured I'd see spiders out here, bumbai.",
        MUTATOR_WATER = "Sweet treats, little buddy!",
        OCEANVINE_COCOON = "I won't bug you, you don't bug me, shoots?",
		OCEANVINE_COCOON_BURNT = "At least they still have the ocean.",

        GRASSGATOR = "A total peacekeeper.",

        TREEGROWTHSOLUTION = "Man, I'd love to be a tree right now.",

        FIGATONI = "I'll try a combo of just about anything these days.",
        FIGKABAB = "It's not sick without the stick.",
        KOALEFIG_TRUNK = "These figs are gonna give me a wicked kanak attack.",
        FROGNEWTON = "There's newton bad about this one!",

		-- The Terrorarium
        TERRARIUM = {
            GENERIC = "I'm getting dizzy around this thing.",
            CRIMSON = "I know a cursed treasure chest when I see one.",
            ENABLED = "It's almost like I'm staring at the sun! Ouch.",
			WAITING_FOR_DARK = "No better time to sleep!",
			COOLDOWN = "It's time to cool off.",
			SPAWN_DISABLED = "Huh. Well, back to the beach.",
        },

        -- Wolfgang
        MIGHTY_GYM = 
        {
            GENERIC = "This reminds me that I should take a nap.",
            BURNT = "Somebody should apologize for that.",
        },

        DUMBBELL = "I've done enough heavy lifting for one lifetime.",
        DUMBBELL_GOLDEN = "These are as heavy as my eyelids after a good meal.",
		DUMBBELL_MARBLE = "It's heavy as my eyelids after some sweet, sweet grinds.",
        DUMBBELL_GEM = "These are as heavy as my eyelids after a good meal.",
        POTATOSACK = "That's one successful boat trip right there.",

        TERRARIUMCHEST = 
		{
			GENERIC = "I never lost the urge to open treasure.",
			BURNT = "That doesn't help anything.",
			SHIMMER = "Who could resist some free treasure?",
		},

		EYEMASKHAT = "You'll stick out like a sore-eye wearing this.",

        EYEOFTERROR = "My eyes get like that too if I don't get any sleep.",
        EYEOFTERROR_MINI = "Ugh. I hate having all eyes on me.",
        EYEOFTERROR_MINI_GROUNDED = "Somebody should take care of that.",

        FROZENBANANADAIQUIRI = "That's one sweet treat!",
        BUNNYSTEW = "Mmm! I'm just gonna focus on the stew part.",
        MILKYWHITES = "Yuck. This stuff pollutes the sea.",

        CRITTER_EYEOFTERROR = "You're the apple of...somebody's eye.",

        SHIELDOFTERROR ="Feeding myself was enough work, now my weapons gotta eat too?",
        TWINOFTERROR1 = "Maka nui. Yeesh, I think it's giving me the stink eye.",
        TWINOFTERROR2 = "Maka nui. Yeesh, I think it's giving me the stink eye.",
        -- Year of the Catcoon
        CATTOY_MOUSE = "That little thing sure can surf!",
        KITCOON_NAMETAG = "I'll just let 'em choose their own path in life.",

		KITCOONDECOR1 =
        {
            GENERIC = "Sweet! Now I can sleep with peace of mind.",
            BURNT = "That's a real bummer.",
        },
		KITCOONDECOR2 =
        {
            GENERIC = "Sweet! Now I can sleep with peace of mind.",
            BURNT = "That's a total bummer.",
        },

		KITCOONDECOR1_KIT = "Yeah, I'll rest on that for later.",
		KITCOONDECOR2_KIT = "I'd do it but it's high-tide right about now.",
        -- WX78
        WX78MODULE_MAXHEALTH = "We could all use a little pick-me-up now and then.",
		WX78MODULE_MAXSANITY1 = "We could all use a little pick-me-up now and then.",
        WX78MODULE_MAXSANITY = "We could all use a little pick-me-up now and then.",
        WX78MODULE_MOVESPEED = "We could all use a little pick-me-up now and then.",
        WX78MODULE_MOVESPEED2 = "We could all use a little pick-me-up now and then.",
        WX78MODULE_HEAT = "We could all use a little pick-me-up now and then.",
        WX78MODULE_NIGHTVISION = "We could all use a little pick-me-up now and then.",
        WX78MODULE_COLD = "We could all use a little pick-me-up now and then.",
        WX78MODULE_TASER = "We could all use a little pick-me-up now and then.",
        WX78MODULE_LIGHT = "We could all use a little pick-me-up now and then.",
		WX78MODULE_MAXHUNGER1 = "We could all use a little pick-me-up now and then.",
        WX78MODULE_MAXHUNGER = "We could all use a little pick-me-up now and then.",
        WX78MODULE_MUSIC = "We could all use a little pick-me-up now and then.",
        WX78MODULE_BEE = "We could all use a little pick-me-up now and then.",
        WX78MODULE_MAXHEALTH2 = "We could all use a little pick-me-up now and then.",

        WX78_SCANNER = 
        {
            GENERIC ="This dude's a grade A air-surfer.",
            HUNTING = "This dude's a grade A air-surfer.",
            SCANNING = "This dude's a grade A air-surfer.",
        },

        WX78_SCANNER_ITEM = "Looks like he took a note from my book! Right behind ya, buddy.",
        WX78_SCANNER_SUCCEEDED = "Pau hana, bud! You can take a nap now.",
        WX78_MODULEREMOVER = "Must be like removing earrings, right?",

        SCANDATA = "Just thinking about reading all that makes me sleepy.",

        -- Pirates
        BOAT_ROTATOR = "Man, work on ships are so easy nowadays!",
        BOAT_ROTATOR_KIT = "Shouldn't we have a crew for this?",
        BOAT_BUMPER_KELP = "The boat would've gotten coated in 'em anyway.",
        BOAT_BUMPER_KELP_KIT = "Our boat's little bump buddy.",
        BOAT_BUMPER_SHELL = "A lot prettier than barnacles.",
        BOAT_BUMPER_SHELL_KIT = "Our boat's little bump buddy.",
        BOAT_CANNON = {
            GENERIC = "Can't we find a peaceful solution?",
            AMMOLOADED = "Shooting it into the water can make some pretty sweet waves.",
            NOAMMO = "Just kidding! You really fell for that one, haha.",
        },
        BOAT_CANNON_KIT = "I'd rather this thing stay in pieces.",
        CANNONBALL_ROCK_ITEM = "It couldn't hurt that bad, could it?",

        OCEAN_TRAWLER = {
            GENERIC = "I've dreamed of having a fish-for-me machine before!",
            LOWERED = "This is the perfect task for me, I'll get to sleep the whole time!",
            CAUGHT = "We got something!",
            ESCAPED = "Guess I was sleeping on the job again.",
            FIXED = "Pau hana!",
        },
        OCEAN_TRAWLER_KIT = "This would work better put together by someone that's not me.",

        BOAT_MAGNET =
        {
            GENERIC = "All magnets find each other in the end.",
            ACTIVATED = "Love must be in the air.",
        },
        BOAT_MAGNET_KIT = "I'm glad I don't have to do all the thinking around here.",

        BOAT_MAGNET_BEACON =
        {
            GENERIC = "It's for attracting magnet mates.",
            ACTIVATED = "I'm immune to its siren song.",
        },
        DOCK_KIT = "Nobody's expecting me to put this together, ya?",
        DOCK_WOODPOSTS_ITEM = "Keep me posted whenever these get put up.",

        MONKEYHUT =
        {
            GENERIC = "Sweet abode, guys.",
            BURNT = "Bummer of an abode.",
        },
        POWDER_MONKEY = "Yeesh. There's still time to quit the act, keko.",
        PRIME_MATE = "What you're doing here is wrong, dude. Trust me.",
		LIGHTCRAB = "You light up my world.",
        CUTLESS = "Pretty dinky compared to the real thing.",
        CURSED_MONKEY_TOKEN = "I bet I'd look great with this.",
        OAR_MONKEY = "Moar to love.",
        BANANABUSH = "They grow bananas! Gnarly!",
        DUG_BANANABUSH = "Planting this sounds like hard work.",
        PALMCONETREE = "Cool hair, tree.",
        PALMCONE_SEED = "This is a whole lotta work stored in here.",
        PALMCONE_SAPLING = "Maybe I'll get to see you all grown up.",
        PALMCONE_SCALE = "Neat casing, tree.",
        MONKEYTAIL = "Suggestions are just suggestions! I've GOT to know how these things taste.",
        DUG_MONKEYTAIL = "Suggestions are just suggestions! I've GOT to know how these things taste.",

        MONKEY_MEDIUMHAT = "High fashion for the high seas.",--SW
        MONKEY_SMALLHAT = "I didn't perfect these hair buns just to wear one of these.",
        POLLY_ROGERSHAT = "Captains of the birds is a pretty big step up, I think!",
        POLLY_ROGERS = "He's a real squawkbuckler.",--SW

        MONKEYISLAND_PORTAL = "Far out! It's like a hurricane in there!",
        MONKEYISLAND_PORTAL_DEBRIS = "Would it kill you to clean up after yourself on the beach?",
        MONKEYQUEEN = "I can vibe with a queen who doesn't care if I don't bow.",
        MONKEYPILLAR = "Sheesh, this place is a real jungle.",
        PIRATE_FLAG_POLE = "Where there's pirates, there's flags.",

        BLACKFLAG = "Harsh. Don't expect me to do any pirate stuff, I'm a lover these days.",
        PIRATE_STASH = "Treasure is the real treasure!",
        STASH_MAP = "Don't need to be a pirate to know what this map's all about.",

        BANANAJUICE = "'Ono loa! I'll be hanging ten with this all day!",

		FENCE_ROTATOR = "Why can't I just hug a fence the other way?",

		-- SHIPWRECKED QUOTES
        FLOTSAM = "Maybe I can fish it out.",
        SUNKEN_BOAT =
		{
			ABANDONED = "Surfboards don't sink!",
			GENERIC = "Surfboards don't sink!",
		},
        SUNKEN_BOAT_BURNT = "An admirer of the sea, burned to ash. What injustice!",

		BOAT_LOGRAFT = "Seriously?",
        BOAT_RAFT = "Worst. Surfboard. Ever.",
        BOAT_ROWB = "Awww, rowing? Ugh.",
        BOAT_CARGO = "This surfboard sure has a lot of pockets.",
        BOAT_ARMOURED = "What a clunky looking board.",
        BOAT_ENCRUSTED = "Whatever floats my boat.",
        CAPTAINHAT = "If only the boss could see me now.",

        BOAT_TORCH = "Night-surfing. I love it!",
        BOAT_LANTERN = "Night-surfing. I love it!",
        BOATREPAIRKIT = "Surfboard maintenance is essential!",
        BOATCANNON = "Can't we find a peaceful solution?",

        BOTTLELANTERN = "This will keep the dark away.",
        BIOLUMINESCENCE = "Pretty glowy goop!",

        BALLPHIN = "My surfing buddies!",
        BALLPHINHOUSE = "Man, even their houses are cute.",
        DORSALFIN = "I thought I was the only one around here who'd fallen to pieces.",
        TUNACAN = "Whoa! Righteous non-spoilage!",

        JELLYFISH = "Don't step on one in bare feet!!",
        JELLYFISH_DEAD = "It might still sting me.",
        JELLYFISH_COOKED = "I hope it doesn't sting on the way down.",
        JELLYFISH_PLANTED = "I hate these things.",
        JELLYJERKY = "Looks extra chewy.",
        RAINBOWJELLYFISH = "Hey there, little buddy.",
        RAINBOWJELLYFISH_DEAD = "Aw.",
        RAINBOWJELLYFISH_COOKED = "Aw jeez. It's even cute when it's cooked.",
        RAINBOWJELLYFISH_PLANTED = "Lookin' good, lil dude!",
        JELLYOPOP = "Is this junk food or health food? I can't tell.",

        CROCODOG = "It's cute, from a safe distance.",
        POISONCROCODOG = "I respect its right to exist, but... not so nearby!",
        WATERCROCODOG = "What's the crocodeal, dude?!",

        PURPLE_GROUPER = "His doofiness is kinda endearing.",
        PIERROT_FISH = "I can't kill that! Look how cute it is!",
        NEON_QUATTRO = "What a cute lil dude!",
        PURPLE_GROUPER_COOKED = "Sorry, man. It was you or me.",
        PIERROT_FISH_COOKED = "Delicious, delicious guilt.",
        NEON_QUATTRO_COOKED = "Sorry. I still gotta eat.",
        TROPICALBOUILLABAISSE = "Eating a lot cures the desert island blues.",

        FISH_FARM = 
        {
            EMPTY = "I guess I gotta find fish eggs now. Oof.",
            STOCKED = "C'mon fish! I'm an \"instant gratification\" kind of gal.",
            ONEFISH = "Hey lil dude! Welcome to the farm!",
            TWOFISH = "Aw, my fish has buddies!",
            REDFISH = "It's looking like a real party in there!",
            BLUEFISH  = "Do I really have to eat them?",
        },
    
        ROE = "Roe, roe, roe my board... I'm going nuts out here.",
        ROE_COOKED = "At least they didn't have faces.", 
	    CAVIAR = "I made them fancy, but they're still fish eggs.",

        CORMORANT = "Hang ten, stinky sea dude!",
        SEAGULL = "Poop hawk!",
        SEAGULL_WATER = "Ewww, its nethers are touching my waves...",
        TOUCAN = "You could catch waves with a nose like that!",
        PARROT = "Will you ever stop squawking?",
        PARROT_PIRATE = "He's a squawkbuckler.",

        SEA_YARD = 
        {
            ON = "Does it work on surfboards?",
            OFF = "Ughhh, I should refill it.",
            LOWFUEL = "It's running on fumes. Same!",
        },
        
        SEA_CHIMINEA =
        {
            EMBERS = "It looks totally pooped.",
            GENERIC = "I taught the fire how to surf.",
            HIGH = "That thing's working waaaay too hard.",
            LOW = "It's okay, fire. You can take a rest if you want.",
            NORMAL = "Don't strain yourself, fire.",
            OUT = "I guess it needed a rest.",
        },

        CHIMINEA = "Stay safe from the howling winds, precious light!",

        TAR_EXTRACTOR =
        {
            ON = "It's doing everything for me. Great!",
            OFF = "I'm giving Mother Nature a rest.",
            LOWFUEL = "It's gonna run out of fuel.",
        },

        TAR = "Why did I want this again?",
        TAR_TRAP = "Sticky and environmentally unfriendly!",
        TAR_POOL = "Aww, the tar's seeping right into the ocean water.",
        TARLAMP = "Will this fit on a surfboard?",
        TARSUIT = "This may have been my worst idea. Ever.",

        PIRATIHATITATOR =
        {
            BURNT = "Fire always wins.",
            GENERIC = "It holds many sea-crets.",
        },

        PIRATEHAT = "High fashion for the high seas.",

        MUSSEL_FARM =
        {
            GENERIC = "There's food hanging out down there.",
            STICKPLANTED = "That's it mussels, do some pull ups.",
        },

        MUSSEL = "I could eat a hundred of these.",
        MUSSEL_COOKED = "Needs shallots.",
        MUSSELBOUILLABAISE = "Seafood, my favorite.",
        MUSSEL_BED = "I should plant it before the poor things dry out.",
        MUSSEL_STICK = "The mussels glop to it.",

        LOBSTER = "Yummy sea bug.",
        LOBSTER_DEAD = "Soon to be good eats.",
        LOBSTER_DEAD_COOKED = "Now that's good eats!",
        LOBSTERHOLE = "I can wait as long as it takes.",
        SEATRAP = "Wobster goes in. Dinner comes out.",

        BUSH_VINE =
		{
			BURNING = "That's not cool.",
			BURNT = "Not much use to anyone now.",
			CHOPPED = "Those vines are mine.",
			GENERIC = "Looks a little tangled.",
		},

        VINE = "Good for lashing stuff down.",
        DUG_BUSH_VINE = "Do I really have to replant it?",

        ROCK_LIMPET =
        {
            GENERIC = "Just look at all these Barneys.",
            PICKED = "Nothing to eat here.",
        },

        LIMPETS = "They're free from the hustle and bustle of daily life.",
        LIMPETS_COOKED = "Still a bit slimy.",
        BISQUE = "This actually might've been worth the effort!",

        MACHETE = "What a hack job.",
        GOLDENMACHETE = "I can hack in style.",

        THATCHPACK = "For when my pockets are full.",
        PIRATEPACK = "Booty in the back.",
        SEASACK = "Squishy.",

        		
		SEAWEED_PLANTED =
        {
            GENERIC = "Gets caught on my board.",
            PICKED = "It will be back someday.",
        },

        SEAWEED = "Name says it all.",
        SEAWEED_COOKED = "A little better I guess...",
        SEAWEED_DRIED = "It's better with some crunch.",
		SEAWEED_STALK = "Now I can plant it where I want.", 

        DUBLOON = "It's my lucky day!",
        SLOTMACHINE = "Should I push my luck?",

        SOLOFISH = "Bring me my slippers.",
        SOLOFISH_DEAD = "Sorry, lil dude.",
        SWORDFISH = "I've resolved to be more of a lover than a fighter.",
        SWORDFISH_DEAD = "Woah, that fish is HUGE!",
        CUTLASS = "It's been awhile since I held one of these.",

        SUNKEN_BOAT_TRINKET_1 = "I have no idea how to use that.",
        SUNKEN_BOAT_TRINKET_2 = "Where's the toy surf board?",
        SUNKEN_BOAT_TRINKET_3 = "A wet candle in a bottle. Mega-genius!",
        SUNKEN_BOAT_TRINKET_4 = "So how much is the sea worth?",
        SUNKEN_BOAT_TRINKET_5 = "I've already got a pair of boots.",
        TRINKET_IA_13 = "Slammable soda!",
        TRINKET_IA_14 = "If I only had a pin...",
        TRINKET_IA_15 = "Choka! A Ukulele!",
        TRINKET_IA_16 = "An odd shaped plate...",
        TRINKET_IA_17 = "Too big...",
        TRINKET_IA_18 = "Old timey craftsmanship.",
        TRINKET_IA_19 = "I only take herbals...",
		TRINKET_IA_20 = "I have no idea h ow to use that.",
		TRINKET_IA_21 = "Is there a toy surf board?",
		TRINKET_IA_22 = "A candle in a bottle. Genius.",
		TRINKET_IA_23 = "Too much work to figure out what this does...",
        EARRING = "I hope someone didn't lose this...",

        TURF_BEACH = "Take the beach to go.",
        TURF_JUNGLE = "Gnarled-ly.",
        TURF_MAGMAFIELD = "Keeps me grounded.",
        TURF_TIDALMARSH = "Marshy.",
        TURF_ASH = "Ashy...",
        TURF_MEADOW = "Nice underfoot.",
        TURF_VOLCANO = "Keeps me grounded.",
        TURF_SWAMP = "Swampy.",
        TURF_SNAKESKIN = "Keeps me grounded.",

        WHALE_BLUE = "What's got ya down, fella?",
        WHALE_CARCASS_BLUE = "You don't have to be sad anymore...",
        WHALE_WHITE = "Do ye spout black blood and roll fin out?",
        WHALE_CARCASS_WHITE = "Vengeance is mine.",
        WHALE_TRACK = "Seems like a lot of effort to follow it.",
        WHALE_BUBBLES = "Something's down there...",
        BLUBBER = "If I had enough of this I could surf in any weather!",
        BLUBBERSUIT = "Gross, but effective.",
        HARPOON = "I don't wanna hurt any poor animals.",

        SAIL_PALMLEAF = "A blow boater's best friend.",
        SAIL_CLOTH = "I'll flow with the wind.",
        SAIL_SNAKESKIN = "Why bother when I have my board?",
        SAIL_FEATHER = "For smooth and swift sailing!",
        IRONWIND = "Uh... I'll stick to my board, thanks.",
        
        BERMUDATRIANGLE = "Take me to a new surf spot!",

        PACKIM = "You got a big mouth, fella.",
        PACKIM_FISHBONE = "I'll hold onto this.",

        TIGERSHARK = "I'll never complain about regular sharks again!",
        MYSTERYMEAT = "Total mushburger.",
        SHARK_GILLS = "It won't be needing these.",
        TIGEREYE = "Nasty.",
        DOUBLE_UMBRELLAHAT = "This should keep me at least twice as dry.",
        SHARKITTEN = "You're pretty cute for my worst nightmare.",

        SHARKITTENSPAWNER =
        {
            GENERIC = "There are kitties jammin' inside.",
            INACTIVE = "Just sand?",
        },

        WOODLEGS_KEY1 = "It's a boney key.",
        WOODLEGS_KEY2 = "Fancy.",
        WOODLEGS_KEY3 = "An iron key. What's it open?",
        WOODLEGS_CAGE = "Whoever invented cages had a cruel heart.",

        CORAL = "I need a fish tank to put this in!",
        ROCK_CORAL = "What a pretty coral reef.",
        LIMESTONENUGGET = "That's some tough stuff.",
        NUBBIN = "Heh. \"Nubbin.\"",
        CORALLARVE = "Look at this lil slime dude! How are you, slime dude?",
        WALL_LIMESTONE = "Now THIS is a wall!",
        WALL_LIMESTONE_ITEM = "Now THIS is a wall!",
        WALL_ENFORCEDLIMESTONE = "A coral corral. Heh.",
        WALL_ENFORCEDLIMESTONE_ITEM = "I still have to like... build it.",
        ARMORLIMESTONE = "Seems maybe, like, *too* sturdy.",
        CORAL_BRAIN = "I get an uneasy feeling of despair near this.",
        CORAL_BRAIN_ROCK = "That is an impressive piece of coral.",
        BRAINJELLYHAT = "It's, like, blowing my mind!",

        SEASHELL = "No two are alike. Well, except those two. And, uh... that one.",
        SEASHELL_BEACHED = "A gift from the sea.",
        ARMORSEASHELL = "The ocean will protect me!",

        ARMOR_LIFEJACKET = "In case the waves get all gnar'.",
        ARMOR_WINDBREAKER = "The wind slips right around me!",

        SNAKE = "I hope you can't swim.",
        SNAKE_POISON = "My buddy has a bodacious bite!",
        SNAKESKIN = "I could make a scaly bikini out of this!",
        SNAKEOIL = "I wasn't born yesterday.",
        SNAKESKINHAT = "Raindrops keep falling on my hat.",
        ARMOR_SNAKESKIN = "I guess snakes aren't so bad.",

        SNAKEDEN =
        {
            BURNING = "That's not cool.",
            BURNT = "Not much use to anyone now.",
            CHOPPED = "Those vines are mine.",
            GENERIC = "Looks a little tangled.",
        },

        OBSIDIANFIREPIT =
        {
            EMBERS = "This fire is tired.",
            GENERIC = "Oh, sweet enemy of darkness.",
            HIGH = "So much bright!",
            LOW = "It's barely doing its job.",
            NORMAL = "This fire keeps on lickin' and tickin'.",
            OUT = "It's taking a break.",
        },

        OBSIDIAN = "Ouch, you're hot!",
        ROCK_OBSIDIAN = "It'd be too much work to get you open...",
        OBSIDIAN_WORKBENCH = "For playing with fire.",
        OBSIDIANAXE = "I gotta work to make you work...",
        OBSIDIANMACHETE = "This blade's as hot as it is cool.",
        OBSIDIANCOCONADE = "Beware of the boom!",
        SPEAR_OBSIDIAN = "Ready. Aim. Fire-y.",
        VOLCANOSTAFF = "I'm the volcano's boss now!",
        ARMOROBSIDIAN = "It kinda pokes my butt.",

        COCONADE =
        {
            BURNING = "This will make a big splash!",
            GENERIC = "Just holding it makes me feel powerful.",
        },

        OBSIDIANCOCONADE =
        {
            BURNING = "Beware of the boom!",
            GENERIC = "Just holding it makes me feel powerful.",
        },

        VOLCANO_ALTAR =
        {
            GENERIC = "The volcano asks its price...",
            OPEN = "Ready to receive...",
        },

        VOLCANO = "Mount gnarly!",
        VOLCANO_EXIT = "Time to split this scene.",
        ROCK_CORAL = "What a pretty coral reef.",
        VOLCANO_SHRUB = "Rubs off on my hands.",
        LAVAPOOL = "It's like a gooey campfire!",

        COFFEEBUSH =
        {
           BARREN = "Nooo, I need my java!",
           GENERIC = "Java!",
           PICKED = "Hurry up and grow back!",
           WITHERED = "If it grew on the volcano...",
        },

        COFFEEBEANS = "Come to mama!",
        COFFEEBEANS_COOKED = "Peppy!",
        DUG_COFFEEBUSH = "Planting this sounds like hard work.",
        COFFEE = "Smells amazing!",


        --ELEPHANTCACTUS =
		--{
		--	BARREN = "",
		--	WITHERED = "",
		--	GENERIC = "Looking sharp!",
		--	PICKED = "",
		--},

        DUG_ELEPHANTCACTUS = "Planting this sounds like hard work.",
        ELEPHANTCACTUS = "Looking sharp!",
        ELEPHANTCACTUS_ACTIVE = "Ouch!",
        ELEPHANTCACTUS_STUMP = "It's taking a nap.",
        NEEDLESPEAR = "Eh. I don't get the point.",
        ARMORCACTUS = "How will I hug trees in this?",

        TWISTER = "This thing needs to relax!",
        TWISTER_SEAL = "I just wanna squish its face into my face.",
        TURBINE_BLADES = "Twirly!",
        MAGIC_SEAL = "Why would anyone need this much power?",
        WIND_CONCH = "Get those waves cranking!",
        WINDSTAFF = "Probably more useful than a regular stick.",

        DRAGOON = "Woah, tone it down, my guy.",
        DRAGOONHEART = "It must have been in love. It's still crazy hot.",
        DRAGOONSPIT = "My brother used to do the same thing.",
        DRAGOONEGG = "I licked one once. It burned my tongue.",
        DRAGOONDEN = "Work out? Why bother.",

        ICEMAKER =
        {
            HIGH = "Super chillin'.",
            LOW = "Not so chillin'.",
            NORMAL = "Just chillin'.",
            OUT = "Illin'.",
            VERYLOW = "Little bit of illin'.",
        },

        HAIL_ICE = "From the freezer in the sky.",

        BAMBOOTREE =
        {
            BURNING = "That won't last.",
            BURNT = "I think it's dead.",
            CHOPPED = "Hurry up and grow already!",
            GENERIC = "I guess I could cut that down.",
        },

        BAMBOO = "This looks pretty float-atious.",
        FABRIC = "Where did my sticks go? I've been bamboozled!",
        DUG_BAMBOOTREE = "Planting this sounds like hard work.",

        JUNGLETREE =
        {
            BURNING = "That's not cool.",
            BURNT = "I know the feeling.",
            CHOPPED = "Sliced and diced.",
            GENERIC = "It's like nature's canopy.",
        },
        JUNGLETREESEED = "A tree yee shall be.",
        JUNGLETREESEED_SAPLING = "Ugh, planting that got me all sweaty. Gross.",
        LIVINGJUNGLETREE = "That tree would make a great board.",

	    OX = "You smell like wet hair.",
	    BABYOX = "Stay young forever!",
        OXWOOL = "Thanks, ox-dude.",
        OX_HORN = "I'm glad it's not attached to anything.",
	    OXHAT = "I hope it doesn't mess up my ponytails.",
	    OX_FLUTE = "I'm gonna play some sweet jams on this baby.",

        MOSQUITO_POISON = "Get lost!",
        MOSQUITOSACK_YELLOW = "Ewwww.",

        STUNGRAY = "I thought shark farts were bad...",
        POISONHOLE = "Gag!",
        GASHAT = "Protection from stink.",

        ANTIVENOM = "This'll make the sickness go away.",
        VENOMGLAND = "What a terrible gland.",
        POISONBALM = "This'll make the sickness go away.",

        SPEAR_POISON = "Want to take a sick day?",
        BLOWDART_POISON = "Not good for party tricks. I learned the hard way.",

        SHARX = "Shoo! Shoo! Shoo!",
        SHARK_FIN = "The only kind of fin I wanna see.",
        SHARKFINSOUP = "I could shove my face right in this.",
        SHARK_TEETHHAT = "Feels wrong having these on my head...",
        AERODYNAMICHAT = "Aerodynamic. Choka.",

        IA_MESSAGEBOTTLE = "There must be a better way to send a message...",
        IA_MESSAGEBOTTLEEMPTY = "I could whistle a tune with you.",
        BURIEDTREASURE = "Hope this is worth the work.",

        SAND = "I like it between my toes and nowhere else.",
        SANDDUNE = "If this was bigger I could sand board.",
        SANDBAGSMALL = "Heavy lifting...",
        SANDBAGSMALL_ITEM = "Remind me not to swim while carrying these!",

        SANDCASTLE =
        {
            GENERIC = "I feel like a kid again!",
            SAND = "I like it between my toes and nowhere else.",
        },

        SUPERTELESCOPE = "I can super sea!!",
        TELESCOPE = "You can't hide from me, islands!",

        DOYDOY = "Well hey there little guy.",
        DOYDOYBABY = "Aw! You look like you want a cuddle!",
        DOYDOYEGG = "Could be a doydoy. Or could be dinner.",
        DOYDOYEGG_COOKED = "You can taste the endangerment.",
        DOYDOYEGG_CRACKED = "I didn't touch it, I swear.",
        DOYDOYFEATHER = "It's soft to the touch.",
        DOYDOYNEST = "Looks like a great place to nap. For a doydoy.",
        TROPICALFAN = "Sweet relief.",

        PALMTREE =
        {
            BURNING = "That's not cool.",
            BURNT = "I know the feeling.",
            CHOPPED = "Sliced and diced.",
            GENERIC = "Cool hair, tree.",
        },

        COCONUT = "Imagine getting conked on the head by one of these!",
        COCONUT_HALVED = "You get two for the price of one. Pfft.",
        COCONUT_COOKED = "Hmmm. Yep. It's cooked now.",
        COCONUT_SAPLING = "Hey little tree dude.",
        PALMLEAF = "Would make a radical skirt.",
        PALMLEAF_UMBRELLA = "My own pint-sized palm tree.",
        PALMLEAF_HUT = "It's like indoors, but outdoors.",
        LEIF_PALM = "Wanna be a board?!",

        CRAB =
        {
            GENERIC = "If I was on my surfboard I could catch you.",
            HIDDEN = "He's taking a break from running.",
        },
        
        CRABHOLE = "A beachfront home. Nice.",

        TRAWLNETDROPPED =
        {
            GENERIC = "A sack of sea goodies!",
            SOON = "Just a little more trawlin'...",
            SOONISH = "I wonder what it grabbed up...",
        },

        TRAWLNET = "Lootin' and cruisin'.",
        IA_TRIDENT = "I feel like the god of...something!",

        KRAKEN = "Surf's up!",
        KRAKENCHEST = "Treasure awaits me!",
        KRAKEN_TENTACLE = "It's much too floppy!",
        QUACKENBEAK = "He was probably a bit peckish when he died.",
        QUACKENDRILL = "This is not a drill! Wait, it totally is.",
        QUACKERINGRAM = "I prefer to be a pacifist on the pacific.",

        MAGMAROCK = "It's warm to the touch.",
        MAGMAROCK_GOLD = "It's warm to the touch.",
        FLAMEGEYSER = "It's got a hot temper.",

        TELEPORTATO_SW_BASE = "I sense its power...",
        TELEPORTATO_SW_BOX = "This thing attaches to the other thing?",
        TELEPORTATO_SW_CRANK = "I hope this isn't a broom 'cause I hate sweeping.",
        TELEPORTATO_SW_POTATO = "Tubular...",
        TELEPORTATO_SW_RING = "Doesn't look suited for hula-hooping...",

        PRIMEAPE = "This guy's a major beach leech.",
        PRIMEAPEBARREL = "Gnarly domicile, man...",
        MONKEYBALL = "What's that little dude? You want me to throw you?",
        WILBUR_CROWN = "I dig the style.",
        WILBUR_UNLOCK = "Welcome aboard, bud!",

        MERMFISHER = "You don't mind working for a living.",
        MERMHOUSE_FISHER = "Where the workers take five.",

        OCTOPUSCHEST = "Booty-ya!",
        OCTOPUSKING = "You seem chill.",
        
        SWEET_POTATO = "Dessert!",
        SWEET_POTATO_COOKED = "Hmm, it's not that sweet actually...",
        SWEET_POTATO_PLANTED = "Dessert grows in the ground!",
        SWEET_POTATO_SEEDS = "Ugh, I guess I should plant these?",
        --SWEET_POTATO_OVERSIZED = "",
        SWEETPOTATOSOUFFLE = "Good enough to eat!",

        BOAT_WOODLEGS = "I'd still rather board.",
        WOODLEGSHAT = "Nautical.",
        SAIL_WOODLEGS = "An acceptable alternative to surfing... I guess.",

        PEG_LEG = "Is it weird to just have one?",
        PIRATEGHOST = "Aw. He could've had an eternal nap, but he came back.",

        WILDBORE = "I'm not wild about it.",
        WILDBOREHEAD = "That's what happens when you lose your head.",
        WILDBOREHOUSE = "Bores live in houses? Wild!",

        MANGROVETREE = "This tree really likes water.",
        MANGROVETREE_BURNT = "How?",

        PORTAL_SHIPWRECKED = "I wonder what this does...",
        SHIPWRECKED_ENTRANCE = "Aloha, world!",
        SHIPWRECKED_EXIT = "Aloha, world!",

        TIDALPOOL = "Wonder how deep it goes...",
        FISHINHOLE = "Looks like a good place to get lunch.",
        FISH_TROPICAL = "Such tasty colors!",
        TIDAL_PLANT = "Stuff can grow anywhere I guess...",
        MARSH_PLANT_TROPICAL = "Tropical planty.",

        FLUP = "It's rude to stare, dude!",
        BLOWDART_FLUP = "It makes a wet squelch when it hits stuff.",

        SEA_LAB = "In the middle of the ocean and I still have to do stuff!",
        BUOY = "It's just hanging out.",
        WATERCHEST = "Now I don't need to swim back to shore for my stuff.",

        LUGGAGECHEST = "This trunk is a little steamy.",
        WATERYGRAVE = "That wound's a little too fresh.",
        SHIPWRECK = "Probably won't be sailing on that anytime soon.",
        BARREL_GUNPOWDER = "Hmm, was I in charge of keeping these stowed?",
        RAWLING = "This dude cracks me up!",
        GRASS_WATER = "This grass is well hydrated!",
        KNIGHTBOAT = "What a horrible knight to have a curse.",

        DEPLETED_BAMBOOTREE = "It's gone for now.",
        DEPLETED_BUSH_VINE = "No more here.",
        DEPLETED_GRASS_WATER = "That was all it had to offer.",

        WALLYINTRO_DEBRIS = "Boat chunks...",
        BOOK_METEOR = "I've got a bad feeling about this.",
        CRATE = "Wonder what's inside.",
        SPEAR_LAUNCHER = "Seems a little aggressive.",
        MUTATOR_TROPICAL_SPIDER_WARRIOR = "Sweet treats, little buddy!",

        BOAT_SURFBOARD = "My love for you is deeper than the ocean.",
		SURFBOARD_ITEM = "My pride and joy!",
    },

	DESCRIBE_GENERIC = "Yup, that's definitely a thing.",
	DESCRIBE_SMOLDERING = "It's getting too hot to handle.",
	DESCRIBE_TOODARK = "HELP! I'm freakin' out here!",

	DESCRIBE_PLANTHAPPY = "Nothing but good vibes on board!",
    DESCRIBE_PLANTVERYSTRESSED = "Aw man, little keiki's all stressed.",
    DESCRIBE_PLANTSTRESSED = "I think it needs a moment to chill.",
    DESCRIBE_PLANTSTRESSORKILLJOYS = "I knew it was gonna get to this point... weeds.",
    DESCRIBE_PLANTSTRESSORFAMILY = "I'll hang out with ya, bud!",
    DESCRIBE_PLANTSTRESSOROVERCROWDING = "My little sprout needs some alone time.",
    DESCRIBE_PLANTSTRESSORSEASON = "The season's not vibing with you, huh?",
    DESCRIBE_PLANTSTRESSORMOISTURE = "I know dehydration when I see it!",
    DESCRIBE_PLANTSTRESSORNUTRIENTS = "Looks like my bud needs a new snack of soil.",
    DESCRIBE_PLANTSTRESSORHAPPINESS = "I know just the song to help ya, little keiki.",

	EAT_FOOD =
	{
		TALLBIRDEGG_CRACKED = "Sorry, lil guy.",
		WINTERSFEASTFUEL = "Broke da mouth!",
	},
}
