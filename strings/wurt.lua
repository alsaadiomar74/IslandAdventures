-- This speech is for Wurt
return {

	ACTIONFAIL =
	{
		REPAIRBOAT = 
		{
			GENERIC = "No need to fix boat, florp.",
		},
		EMBARK = 
		{
			INUSE = "Let me on, florpt!",
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		OPEN_CRAFTING  = 
   		{
        FLOODED = "Why it not work in water, florp?",
		}, 

	},
	
	ANNOUNCE_MAGIC_FAIL = "It won't work here, florp.",
	
	ANNOUNCE_SHARX = "Glurp... they not look very nice.",
	
	ANNOUNCE_TREASURE = "Treasure?",
	ANNOUNCE_MORETREASURE = "Is another treasure?",
	ANNOUNCE_OTHER_WORLD_TREASURE = "Grrr... I can't find it here.",
	ANNOUNCE_OTHER_WORLD_PLANT = "It don't like this soil.",
	
	ANNOUNCE_IA_MESSAGEBOTTLE =
	{
		"Glorp... it too hard to read!",
	},
	ANNOUNCE_READ_BOOK =
	{
		BOOK_METEOR = "Want to throw pigfolk into volcano!",
	},
	ANNOUNCE_VOLCANO_ERUPT = "Volcano is angry, glurp!",
	ANNOUNCE_MAPWRAP_WARN = "It scary here, glorp!",
	ANNOUNCE_MAPWRAP_LOSECONTROL = "Something bad happening!",
	ANNOUNCE_MAPWRAP_RETURN = "G-glurp... that was scary.",
	ANNOUNCE_CRAB_ESCAPE = "Where did little crab go?",
	ANNOUNCE_TRAWL_FULL = "My net is full of treasures!",
	ANNOUNCE_BOAT_DAMAGED = "My boat don't look good.",
	ANNOUNCE_BOAT_SINKING = "Boat gets wet!",
	ANNOUNCE_BOAT_SINKING_IMMINENT = "Need to find land quick!",
	ANNOUNCE_WAVE_BOOST = "Splish splash!",
	
	ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "Where these bubbles go?",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "No more bubbles, flort.",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "Water too rough for bubbles, florp.",
	
	ANNOUNCE_CANTBUILDHERE_FISHERHOUSE = "Prettiest fish are in swamp!",

	DESCRIBE = {
	
		GHOST_SAILOR = "What you want, florpt?",
		FLOTSAM = "Bye-bye boat.",
		SUNKEN_BOAT = 
		{
			GENERIC = "Hello pirate birdy!",
			ABANDONED = "Where the birdie go?",
		},
		
		SUNKEN_BOAT_BURNT = "Pirate birdie lost his home.",
		SUNKBOAT = "It broken, florp.",

		BOAT_LOGRAFT = "Is a small boat.",
		BOAT_RAFT = "Nice and green, florp.",
		BOAT_ROW = "It good enough.",
		BOAT_CARGO = "It has room for treasures!",
		BOAT_ARMOURED = "It has pretty shells on it!",
		BOAT_ENCRUSTED = "Heavy boat make me feel safe, florp.",
		CAPTAINHAT = "Fancy looking hat!",

		BOAT_TORCH = "It torch for boat.",
		BOAT_LANTERN = "Glowy on stick!",
		BOATREPAIRKIT = "Why repair? Let water in, florpt!",
		BOATCANNON = "Shoots splodey balls, florp!",

		BOTTLELANTERN = "Glowy bottle!",
		BIOLUMINESCENCE = "Glowy water bugs!",

		BALLPHIN = "Silly Wicker-lady said these not fish folk, I know fish when I see one, florpt!",
		BALLPHINHOUSE = "That house full of cute fishies!",
		DORSALFIN = "Glargh!",
		TUNACAN = "Is fishie inside?",

		JELLYFISH = "My new pet!",
		JELLYFISH_DEAD = "Nooo!!",
		JELLYFISH_COOKED = "Glargh...",
		JELLYFISH_PLANTED = "Cute fishy, but hurts to touch.",
		JELLYJERKY = "Gluurrgh.",
		RAINBOWJELLYFISH = "Pretty!",
		RAINBOWJELLYFISH_PLANTED = "Is so colorful!",
		RAINBOWJELLYFISH_DEAD = "Was so pretty...",
		RAINBOWJELLYFISH_COOKED = "Glargh!",
		JELLYOPOP = "Blegh, who wanna eat that?",

		CROCODOG = "Bad doggy!",
		POISONCROCODOG = "Go away, florp!",
		WATERCROCODOG = "Doggy brought me some water!",
	
		PURPLE_GROUPER = "You look really nice, florp!",
		PIERROT_FISH = "Hello striped fishie!",
		NEON_QUATTRO = "Like your scales, flort!",
		PURPLE_GROUPER_COOKED = "No!!",
		PIERROT_FISH_COOKED = "Glurgh... who do such thing!",
		NEON_QUATTRO_COOKED = "Won't eat it!",
		TROPICALBOUILLABAISSE = "Who make this terrible thing?!",

		FISH_FARM = 
		{
			EMPTY = "I need fishie eggs.",
			STOCKED = "No fishies yet.",
			ONEFISH = "There's a fish!",
			TWOFISH = "Can't wait for more fishies!",
			REDFISH = "So many fishies!",
			BLUEFISH  = "I should make room for more fishies!",
		},
	
		ROE = "So many little fishies inside!",
		ROE_COOKED = "Glurgh... who cooks little babies?!",
		CAVIAR = "Glargh!",

		CORMORANT = "Where it get fish eggs from?!",
		SEAGULL = "Glargh, greedy bird!",
		SEAGULL_WATER = "Glargh, greedy bird!",
		TOUCAN = "Why it need a beak that big, florp?",
		PARROT = "There no bird like it back home.",
		PARROT_PIRATE = "How did you get that hat, florp?",

		SEA_YARD =
		{
			ON = "Good machine for my boat!",
			OFF = "It sleeping.",
			LOWFUEL = "It getting hungry.",
		},
	
		SEA_CHIMINEA = 
		{
			EMBERS = "It dying.",
			GENERIC = "Time for story-tell around fire, florp!",
			HIGH = "Big fire!",
			LOW = "Fire getting small.",
			NORMAL = "Warm and toasty.",
			OUT = "Bye-bye.",
		}, 

		CHIMINEA = "Wind is not scary anymore, florp!",

		TAR_EXTRACTOR =
		{
			ON = "Is making black goo.",
			OFF = "I have to wake it up.",
			LOWFUEL = "It getting hungry.",
		},

		TAR = "Sticky goo!",
		TAR_TRAP = "I will make pigfolk walk into it!",
		TAR_POOL = "It bad for fishies around.",
		TARLAMP = "Pretty lamp!",
		TARSUIT = "Have to wear it? Blegh...",

		PIRATIHATITATOR =
		{
			GENERIC = "Think you just made that up, florp.",
			BURNT = "Weird hat machine burned up.",
		},

		PIRATEHAT = "Has a skull on it.",

		MUSSEL_FARM =
		{
			GENERIC = "What they doing down there?",
			STICKPLANTED = "Little mussels stick to stick!"
		},

		MUSSEL = "They hiding in shell.",
		MUSSEL_COOKED = "Glurgh... who did that?",
		MUSSELBOUILLABAISE = "Blegh!",
		MUSSEL_BED = "You will feel better in water, florp!",
		MUSSEL_STICK = "Why pull the mussels out of the water?",

		LOBSTER = "No pinching!",
		LOBSTER_DEAD = "Is sleeping?",
		LOBSTER_DEAD_COOKED = "Ewww, it all pink now!",
		LOBSTERHOLE = "What's down there?",
		SEATRAP = "For fishies with shell.",

		BUSH_VINE =
		{
			BURNING = "Aaah, it hot!!",
			BURNT = "Bye-bye little bush.",
			CHOPPED = "Choppy chop!",
			GENERIC = "Hope it has no snake in there, florp...",
		},
		VINE = "Thick sticks!",
		DUG_BUSH_VINE = "I need you at home, florp.",
	
		ROCK_LIMPET =
		{
			GENERIC = "Is full of little snails.",
			PICKED = "All snails are gone, florp.",
		},

		LIMPETS = "Glurgh...",
		LIMPETS_COOKED = "Not gonna eat it, florp.",
		BISQUE = "Don't want it",

		MACHETE = "Plant hacker.",
		GOLDENMACHETE = "It better than normal hacker.",

		THATCHPACK = "For carry small things!",
		PIRATEPACK = "Is for carrying my treasures, florp.",
		SEASACK = "Slimy backpack!",

		SEAWEED_PLANTED =
        {
            GENERIC = "Water snack!",
            PICKED = "Will come back later, florp.",
        },

		SEAWEED = "Sea snack!",
		SEAWEED_COOKED = "Mmmm, slimy!",
		SEAWEED_DRIED = "Salty crunchies.",
		SEAWEED_STALK = "Plant more water snacks!",

		DUBLOON = "Shiny coins!",
		SLOTMACHINE = "Can I win a fish?",
		
		SOLOFISH = "Looks like a doggy, but.. a fish?",
		SOLOFISH_DEAD = "Was so cute...",
		SWORDFISH = "Pointy fish is not nice!",
		SWORDFISH_DEAD = "Sorry, bad pointy fishy...",
		CUTLASS = "Glurgh...",

		SUNKEN_BOAT_TRINKET_1 = "What is that?", --sextant
		SUNKEN_BOAT_TRINKET_2 = "Tiny boat.", --toy boat
		SUNKEN_BOAT_TRINKET_3 = "It wet, I like it!.", --candle
		SUNKEN_BOAT_TRINKET_4 = "Looks important!", --sea worther
		SUNKEN_BOAT_TRINKET_5 = "Why do scale-less wear these?", --boot
		TRINKET_IA_13 = "Is there some juice left for me?", --orange soda
		TRINKET_IA_14 = "Can I play with it?", --voodoo doll
		TRINKET_IA_15 = "Maybe Wicker-lady can show me how to play on this", --ukulele
		TRINKET_IA_16 = "It not a place for food.", --license plate
		TRINKET_IA_17 = "Why do scale-less wear these?", --boot
		TRINKET_IA_18 = "Old thing.", --vase
		TRINKET_IA_19 = "Clouds in head?", --brain cloud pill
		TRINKET_IA_20 = "What is that?", --sextant
		TRINKET_IA_21 = "Tiny boat.", --toy boat
		TRINKET_IA_22 = "It wet, I like it!.", --wine candle
		TRINKET_IA_23 = "It broken but what is that?", --broken aac device
		EARRING = "It not fit, florp.",
		
		TURBINE_BLADES = "This make the wind?",

		TURF_BEACH = "Ground bit.",
		TURF_JUNGLE = "Ground bit.",
		TURF_MAGMAFIELD = "Ground bit.",
		TURF_TIDALMARSH = "Ground bit.",
		TURF_ASH = "Ground bit.",
		TURF_MEADOW = "Ground bit.",
		TURF_VOLCANO = "Ground bit.",
		TURF_SWAMP = "Ground bit.",
		TURF_SNAKESKIN = "It rug made of nasty Snakefolk.",

		WHALE_BLUE = "Big fishie friend!",
		WHALE_CARCASS_BLUE = "Is sleeping?",
		WHALE_WHITE = "Big fishie is angry!",
		WHALE_CARCASS_WHITE = "Is sleeping?",
		WHALE_TRACK = "Little bubbles, florp!",
		WHALE_BUBBLES = "It little bubbles, florp.",
		BLUBBERSUIT = "Glurph, terrible but cozy...",
		BLUBBER = "Glargh...",
		HARPOON = "Don't wanna hurt fishies, glorp.",

		SAIL_PALMLEAF = "Leafy sail helps my boat.",
		SAIL_CLOTH = "That wind isn't getting away now!",
		SAIL_SNAKESKIN = "It sail made of red scales.",
		SAIL_FEATHER = "It made from dumb bird.",
		IRONWIND = "Weeeeeee!",


		BERMUDATRIANGLE = "Normally don't see shapes like that in sea...",
	
		PACKIM_FISHBONE = "Someone not take care of pet!",
		PACKIM = "Is Birdfolk with big belly!",

		TIGERSHARK = "Big fish mom!",
		MYSTERYMEAT = "Maybe it still good?",
		SHARK_GILLS = "Glargh!",
		TIGEREYE = "Feels goopy... want to touch it!",
		DOUBLE_UMBRELLAHAT = "Keeps all rain away...",
		SHARKITTEN = "Awww... so cute.",
		SHARKITTENSPAWNER = 
		{
			GENERIC = "It full of little fishies",
			INACTIVE = "Huge pile of sand.",
		},

		WOODLEGS_KEY1 = "Something, somewhere must be locked.",--Unused
		WOODLEGS_KEY2 = "This key probably unlocks something.",--Unused
		WOODLEGS_KEY3 = "That's a key.",--Unused
		WOODLEGS_CAGE = "That seems like an excessive amount of locks.",--Unused

		CORAL = "Pretty sea rocks!",
		ROCK_CORAL = "Oooh, such a nice rock!",
		LIMESTONENUGGET = "Is stone that floats!",
		NUBBIN = "I can plant pretty rocks.",
		CORALLARVE = "Ooooh, slimy!",
		WALL_LIMESTONE = "Looks nice, florp!",
		WALL_LIMESTONE_ITEM = "Limesone pieces.",
		WALL_ENFORCEDLIMESTONE = "Good-looking wall, florp!",
		WALL_ENFORCEDLIMESTONE_ITEM = "Is for building on water.",
		ARMORLIMESTONE = "Is a bit heavy.",
		CORAL_BRAIN_ROCK = "Eat to get smart? how that work, florp?",
		CORAL_BRAIN = "Won't eat it!",
		BRAINJELLYHAT = "No need books anymore, flort.",

		SEASHELL = "Pretty shell is now mine!",
		SEASHELL_BEACHED = "Pretty shell.",
		ARMORSEASHELL = "Is made out of pretty shells.",

		ARMOR_LIFEJACKET = "Is for safe swimming.",
		ARMOR_WINDBREAKER = "Wind is weak with this, florpt.",

		SNAKE = "It a Snakefolk!",
		SNAKE_POISON = "This Snakefolk has strange color.",
		SNAKESKIN = "Is a nice skin.",
		SNAKEOIL = "Is best medicine!",
		SNAKESKINHAT = "At least their scales pretty.",
		ARMOR_SNAKESKIN = "Why scale-less scared of water?",
		SNAKEDEN =
		{
			BURNING = "Aaah, it hot!!",
			BURNT = "Bye-bye little bush.",
			CHOPPED = "Choppy chop!",
			GENERIC = "Hope it has no snake in there, florp...",
		},

		OBSIDIANFIREPIT =
		{
			EMBERS = "It dying.",
			GENERIC = "Time for story-tell around fire, florp!",
			HIGH = "Big fire!",
			LOW = "Fire getting small.",
			NORMAL = "Comfy cozy.",
			OUT = "Can make more fire later!",
		},

		OBSIDIAN = "It really hot!",
		ROCK_OBSIDIAN = "It hard to mine, florp.",
		OBSIDIAN_WORKBENCH = "Hot table for hot things!",
		OBSIDIANAXE = "Hot choppy chop!",
		OBSIDIANMACHETE = "Hot plant hacker!",
		SPEAR_OBSIDIAN = "Hot stick!",
		VOLCANOSTAFF = "It shoot big fire rocks everywhere!",
		ARMOROBSIDIAN = "Hot and strong!",
		COCONADE =
		{
			BURNING = "That not safe!",
			GENERIC = "Splodey ball.",
		},

		OBSIDIANCOCONADE =
		{
			BURNING = "That not safe!",
			GENERIC = "Hot splodey ball!",
		},

		VOLCANO_ALTAR =
		{
			GENERIC = "It closed.",
			OPEN = "Looks hungry!",
		},

		VOLCANO = "Hot mountain!",
		VOLCANO_EXIT = "Is more wet at the other side, florp.",
		ROCK_CHARCOAL = "It made of smudgy charcoal.",
		VOLCANO_SHRUB = "Burnt up.",
		LAVAPOOL = "Too hot for swimming, florp!",
		COFFEEBUSH =
		{
			BARREN = "Needs ashes.",
			WITHERED = "Too cold for him.",
			GENERIC = "Snacks!",
			PICKED = "When more snacks?",
		},

		COFFEEBEANS = "Yummy snacks!",
		COFFEEBEANS_COOKED = "No longer yummy, flort!",
		DUG_COFFEEBUSH = "You are coming  with me, florp.",
		COFFEE = "Flurph... this make head hurt.",

		ELEPHANTCACTUS =
		{
			BARREN = "Needs ashes.",
			WITHERED = "He don't like cold, florp.",
			GENERIC = "It full of huge spikes!",
			PICKED = "Nasty spikes are gone, florp!",
		},

		DUG_ELEPHANTCACTUS = "Should I plant it?",
		ELEPHANTCACTUS_ACTIVE = "Is spiky.",
		ELEPHANTCACTUS_STUMP = "Will it grow back?",
		NEEDLESPEAR = "Is spiky!",
		ARMORCACTUS = "Spiky scales.",
		
		TWISTER = "Glurgh... why wind so angry at me?",
		TWISTER_SEAL = "Awww, so cute!",
		MAGIC_SEAL = "How did that cute seal got this?",
		WIND_CONCH = "I hear wind, florpt!",
		WINDSTAFF = "Wind can sometimes be nice.",

		DRAGOON = "Glorp, I read about you in book!",
		DRAGOONHEART = "Feel sad for him.",
		DRAGOONSPIT = "It not nice to spit!",
		DRAGOONEGG = "It huge egg!",
		DRAGOONDEN = "It home of beast!",

		ICEMAKER = 
		{
			OUT = "It need fuel.",
			VERYLOW = "It dying.",
			LOW = "It getting hungry.",
			NORMAL = "It works good.",
			HIGH = "It running great!",
		},

		HAIL_ICE = "Brrr, it cold!",
	
		BAMBOOTREE =
		{
			BURNING = "Oh no, it hot!",
			BURNT = "Bye-bye little tree.",
			CHOPPED = "Choppy chop!",
			GENERIC = "Tree with strange sticks, florp!",
		},

		BAMBOO = "Some bamboo, florp.",
		FABRIC = "Is soft.",
		DUG_BAMBOOTREE = "Where to plant this?",
		
		JUNGLETREE =
		{
			BURNING = "Bye-bye big tree.",
			BURNT = "All burnt up.",
			CHOPPED = "Only stump left.",
			GENERIC = "Big and leafy!",
		},

		JUNGLETREESEED = "Bad for eating, florpt.",
		JUNGLETREESEED_SAPLING = "Grow big!",
		LIVINGJUNGLETREE = "It giving me funny look, florp.",


		OX = "Big fuzzy know's that water is best!",
		BABYOX = "So little.",--unused
		OX_HORN = "Is really sharp horn!",
		OXHAT = "Make good shell hat!",
		OX_FLUTE = "Makes drain, flort!",

		MOSQUITO_POISON = "Glorp, it dangerous blood sucker!",
		MOSQUITOSACK_YELLOW = "Glurgh...",

		STUNGRAY = "Flying fish?",
		POISONHOLE = "Glurgh, that is not safe.",
		GASHAT = "Protects from really bad stinks.",

		ANTIVENOM = "Can help when I feel bad.",
		VENOMGLAND = "Glargh!....ugly tasting medicine.",
		POISONBALM = "It helps with sick.",
		
		SPEAR_POISON = "It spreads sickness.",
		BLOWDART_POISON = "The pointy end goes that way.",

		SHARX = "Glurp... he is not nice fish.",
		SHARK_FIN = "Ewww...",
		SHARKFINSOUP = "Glargh!",
		SHARK_TEETHHAT = "Can be a queen of swamp?",
		AERODYNAMICHAT = "Wheeeeeeeeee!!",

		IA_MESSAGEBOTTLE = "Wicker-lady!! Read what it say!",
		IA_MESSAGEBOTTLEEMPTY = "There nothing in here.",
		BURIEDTREASURE = "Treasure?",

		SAND = "Feel nice on scales, florp!",
		SANDDUNE = "Big pile of sand, florp.",
		SANDBAGSMALL = "Glurgh, why we make this?",
		SANDBAGSMALL_ITEM = "I don't want to keep water out.",
		SANDCASTLE =
		{
			SAND = "What happend to pretty castle?",
			GENERIC = "Has pretty shells on it."
		},

		SUPERTELESCOPE = "Stick has an eye now, florp.",
		TELESCOPE = "Long stick to see in the sea.",
		
		DOYDOY = "Dumb Birdfolk!",
		DOYDOYBABY = "Small Birdfolk dummy!",
		DOYDOYEGG = "Little dummy inside?",
		DOYDOYEGG_COOKED = "Don't want it!",
		DOYDOYFEATHER = "Green birdy feather.",
		DOYDOYNEST = "It nest for a dummy.",
		TROPICALFAN = "Oo, it big fluffy fan!",
	
		PALMTREE =
		{
			BURNING = "Bye-bye tall tree.",
			BURNT = "All burnt up.",
			CHOPPED = "Broke into pieces!",
			GENERIC = "It so tall.",
		},

		COCONUT = "How to open it, florp?",
		COCONUT_HALVED = "It has water inside, florp!",
		COCONUT_COOKED = "Yummy!",
		COCONUT_SAPLING = "Will be tall someday!",
		PALMLEAF = "Tree lost his hair.",
		PALMLEAF_UMBRELLA = "Why scaleless want to keep rain away?",
		PALMLEAF_HUT = "Shady house.",
		LEIF_PALM = "Palmbeast!!",

		CRAB = 
		{
			GENERIC = "Little crab, flort!",
			HIDDEN = "Where it go?",
		},

		CRABHOLE = "Crabbit home.",

		TRAWLNETDROPPED = 
		{
			SOON = "It will sink, florpt.",
			SOONISH = "It sinks.",
			GENERIC = "Any treasures inside?",
		},

		TRAWLNET = "Is for fishies.",
		IA_TRIDENT = "Is really old, florp.",

		KRAKEN = "Glurp!! Is big nasty fish!",
		KRAKENCHEST = "Fish had box all along?",
		KRAKEN_TENTACLE = "Worse than those at home, glorp.",
		QUACKENBEAK = "It so big!",
		QUACKENDRILL = "Glurp... makes ocean dirty!",
		QUACKERINGRAM = "Out of my way, sea monsters!",

		MAGMAROCK = "Rocks on rocks.",
		MAGMAROCK_GOLD = "Rocks with shiny!",
		FLAMEGEYSER = "It hot hole!",

		TELEPORTATO_SW_RING = "Looks like I could use this.",--unused
		TELEPORTATO_SW_BOX = "It looks like a part for something.",--unused
		TELEPORTATO_SW_CRANK = "I wonder what this is used for.",--unused
		TELEPORTATO_SW_BASE = "I think it's missing some parts.",--unused
		TELEPORTATO_SW_POTATO = "Seems like it was made with a purpose in mind.",--unused

		PRIMEAPE = "Hee-hee, funny ape.",
		PRIMEAPEBARREL = "It messy like in my swamp.",
		MONKEYBALL = "Funny looking ball, florp.",
		WILBUR_UNLOCK = "He look like king!",--unused
		WILBUR_CROWN = "It for monkey!",--unused

		MERMFISHER = "Can you catch me pet?",
		MERMHOUSE_FISHER = "Home of mermfolk with fishie friends!",

		OCTOPUSKING = "King of the sea!",
		OCTOPUSCHEST = "Treasure inside?",

		SWEET_POTATO = "Sweet veggie!",
		SWEET_POTATO_COOKED = "Mmm, sweet and hot!",
		SWEET_POTATO_PLANTED = "It a potato!",
		SWEET_POTATO_SEEDS = "Make more tasty snacks!",
		SWEETPOTATOSOUFFLE = "Wha-? This sweet potato!",

		BOAT_WOODLEGS = "Florp, it makes splashes!",
		WOODLEGSHAT = "It has cute fishie on it, florp!",
		SAIL_WOODLEGS = "Scale-less use so many cloths.",

		PEG_LEG = "What is this stick for?",
		PIRATEGHOST = "Glorph, go away!",

		WILDBORE = "Glurp! Pigfolk...",
		WILDBOREHEAD = "Ha ha!",
		WILDBOREHOUSE = "That house full of nasty Pigfolk!",

		MANGROVETREE = "Water tree.",
		MANGROVETREE_BURNT = "Bye-bye water tree.",

		PORTAL_SHIPWRECKED = "It broken.",--In SW it's used for broken seaworthy --unused
		SHIPWRECKED_ENTRANCE = "Wonder where it goes, florp!",
		SHIPWRECKED_EXIT = "But it so nice in here...",

		TIDALPOOL = "Lots of baby fishies in there!!",
		FISHINHOLE = "Hello fishies!",
		FISH_TROPICAL = "So cute!",
		TIDAL_PLANT = "Swamp plant.",
		MARSH_PLANT_TROPICAL = "Lots of these in swamp, flort.",

		FLUP = "This fishie is not nice.",
		BLOWDART_FLUP = "Makes Birfolks sleepy, florp!",

		SEA_LAB = "Is for sea stuff.",
		BUOY = "Floating glowy!", 
		WATERCHEST = "Maybe something good inside?!",

		LUGGAGECHEST = "Something good inside?",
		WATERYGRAVE = "Hmm? Something in water?",
		SHIPWRECK = "Poor boat.",
		BARREL_GUNPOWDER = "It has splodey powder inside!",
		RAWLING = "Funny talking ball!, flort!",
		GRASS_WATER = "Grass in water?",
		KNIGHTBOAT = "Water Ironfolk!",

		DEPLETED_BAMBOOTREE = "Will it grow again?",--unused?
		DEPLETED_BUSH_VINE = "Will it come back?",--unused?
		DEPLETED_GRASS_WATER = "Poor plant, florp.",--unused?

		WALLYINTRO_DEBRIS = "Broken ship.", --unused
		BOOK_METEOR = "Wicker-lady won't read this one for me, florp.",
		CRATE = "Something good inside?",
		SPEAR_LAUNCHER = "It for shooting with stabby sticks, glorp.",
		MUTATOR_TROPICAL_SPIDER_WARRIOR = "Webby boy make Merm cookie next!!",

		--SWC
		BOAT_SURFBOARD = "For travel on the big pond!",
		SURFBOARD_ITEM = "For travel on the big pond!",

		WALANI = {
		    GENERIC = "Hello surf lady!",
	        ATTACKER = "Why mad? Didn't do anything!",
	        MURDERER = "Glorp! Thought you like not hurt!",
	        REVIVER = "....Thank you, florp.",
	        GHOST = "Like being like that, flort?",
	        FIRESTARTER = "Why fire when can be in sea, florp?",
		},

		WILBUR = {
            GENERIC = "Hello funny monkey!",
            ATTACKER = "Glorph, stop throw poop!",
            MURDERER = "Grrr, this mean war!! Monkey not king!",
            REVIVER = "You has friendly face, florp.",
            GHOST = "Hee-hee, funny monkey.",
            FIRESTARTER = "Why you do that?",
		},

		WOODLEGS = {
            GENERIC = "Hello wood legs man!",
            ATTACKER = "Glorph, go away back to sea!",
            MURDERER = "Glorp! Me not like sea fish!",
            REVIVER = "You actually pretty nice, florp.",
            GHOST = "Sea dangerous, will find heart.",
            FIRESTARTER = "Bad idea for legs, flort.",
		},
	},
}