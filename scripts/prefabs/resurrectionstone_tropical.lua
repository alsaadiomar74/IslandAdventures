local assets = {
    Asset("ANIM", "anim/resurrection_stone_sw.zip"),
}

local prefabs = {
    "resurrectionstone"
}

local function fn()
    local inst = Prefabs["resurrectionstone"].fn()

    if not GetGhostEnabled(TheNet:GetServerGameMode()) then
        return inst
    end

    inst.AnimState:SetBank("resurrection_stone_sw")
    inst.AnimState:SetBuild("resurrection_stone_sw")

    inst.realprefab = "resurrectionstone_tropical"

    inst:SetPrefabName("resurrectionstone")

    if not TheWorld.ismastersim then
        return inst
    end
    if inst.components.lootdropper then
        if inst.components.lootdropper and inst.components.lootdropper.loot then
            for k, loot_name in pairs(inst.components.lootdropper.loot) do
                if loot_name == "marble" then
                    inst.components.lootdropper.loot[k] = "limestonenugget"
                end
            end
        end
    end

    return inst
end

return Prefab("resurrectionstone_tropical", fn, assets, prefabs)
