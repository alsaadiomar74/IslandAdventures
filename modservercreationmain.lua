--[WARNING]: This file is imported into modclientmain.lua, be careful!
if not env.is_mim_enabled then
	FrontEndAssets = {
		Asset("IMAGE", "images/hud/customization_shipwrecked.tex"),
		Asset("ATLAS", "images/hud/customization_shipwrecked.xml"),
	}
	ReloadFrontEndAssets()
	
	modimport("main/strings")
end

local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local ImageButton = require "widgets/imagebutton"

local size_descriptions = IAENV.GetCustomizeDescription("size_descriptions")
local yesno_descriptions = IAENV.GetCustomizeDescription("yesno_descriptions")
local frequency_descriptions = IAENV.GetCustomizeDescription("frequency_descriptions")

local rate_descriptions = {
    { text = STRINGS.UI.SANDBOXMENU.SLIDENEVER,    data = "never" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEVERYRARE, data = "veryrare" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDERARE,     data = "rare" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEDEFAULT,  data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEOFTEN,    data = "often" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEALWAYS,   data = "always" },
}

local islandquantity_descriptions = {
    { text = STRINGS.UI.SANDBOXMENU.BRANCHINGLEAST, data = "never" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDERARE,      data = "rare" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEDEFAULT,   data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.SLIDEOFTEN,     data = "often" },
    { text = STRINGS.UI.SANDBOXMENU.BRANCHINGMOST,  data = "always" },
}

local worldtypes = {
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_DEFAULT,     data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_MERGED,      data = "merged" },
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_ISLANDSONLY, data = "islandsonly" },
    { text = STRINGS.UI.SANDBOXMENU.WORLDTYPE_VOLCANOONLY, data = "volcanoonly" },
}

local clocktypes = {
    { text = STRINGS.UI.SANDBOXMENU.CLOCKTYPE_DEFAULT,     data = "default" },
    { text = STRINGS.UI.SANDBOXMENU.CLOCKTYPE_SHIPWRECKED,      data = "tropical" },
    -- { text = STRINGS.UI.SANDBOXMENU.CLOCKTYPE_HAMLET,      data = "plateau" },
}

local season_length_descriptions = {
	{ text = STRINGS.UI.SANDBOXMENU.SLIDENEVER, data = "noseason" },
	{ text = STRINGS.UI.SANDBOXMENU.SLIDEVERYSHORT, data = "veryshortseason" },
	{ text = STRINGS.UI.SANDBOXMENU.SLIDESHORT, data = "shortseason" },
	{ text = STRINGS.UI.SANDBOXMENU.SLIDEDEFAULT, data = "default" },
	{ text = STRINGS.UI.SANDBOXMENU.SLIDELONG, data = "longseason" },
	{ text = STRINGS.UI.SANDBOXMENU.SLIDEVERYLONG, data = "verylongseason" },
	{ text = STRINGS.UI.SANDBOXMENU.RANDOM, data = "random"},
}

local LEVELCATEGORY = LEVELCATEGORY
local worldgen_atlas = "images/worldgen_customization.xml"
local ia_atlas = "images/hud/customization_shipwrecked.xml"

local function add_group_and_item(category, name, text, desc, atlas, order, items)
    if text then --assume that if the group has a text string its new
        IAENV.AddCustomizeGroup(category, name, text, desc, atlas or ia_atlas, order)
    end
    if items then
        for k, v in pairs(items) do
            IAENV.AddCustomizeItem(category, name, k, v)
        end
    end
end

local ia_customize_table = {

    islandmisc = {
        category = LEVELCATEGORY.WORLDGEN,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEMISC,
        items = {
            primaryworldtype = { value = "default", enable = false, image = "world_map.tex", atlas = worldgen_atlas, desc = worldtypes, order = 1, world = { "forest","cave" } },
            islandquantity   = { value = "default", enable = false, image = "world_size.tex", atlas = worldgen_atlas, desc = size_descriptions, order = 3, world = {"forest"} },
            -- islandsize      = { value = "merged", enable = false, image = "world_map.tex", desc = worldtypes, order = 1, world = {"forest"} },
            volcano          = { value = "default", enable = false, image = "volcano.tex", desc = yesno_descriptions, order = 4, world = {"forest"} },
            dragoonegg       = { value = "default", enable = false, image = "dragooneggs.tex", desc = frequency_descriptions, order = 5, world = {"forest"} },
            tides            = { value = "default", enable = false, image = "tides.tex", desc = yesno_descriptions, order = 6, world = {"forest"} },
            floods           = { value = "default", enable = false, image = "floods.tex", desc = frequency_descriptions, order = 7, world = {"forest"} },
            oceanwaves       = { value = "default", enable = false, image = "waves.tex", desc = rate_descriptions, order = 8, world = {"forest"} },
            poison           = { value = "default", enable = false, image = "poison.tex", desc = yesno_descriptions, order = 9, world = {"forest"} },
            bermudatriangle  = { value = "default", enable = false, image = "bermudatriangle.tex", desc = frequency_descriptions, order = 10, world = {"forest"} },
            volcanoisland = { value = "never", enable = false, image = "volcano.tex", desc = yesno_descriptions, order = 11, world = {"forest"} },
        }
    },

	islandunprepared = {
        category = LEVELCATEGORY.WORLDGEN,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEFOOD,
		desc = frequency_descriptions,
        items = {
			sweet_potato = { value = "default", enable = true, image = "sweetpotatos.tex", order = 2, world = {"forest"} },
			limpets 	 = { value = "default", enable = false, image = "limpets.tex", order = 4, world = {"forest"} },
			mussel_farm  = { value = "default", enable = false, image = "mussels.tex", order = 5, world = {"forest"} },
        }
    },

    global = {
		category = LEVELCATEGORY.SETTINGS,
        items = {
			clocktype = { value = "default", enable = false, image = "blank_world.tex", atlas = ia_atlas, desc = clocktypes, order = 50, world = { "forest","cave" } },
			mild = {value = "default", image = "mild.tex", atlas = ia_atlas, options_remap = {img = "blank_season_yellow.tex", atlas = "images/customisation.xml"}, desc = season_length_descriptions, master_controlled = true, order = 51},
			hurricane = {value = "default", image = "hurricane.tex", atlas = ia_atlas, options_remap = {img = "blank_season_yellow.tex", atlas = "images/customisation.xml"}, desc = season_length_descriptions, master_controlled = true, order = 52},
			monsoon = {value = "default", image = "monsoon.tex", atlas = ia_atlas, options_remap = {img = "blank_season_yellow.tex", atlas = "images/customisation.xml"}, desc = season_length_descriptions, master_controlled = true, order = 53},
			dry = {value = "default", image = "dry.tex", atlas = ia_atlas, options_remap = {img = "blank_season_yellow.tex", atlas = "images/customisation.xml"}, desc = season_length_descriptions, master_controlled = true, order = 54},
		}
	},

	islandanimals = {
		category = LEVELCATEGORY.SETTINGS,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEANIMALS,
		desc = frequency_descriptions,
        items = {
			wildbores = { value = "default", enable = false, image = "wildbores.tex", order = 3, world = {"forest"} },
			whalehunt = { value = "default", enable = false, image = "whales.tex", order = 7, world = {"forest"} },
			crabhole  = { value = "default", enable = false, image = "crabbits.tex", order = 8, world = {"forest"} },
			ox        = { value = "default", enable = false, image = "ox.tex", order = 9, world = {"forest"} },
			solofish  = { value = "default", enable = false, image = "dogfish.tex", order = 10, world = {"forest"} },
			doydoy    = { value = "default", enable = false, image = "doydoy.tex", desc = yesno_descriptions, order = 11, world = {"forest"} },
			jellyfish = { value = "default", enable = false, image = "jellyfish.tex", order = 12, world = {"forest"} },
			lobster   = { value = "default", enable = false, image = "lobsters.tex", order = 13, world = {"forest"} },
			--Note: This one could be linked to Birds
			seagull   = { value = "default", enable = false, image = "seagulls.tex", order = 14, world = {"forest"} },
			ballphin  = { value = "default", enable = false, image = "ballphins.tex", order = 15, world = {"forest"} },
			primeape  = { value = "default", enable = false, image = "monkeys.tex", order = 16, world = {"forest"} },
		}
	},

	islandmonsters = {
		category = LEVELCATEGORY.SETTINGS,
        text = STRINGS.UI.SANDBOXMENU.CUSTOMIZATIONPREFIX_IA..STRINGS.UI.SANDBOXMENU.CHOICEMONSTERS,
		desc = frequency_descriptions,
        items = {
			--TODO implement "Sharx" as "likelihood of sharx/crocodogs when meat lands on water" ?
			-- sharx = { value = "default", enable = false, image = "crocodog.tex", order = 1, world = {"forest"} },
			--Note: This one is houndwaves, which technically speaking already exists.
			-- crocodog = { value = "default", enable = false, image = "crocodog.tex", order = 2, world = {"forest"} },
			twister    = { value = "default", enable = false, image = "twister.tex", order = 7, world = {"forest"} },
			tigershark = { value = "default", enable = false, image = "tigershark.tex", order = 8, world = {"forest"} },
			kraken 	   = { value = "default", enable = false, image = "kraken.tex", order = 9, world = {"forest"} },
			flup 	   = { value = "default", enable = false, image = "flups.tex", order = 10, world = {"forest"} },
			mosquito   = { value = "default", enable = false, image = "mosquitos.tex", order = 11, world = {"forest"} },
			swordfish  = { value = "default", enable = false, image = "swordfish.tex", order = 12, world = {"forest"} },
			stungray   = { value = "default", enable = false, image = "stinkrays.tex", order = 13, world = {"forest"} },
		}
	},

}

for name, data in pairs(ia_customize_table) do
    add_group_and_item(data.category, name, data.text, data.desc, data.atlas, data.order, data.items)
end

IACustomizeTable = ia_customize_table

local function LoadPreset(worldsettings_widgets, preset)
    for _, v in ipairs(worldsettings_widgets) do
        v:OnPresetButton(preset)
    end
end

scheduler:ExecuteInTime(0, function() -- Delay a frame so we can get ServerCreationScreen when entering a existing world
    local servercreationscreen = TheFrontEnd:GetOpenScreenOfType("ServerCreationScreen") --= TheFrontEnd:GetActiveScreen()

    if not servercreationscreen or not servercreationscreen.world_tabs then
        return
    end

    local forest_tab = servercreationscreen.world_tabs[1]
    local cave_tab = servercreationscreen.world_tabs[2]

    if not forest_tab or not cave_tab  or not KnownModIndex:IsModEnabled(IAENV.modname) then  -- IsModEnabled fix quick turn on-off mod collapse
        return
    end

    if not servercreationscreen:CanResume() then -- Only when first time creating the world
        LoadPreset(forest_tab.worldsettings_widgets, "SURVIVAL_SHIPWRECKED_CLASSIC")  -- Automatically try switching to the Shipwrecked Preset
    end

    if not servercreationscreen:CanResume() and cave_tab:IsLevelEnabled() then  -- Only when first time creating the world and auto add cave
        LoadPreset(cave_tab.worldsettings_widgets, "SURVIVAL_VOLCANO_CLASSIC")  -- Automatically try switching to the Volcano Preset
    end

    local add_cave_btn = cave_tab.sublevel_adder_overlay:GetAddButton()
    add_cave_btn:SetPosition(-170, 0)

    local button = cave_tab.sublevel_adder_overlay:AddChild(ImageButton("images/global_redux.xml", "button_carny_xlong_normal.tex", "button_carny_xlong_hover.tex", "button_carny_xlong_disabled.tex", "button_carny_xlong_down.tex"))
    button.image:SetScale(.49)
    button:SetFont(CHATFONT)
    button.text:SetColour(0, 0, 0, 1)
    button:SetOnClick(function(self, ...)
        add_cave_btn.onclick(self, ...)

        local PopupDialogScreen = TheFrontEnd:GetOpenScreenOfType("PopupDialogScreen")
        if PopupDialogScreen then  -- There is already a world
            local _add_cave_cb = PopupDialogScreen.dialog.actions.items[1].onclick
            PopupDialogScreen.dialog.actions.items[1].onclick = function(_self, ...)
                _add_cave_cb(_self, ...)
                LoadPreset(cave_tab.worldsettings_widgets, "SURVIVAL_VOLCANO_CLASSIC")
            end
        else
            LoadPreset(cave_tab.worldsettings_widgets, "SURVIVAL_VOLCANO_CLASSIC")
        end
    end)
    button:SetTextSize(19.6)
    button:SetText(STRINGS.UI.SANDBOXMENU.ADDVOLCANO)
    button:SetPosition(120, -63)
    table.insert(cave_tab.sublevel_adder_overlay.actions.items, button)
end)
